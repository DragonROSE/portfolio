using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Расширение UnityEditor
/// Заменяет выбранные GameObjects на сцене на выбранный префаб
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.1 </para>

public class ReplaceGameObjectToPrefab : EditorWindow {

	GameObject prefab = null;

	string stateMessage = "Please Drag'n'Drop prefab to \"Prefab\" and select game objects in the scene";

	bool copyPosition = true;
	bool copyRotation = true;
	bool copyScale = false;

	bool deleteSelectedObjects = true;

	[MenuItem("Tools/Prefab/Replace GameObject To Prefab...")]
	static void ShowCreateWindow(){
		EditorWindow.GetWindow<ReplaceGameObjectToPrefab>(false, "Replace GO");
	}

	void OnSelectionChange(){
		Repaint();
	}

	void OnGUI(){
		prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", prefab, typeof(GameObject), true);

		copyPosition = EditorGUILayout.Toggle("Copy Position", copyPosition);
		copyRotation = EditorGUILayout.Toggle("Copy Rotation", copyRotation);
		copyScale = EditorGUILayout.Toggle("Copy Scale", copyScale);
		deleteSelectedObjects = EditorGUILayout.Toggle("Delete Selected Objects", deleteSelectedObjects);

		if (GUILayout.Button("Replace Selection To Prefab")){
			if (prefab != null){
				if (PrefabUtility.GetPrefabObject(prefab) == null){
					stateMessage = "This object is not a prefab!";
				}else if (Selection.gameObjects.Length == 0){
					stateMessage = "There are no selected objects!";

				}else{
					ReplaceObjects();
				}
			}else{			
				stateMessage = "Prefab is null!";
			}
		}

		GUILayout.Label("State: " + stateMessage);
	}

	void ReplaceObjects(){
		GameObject[] gameObjects = Selection.gameObjects;

		int gameObjectsCount = gameObjects.Length;

		for (int i = 0; i < gameObjectsCount; i++){
			Transform parentTransform = gameObjects[i].transform.parent;
			Vector3 pos = gameObjects[i].transform.position;
			Quaternion rot = gameObjects[i].transform.rotation;
			Vector3 scl = gameObjects[i].transform.localScale;

			if (deleteSelectedObjects){
				DestroyImmediate(gameObjects[i]);
			}

			GameObject newGameObject = PrefabUtility.InstantiatePrefab(prefab) as GameObject;

			if (copyPosition){
				newGameObject.transform.position = pos;
			}
			if (copyRotation){
				newGameObject.transform.rotation = rot;
			}
			if (copyScale){
				newGameObject.transform.localScale = scl;
			}
			if (parentTransform != null){
				newGameObject.transform.parent = parentTransform;
			}
		}

		stateMessage = "GameObjects was " + (deleteSelectedObjects ? "replaced" : "copied") + " = " + gameObjectsCount.ToString();
	}
}

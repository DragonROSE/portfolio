using UnityEngine;
using System.Collections;

/// <summary>
/// Реализация физики ручной и автоматической коробки передач, все величины вычисляются в системе "СИ"
/// Реализовано сцепление, передние и задние передачи, понижающий редуктор
/// Реализована обратная связь от колёс к двигателю
/// Для общения с двигателем и шасси используются соответственно интерфейсы I_Engine, I_Chassis и I_Chassis_Extended
/// Передачи должны идти по порядку, от отрицательных к положительным, через нейтральную, т.е. = 0
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.2 </para>

/*
 * Настройка автоматического сцепления (только для isAutoClutch = true)
 * 1. выставить middleRPM на значение немного больше, чем idleRPM у двигателя, начиная с оборотов двигателя middleRPM и выше, начнёт работать автоматическое сцепление
 * 2. выставить convergenceRPM в 100 ~ 400, именно в этом диапазоне будет работать авто сцепление начиная с middleRPM
 * 3. выставить lowRPM на значение немного меньше, чем idleRPM у двигателя, ниже этого значения, сцепление принудительно выжимается (отключается)
 * 4. выставить minSpeed, при котором будет срабатывать сцепение
 * 5. clutch изменяется линейно от lowRPM до lowRPM+convergenceRPM, при этом соблюдаются условия:
 *   a. RPM < lowRPM, то clutch = 0
 *   b. RPM > lowRPM + convergenceRPM, то clutch = 1
 *   c. current speed < minSpeed и RPM < lowRPM, то clutch = 0
 */

public class GearBoxManualAuto : MonoBehaviour, IGearBox, IGearBoxExtended, IQuality{

	const float getControllerDelay = 0.5f;

	public float topGear = 4.619f; //главная пара (за основу взята главная передача Hyundai Solaris)
	public float reductorGear = 1.0f; //отношение передачи редуктора
	public float[] gear = { -3.583f, 0.0f, 3.615f, 1.95f, 1.37f, 1.031f, 0.837f }; //передачи (за основу взяты передаточные числа Hyundai Solaris)
	public float gearsEfficiency = 0.95f; //потеря мощности на КПП (0 - 100% потеря, 1 - нет потери)

	public float minSpeed = 1.0f; //минимальная скорость при которой включается автоматическое сцепление [км/ч]
	public float lowRPM = 1000.0f; //нижний предел RPM двигателя (переключение передачи вниз или переключение с найтральной на первую)
	public float middleRPM = 1300.0f; //нижний динамический диапазон повышения скорости (зависит от педали газа)
	public float highRPM = 2000.0f; //верхний передел RPM двигателя (переключение передачи вверх)
	public float kickDownRPM = 1500.0f; //RPM ниже которых происходит Kick Down
	public float convergenceRPM = 50.0f; //точность сведения к холостым оборотам
	
	public int startKickDownGear = 2; //номер передачи, с которой начинает работать режим Kick Down (только для автомата "isAutoGear = true")
	public int gearModeDrive = 2; //значение контроллера для режима "Drive"
	public int gearModeNeutral = 1; //значение контроллера для нейтральной передачи
	public int gearModeRear = 0; //значение контроллера для задней передачи
	public int gearModeDrive1 = 3; //значение контроллера для 1-й передачи
	public int gearModeDrive2 = 4; //значение контроллера для 2-й передачи
	
	public bool isAutoGear = false; //автоматическое переключение передач
	public bool isAutoClutch = false; //автоматическое сцепление
	public bool isReductor = false; //наличие редуктора

	float reductorRatio = 1.0f; //текущее отношение передачи понижающего редуктора
	float reductorDelta = 0.0f; //1 - reductorRatio
	float differenceToWheelTorque = 0.0f; //промежуточный крутящий момент
	float engineAngularVelocityFromWheel = 0.0f; //изменеие оборотов двигателя от момента колёс
	float toEngineAngularAcceleration = 0.0f; //добавочное угловое ускорение от колёс передаваемый на двигатель
	float toWheelAccelerationTorque = 0.0f; //разгоняющий крутящий момент передаваемый на колёса
	float toWheelBrakeTorque = 0.0f; //тормозящий крутящий момент передаваемый на колёса

	int currentGear = 0; //текущая передача
	int neutralGear = 0; //номер нейтральной передачи

	float clutchAxis = 1.0f; //сцепление от 0 до 1 (0 - выжато, 1 - колёса сцеплены с двигателем)
	float accelAxis = 0.0f;

	//кэшированные величины автоматической коробки передач
	float delayChangeGear = 0.5f; //промежуток времеми запрета переключения передачи [с]
	float convCluth = 0.1f; //время на переключение передачи [с]
	float currConvRPM = 0.0f;
	float timeChange = 0.0f;
	float quality = 1.0f;
	int gearChange = 0;
	int autoGear = 0;
	bool switched = false;

//	float rpmToRad = (2.0f * Mathf.PI) / 60.0f; //перевод из оборотов в минуту в радианы в секунду
	float fixedDeltaTime = 0.01f; //кеширование при старте Time.fixedDeltaTime

	bool doGetController = true;
	bool startIsAutoGear = false;

//	public Transform controllerTransform = null; //объект на котором висит контроллер
	private IInputMan controller = null; //данные с органов управления
	
	public Transform engineMainTransform = null; //объект на котором висит скрипт двигателя
	private IEngine engineMain = null; //интерфейс двигателя
	public Transform chassisMainTransform = null; //объект на котором висит скрипт шасси
	private IChassis chassisMain = null; //интерфейс шасси
	private IChassisExtended chassisExtendedMain = null; //расширенный интерфейс шасси

	delegate void AutoSwitchGears();
	AutoSwitchGears CallAutoGear;
	AutoSwitchGears StartAutoGear;
	delegate void ReductorGears();
	ReductorGears CallReductorGear;

	// ------- DEBUG Start ------- //
//	void OnGUI(){
//		GUI.Label(new Rect(10.0f, 10.0f, 200.0f, 25.0f), "Clutch axis: " + clutchAxis.ToString());
//		GUI.Label(new Rect(10.0f, 30.0f, 200.0f, 25.0f), "Accel: " + toWheelAccelerationTorque.ToString());
//		GUI.Label(new Rect(10.0f, 50.0f, 200.0f, 25.0f), "Brake: " + toWheelBrakeTorque.ToString());
//	}
	// -------- DEBUG End -------- //

	void Start(){
		GetInterfaces();
		SetConstants();
		SetVariables();
		StartCoroutine(GetController());
	}

	void FixedUpdate(){
		GetControls();
		CallAutoGear();
		Gears();
	}

	void SetConstants(){
		fixedDeltaTime = Time.fixedDeltaTime;
	}

	void SetVariables(){
		if (isAutoGear){
			CallAutoGear = IsAutoGears;
		}else if (isAutoClutch){
			CallAutoGear = AutoClutch;
		}else{
			CallAutoGear = NotAutoGears;
		}
		StartAutoGear = CallAutoGear;
		startIsAutoGear = isAutoGear;

		if (isReductor){
			CallReductorGear = IsReductorGears;
			reductorDelta = reductorGear - 1.0f;
		}else{
			CallReductorGear = NotReductorGears;
			reductorRatio = 1.0f;
		}

		for (int i = 0; i < gear.Length; i++){ //поиск нейтральной передачи
			if (gear[i] == 0.0f){
				neutralGear = i;
				currentGear = i;
				break;
			}
		}
	}

	void GetInterfaces(){
//		controller = InterfaceLib.GetInterfaceComponent<IInputMan>(controllerTransform);
		engineMain = InterfaceLib.GetInterfaceComponent<IEngine>(engineMainTransform);
		chassisMain = InterfaceLib.GetInterfaceComponent<IChassis>(chassisMainTransform);
		chassisExtendedMain = InterfaceLib.GetInterfaceComponent<IChassisExtended>(chassisMainTransform);
	}

	void GetControls(){
		CallReductorGear();
	}

	void IsReductorGears(){
		reductorRatio = 1.0f + reductorDelta * controller.GetAxis("Reductor");
	}

	void NotReductorGears(){
		return;
	}

	void IsAutoGears(){ //автоматическая коробка передач
		autoGear = (int)controller.GetAxis("gear_lever");
		accelAxis = controller.GetAxis("Gas");

		if (autoGear == gearModeRear){ //режим R (Reverse)
			currentGear = 0;
		}else if (autoGear == gearModeNeutral){ //режим N (Neutral)
			currentGear = neutralGear;
		}else if (autoGear == gearModeDrive1){ //режим 1-я передача
			currentGear = neutralGear + 1;
		}else if (autoGear == gearModeDrive2){ //режим 2-я передача
			currentGear = neutralGear + 2;
		}else if (autoGear == gearModeDrive && Time.time >= timeChange + delayChangeGear){ //режим D (Drive)
			if (currentGear <= neutralGear){ // && engineMain.rpm > lowRPM
				gearChange = 1;
				switched = true;
			}else if (engineMain.rpm < lowRPM + accelAxis * (kickDownRPM - lowRPM) && currentGear > startKickDownGear){
				gearChange = -1;
				switched = true;
			}else if (engineMain.rpm > middleRPM + accelAxis * (highRPM - middleRPM) && currentGear < gear.Length - 1){
				gearChange = 1;
				switched = true;
			}else if (engineMain.rpm < lowRPM && currentGear > neutralGear + 1){
				gearChange = -1;
				switched = true;
			}
		}

		if (switched){
			if (clutchAxis == 0.0f){
				switched = false;
				timeChange = Time.time; //запрет на переключение передачи на некоторое время
				currentGear += gearChange;
			}
			clutchAxis -= convCluth; //выжимаем сцепление для переключения передачи
		}else{
			currConvRPM = (engineMain.rpm - (engineMain.idleRpm - convergenceRPM)) / convergenceRPM;
			if (clutchAxis < currConvRPM){
				clutchAxis += convCluth;
			}else if (clutchAxis > currConvRPM){
				clutchAxis -= convCluth;
			}
		}

		clutchAxis = Mathf.Clamp01(clutchAxis);
	}

	void AutoClutch(){
		currentGear = (int)controller.GetAxis("gear_lever");

		currConvRPM = (engineMain.rpm - middleRPM) / convergenceRPM;
		if (gear[currentGear] != 0.0f && ((clutchAxis < currConvRPM && engineMain.rpm > middleRPM) || (Mathf.Abs(chassisMain.GetLinearSpeed()) > minSpeed && engineMain.rpm > lowRPM))){
			clutchAxis += convCluth;
		}else{
			clutchAxis -= convCluth;
		}

		clutchAxis = Mathf.Clamp01(clutchAxis);
	}

	void NotAutoGears(){ //ручная коробка передач
		clutchAxis = 1.0f - controller.GetAxis("Clutch");
		currentGear = (int)controller.GetAxis("gear_lever");
	}

	void Gears(){
		engineAngularVelocityFromWheel = topGear * reductorRatio * gear[currentGear] * chassisExtendedMain.CalcAverageAngularVelocity(); //угловая скорость вала двигателя от колёс
		toEngineAngularAcceleration = (engineAngularVelocityFromWheel - engineMain.angularVelocity) * chassisMain.inertia / engineMain.inertia;
		differenceToWheelTorque = (engineMain.angularVelocity + toEngineAngularAcceleration * fixedDeltaTime - engineAngularVelocityFromWheel) * engineMain.inertia; //разница вращений валов двигателя и КПП с учётом следующего шага
		if (engineMain.torque + differenceToWheelTorque >= 0.0f){
			toWheelAccelerationTorque = (engineMain.torque + differenceToWheelTorque) * gearsEfficiency * topGear * reductorRatio * gear[currentGear] * clutchAxis;
			toWheelBrakeTorque = 0.0f;
		}else{
			toWheelBrakeTorque = Mathf.Abs((engineMain.torque + differenceToWheelTorque) * gearsEfficiency * topGear * reductorRatio * gear[currentGear] * clutchAxis);
			toWheelAccelerationTorque = 0.0f;
		}
	}

	//мониторинг изменения типа контроллера
	IEnumerator GetController(){
		while (doGetController){
			controller = InputManLib.GetController(this);
			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	void OnEnable(){
		if (!doGetController){
			doGetController = true;
			StartCoroutine(GetController());
		}
	}

	void OnDisable(){
		doGetController = false;
	}

	void OnDestroy(){
		OnDisable();
	}

	//Возвращает направление вращения коробки передач в зависимости от включенной скорости
	public int GetDirectionRotation(){
		return (int)Mathf.Sign(gear[currentGear]);
	}

	public float feedbackAngularAcceleration{
		get{
			return toEngineAngularAcceleration;
		}
		set{
			return;
		}
	}

	public float clutch{
		get{
			return clutchAxis;
		}
		set{
			return;
		}
	}

	public float gearRatio{
		get{
			return topGear * reductorRatio * gear[currentGear];
		}
		set{
			return;
		}
	}

	public int currGear{
		get{
			return currentGear;
		}
		set{
			return;
		}
	}

	public float wheelAccelerationTorque{
		get{
			return toWheelAccelerationTorque;
		}
		set{
			return;
		}
	}

	public float wheelBrakeTorque{
		get{
			return toWheelBrakeTorque;
		}
		set{
			return;
		}
	}

	public int GetCount(){
		return gear.Length;
	}

	public float GetGearRatio(int index){
		return gear[index];
	}

	public void SetGearRatio(int index, float newRatio){
		gear[index] = newRatio;
	}

	public float GetTorque(float torq){
		return toWheelAccelerationTorque;
	}

	public float FeedBackRPM(float fRPM){
		return fRPM * topGear * reductorRatio * gear[currentGear];
	}

	public int offsetGears{
		get{
			return 0;
		}
		set{
			return;
		}
	}

	public int currOffsetGears{
		get{
			return 0;
		}
		set{
			return;
		}
	}

	public int mode{
		get{
			return currentGear;
		}
		set{
			return;
		}
	}

	public string GetModeToString(int m){
		string currReductor = reductorRatio != 1.0f ? " + reductor" : "";
		if (currentGear > neutralGear){
			return "drive " + (currentGear - neutralGear).ToString() + currReductor;
		}else if (currentGear < neutralGear){
			return "reverse " + (Mathf.Abs(currentGear - neutralGear)).ToString() + currReductor;
		}else{
			return "neutral" + currReductor;
		}
	}

	public float GetQuality(){
		return quality;
	}

	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality > 0.0f && newQuality <= 0.5f){
				CallAutoGear = IsAutoGears;
				isAutoGear = true;
				enabled = true;
			}else 
			if (newQuality == 0.0f){
				enabled = false;
			}else{
				CallAutoGear = StartAutoGear;
				isAutoGear = startIsAutoGear;
				enabled = true;
			}
		}
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Реализация физики дизельного двигателя, все величины вычисляются в системе "СИ"
/// Реализован график зависимости крутящего момента от оборотов двигателя
/// Для работы необходимо задать график крутящего момента от оборотов вручную для конкретного двигателя
/// Для общения с коробкой передач используется интерфейс I_GearBox
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.6 </para>

public class EngineDiesel : MonoBehaviour, IEngine, IEngineExtended, IParameters, INetworked, IQuality{

	const float getControllerDelay = 0.5f;
	const float delayCalcParam = 0.5f; //задержка между пересчётами дополнительных параметров двигателя
	const float allEnginesLowMaxRPM = 4000.0f; //"максимальные обороты" при которых первые "numAxisCurveX" точек графика крутящего момента строятся для всех двигателей одинаково, а далее в зависимости от "engineMaxTorque"
	const int countParameters = 16; //количество параметров для <Get|Set>Parameters()
	const int numAxisCurveX = 2; //количество точек начиная с нулевой, для построения начала кривой графика крутящего момента

	public AnimationCurve torqueRPM = null; //кривая крутящего момента

	public float engineIdleRPM = 800.0f; //холостой ход [об/мин]
	public float engineMinRPM = 500.0f; //минимальные обороты двигателя [об/мин]
	public float engineMaxRPM = 10000.0f; //максимальные обороты [об/мин]
	public float engineWorkRPM = 1500.0f; //рабочие обороты двигателя [об/мин]
	public float engineMaxTorque = 170.0f; //максимальный крутящий момент [н*м]
	public float engineDifferenceRPM = 50.0f; //разность оборотов холостого хода и минимальных оборотов (для поддержания оборотов холостого хода) [об/мин]
	public float engineStopRPM = 10.0f; //обороты остановки двигателя [об/мин]
	public float engineInertia = 0.3f; //инертность двигателя [кг * м^2]
	public float engineBrakeTorque = 1.0f; //трение в двигателе (тормозящий двигатель момент)
	public float engineBrakeRPMTorque = 0.01f; //коэффициент трения в двигателе при увеличении оборотов
	public float engineStarterTorque = 100.0f; //крутящий момент стартера [н*м]
	public float decompressionRatio = 0.9f; //коэффициент снижения мощности двигателя (декомпрессия двигателя)
	public float environmentTemperature = 20.0f; //температура окружающей среды и стартовая температура двигателя [градусы цельсия]
	public float workTemperature = 95.0f; //рабочая температура двигателя [градусы цельсия]
	public float speedTemperatureToWork = 0.01f; //скорость схождения к рабочей температуре
	public float speedTemperatureToEnvironment = 0.005f; //скорость схождения к температуре окружающей среды
	public float startFuel = 50.0f; //количество литров топлива на старте [л]
	public float consumptionFuel = 2.0f; //количество потребления топлива на холостом ходу [л/ч]
	public float consumptionFuelRatio = 5.0f; //во сколько раз отличается потребление топлива на холостом ходу и полных оборотах
	public float oilMaxPressure = 500.0f; //давление масла в двигателе [кПа]
	public float convergenceOil = 0.1f; //скорость схождения давления масла к максимуму

	//пренастройки двигателя
	public bool isFeedback = true; //разрешить обратную связь и реакцию на сцепление
	public bool isDecelerator = false; //наличие педали понижающий подачу топлива (деселератор)
	public bool isAutoAccel = true; //автоматическая поддержка двигателем холостого хода
	public bool isStarter = true; //наличие стартера
	public bool isDecompression = false; //наличие декомпрессии (снижение мощности)
	public bool isAdditionalAccelerate = false; //наличие дополнительного управления подачей топлива

	//состояние двигателя
	float engineRPM = 0.0f; //текущие обороты двигателя
	float engineAngularVelocity = 0.0f; //обороты двигателя в [рад / с]
	float engineAngularAcceleration = 0.0f; //угловое ускорение вала двигателя в [рад / с^2]
	float engineAutoAccelRPM = 0.0f; //обороты, при которых срабатывает добавление газа (чтобы двигатель не заглох)
	float engineTargetRPM = 0.0f; //стремиться к оборотам (engineIdleRPM + engineDifferenceRPM + engineTargetRPM) используется при (isFeedback = false)
	float engineTorque = 0.0f; //текущий крутящий момент на двигателе (внутренний)
	float engineTorqueOut = 0.0f; //текущий крутящий момент на двигателе (на выходе)
	float engineBrakeFinalTorque = 0.0f; //результирующее трение в двигателе
	float currStarterTorque = 0.0f; //текущий крутящий момент стартера
	float temperatureKPD = 1.0f; //КПД двигателя меняется от температуры
	float engineTemperature = 0.0f; //текущая температура двигателя
	float decompression = 1.0f; //снижение мощности (декомпрессия двигателя)
	float engineFuel = 1.0f; //текущее количество топлива
	float engineFuelFill = 0.0f; //текущая заполненность двигателя топливом
	float engineOilPressure = 0.0f; //текущее давление масла

	//оси
	float accelAxis = 0.0f; //педаль газа
	float additionalAccel = 0.0f; //управление педалью газа через внешние модули
	float deceleratorAxis = 0.0f; //педаль уменьшения подачи топлива
	float clutchAxis = 0.0f; //сцепление от 0 до 1 (0 - выжато, 1 - колёса сцеплены с двигателем)
	float starterKey = 0.0f; //стартер двигателя
	float engineOff = 0.0f; //перекрытие подачи топлива

	//константы кривой крутящего момента
	float[] axisCurveX = { 0.0f, 0.13f, 0.19f, 0.25f, 0.31f, 0.38f, 0.44f, 0.5f,  0.56f, 0.63f, 0.69f, 0.75f, 0.81f, 0.88f, 0.94f, 1.0f };
	float[] axisCurveY = { 0.0f, 0.19f, 0.62f, 0.93f, 1.0f,  0.98f, 0.97f, 0.97f, 0.93f, 0.89f, 0.81f, 0.66f, 0.53f, 0.39f, 0.23f, 0.0f };

//	public Transform controllerTransform = null; //объект на котором висит контроллер
	IInputMan controller = null; //данные с органов управления

	public Transform gearBoxTransform = null; //объект на котором висит скрипт КПП
	IGearBox gearBox = null; //интерфейс КПП
	IGearBoxExtended gearBoxExtended = null; //расширенный интерфейс КПП

	float fixedDeltaTime = 1.0f; //Time.fixedDeltaTime
	float RadiansToRPM = 60.0f / (2.0f * Mathf.PI); //перевод из радиан в секунду в обороты в минуту
	float RPMToRadians = (2.0f * Mathf.PI) / 60.0f; //перевод из оборотов в минуту в радианы в секунду
	float quality = 1.0f; //включает или отключает расчёт топлива, масла и т.д.

	bool doCalcParam = true;
	bool doGetController = true;

	delegate void AxisAdditionalAccelerateDelegate();
	AxisAdditionalAccelerateDelegate CallAxisAdditionalAccelerate;
	delegate void AxisFeedbackDelegate();
	AxisFeedbackDelegate CallAxisFeedback;
	delegate void AxisDeceleratorDelegate();
	AxisDeceleratorDelegate CallAxisDecelerator;
	delegate void AxisDecompressionDelegate();
	AxisDecompressionDelegate CallAxisDecompression;
	delegate void TorqueOutFeedbackDelegate();
	TorqueOutFeedbackDelegate CallTorqueOut;
	delegate void AxisStarterDelegate();
	AxisStarterDelegate CallAxisStarter;

	float Sqr(float x){
		return x * x;
	}

	// ------- DEBUG Start ------- //
//	void OnGUI(){
//		GUI.Label(new Rect(10.0f, 10.0f, 200.0f, 25.0f), "Accel axis: " + accelAxis.ToString());
//	}
	// -------- DEBUG End -------- //

	void Start(){
        NetRegister();
		SetConstants();
		SetVariables();
		GetInterfaces();
		SetCurve();
		if (quality >= 0.5f){
			StartCoroutine(ExtendedParameters());
		}
		StartCoroutine(GetController());
	}

	void SetConstants(){
		fixedDeltaTime = Time.fixedDeltaTime;
	}

	void SetVariables(){
		engineTemperature = environmentTemperature;
		engineFuel = startFuel;

		if (isAutoAccel){
			engineAutoAccelRPM = engineIdleRPM + engineDifferenceRPM;
		}else{
			engineAutoAccelRPM = engineMinRPM;
		}

		if (isAdditionalAccelerate){
			CallAxisAdditionalAccelerate = AxisIsAdditionalAccelerate;
		}else{
			CallAxisAdditionalAccelerate = AxisNotAdditionalAccelerate;
		}

		if (isDecelerator){
			CallAxisDecelerator = AxisIsDecelerator;
		}else{
			CallAxisDecelerator = AxisNotDecelerator;
			deceleratorAxis = 0.0f;
		}

		if (isFeedback){
			CallAxisFeedback = AxisIsFeedback;
			CallTorqueOut = TorqueOutIsFeedback;
		}else{
			CallAxisFeedback = AxisNotFeedback;
			CallTorqueOut = TorqueOutNotFeedback;
		}

		if (isDecompression){
			CallAxisDecompression = AxisIsDecompression;
		}else{
			CallAxisDecompression = AxisNotDecompression;
			decompression = 1.0f;
		}

		if (isStarter){
			CallAxisStarter = AxisIsStarter;
		}else{
			CallAxisStarter = AxisNotStarter;
		}
	}

	void GetInterfaces(){
		//controller = InterfaceLib.GetInterfaceComponent<IInputMan>(controllerTransform);
		gearBox = InterfaceLib.GetInterfaceComponent<IGearBox>(gearBoxTransform);
		if (isFeedback){
			gearBoxExtended = InterfaceLib.GetInterfaceComponent<IGearBoxExtended>(gearBoxTransform);
		}
	}

	void SetCurve(){
		float currMaxRPM = 0.0f;
		if (allEnginesLowMaxRPM < engineMaxRPM){
			currMaxRPM = allEnginesLowMaxRPM;
		}else{
			currMaxRPM = engineMaxRPM;
		}
		torqueRPM = new AnimationCurve();
		for (int i = 0; i < axisCurveX.Length; i++){
			if (i == numAxisCurveX){
				currMaxRPM = engineMaxRPM; //первые 3 точки строятся для всех двигателей одинаково, а далее в зависимости от "engineMaxTorque"
			}
			torqueRPM.AddKey(axisCurveX[i] * currMaxRPM, axisCurveY[i] * engineMaxTorque);
		}
	}

    #region INetworked
	public bool IsNetworked=true;
    public void NetRegister()
    {
		if(!IsNetworked)
			return;
        ClientServer.Register(this);
    }

    public string GetName()
    {
        return ClientServer.GetName(this);
    }

    public bool? IsMasterSlave()
    {
        return !BuildInfo.IsRMI();
    }

//	int seconds = 0; //??? никогда не используется ???

    public void Serialize(SStream stream, SNetworkMessageInfo info, bool isWriting)
    {
		if(!IsNetworked)
			return;
        float _rpm = rpm;

        if (isWriting)
        {
            stream.Serialize(ref _rpm);
            #region Debug
            //////////////////////////////////////////////////////
            //int currentTime = (int)(Time.timeSinceLevelLoad / 3f);
            //if (currentTime > seconds)
            //{
            //    seconds = currentTime;
            //    Debug.LogError("Engine_Diesel rpm: " + rpm);
            //}
            ////////////////////////////////////////////////////// 
            #endregion
        }
        else
        {
            stream.Serialize(ref _rpm);
            rpm = _rpm;
            #region Debug
            //////////////////////////////////////////////////////
            //int currentTime = (int)(Time.timeSinceLevelLoad/3f);
            //if (currentTime > seconds)
            //{
            //    seconds = currentTime;
            //    Debug.LogError("Engine_Diesel rpm: " + rpm);
            //}
            ////////////////////////////////////////////////////// 
            #endregion
        }
    } 
    #endregion

	void FixedUpdate(){
		GetControls();
		Engine();
	}

	//мониторинг изменения типа контроллера
	IEnumerator GetController(){
		while (doGetController){
			controller = InputManLib.GetController(this);
			//Debug.Log(controller);
			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	void GetControls(){
		if (controller.GetAxis("starter_fast") == 1.0f){
			StartEngine();
		}

		accelAxis = controller.GetAxis("Gas");

		CallAxisDecelerator();
		CallAxisAdditionalAccelerate();
		CallAxisFeedback();
		CallAxisDecompression();
		CallAxisStarter();
		engineOff = 1.0f - controller.GetAxis("engine_stop");
	}

	void AxisIsDecelerator(){
		deceleratorAxis = controller.GetAxis("Decelerator");
		accelAxis -= deceleratorAxis * Mathf.Clamp(accelAxis - 0.2f, 0.0f, 1.0f); //ограничение (чтобы нельзя было полностью перекрыть подачу топлива)
	}

	void AxisNotDecelerator(){
		return;
	}

	void AxisIsAdditionalAccelerate(){
		accelAxis += additionalAccel;
		accelAxis = Mathf.Clamp01(accelAxis);
		additionalAccel = 0.0f;
	}

	void AxisNotAdditionalAccelerate(){
		return;
	}

	void AxisIsFeedback(){
		clutchAxis = gearBoxExtended.clutch;
	}

	void AxisNotFeedback(){
		engineTargetRPM = (engineWorkRPM - engineAutoAccelRPM) * accelAxis;
		accelAxis = 0.0f;
	}

	void AxisIsDecompression(){
		decompression = 1.0f - controller.GetAxis("Decompression") * decompressionRatio;
	}

	void AxisNotDecompression(){
		return;
	}

	void AxisIsStarter(){
		starterKey = controller.GetAxis("starter");
		currStarterTorque = engineStarterTorque * starterKey;
	}

	void AxisNotStarter(){
		return;
	}

	void Engine(){
		if (gearBox.gearRatio == 0.0f || !isFeedback){ //обороты на холостом ходу или без обратной связи
			engineAngularVelocity += engineAngularAcceleration;
		}else{ //обороты на передаче
			engineAngularVelocity += engineAngularAcceleration * (1.0f - clutchAxis) + gearBoxExtended.feedbackAngularAcceleration * fixedDeltaTime * clutchAxis;
		}

		engineRPM = engineAngularVelocity * RadiansToRPM;

		if (engineRPM < engineAutoAccelRPM + engineTargetRPM && engineOn){ //регулятор холостого хода или стремление к выбранным оборотам при (isFeedback = false)
			accelAxis += Mathf.Clamp((engineAutoAccelRPM + engineTargetRPM - engineRPM) / (engineDifferenceRPM * 2.0f), 0.0f, 1.0f - accelAxis); //добавляем газу, чтобы двигатель не заглох
		}

		accelAxis *= engineOff; //подача топлива перекрыта

		if (engineFuel <= 0.0f){
			accelAxis = 0.0f;
			engineFuel = 0.0f;
		}

		engineBrakeFinalTorque = engineBrakeTorque * temperatureKPD + Mathf.Abs(engineRPM) * engineBrakeRPMTorque; //внутреннее сопротивление двигателя
		engineBrakeFinalTorque *= Mathf.Clamp(Mathf.Abs(engineRPM) / engineStopRPM, 0.0f, 1.0f); //схождение сопротивления двигателя к нулю при приближении оборотов к нулю

		engineTorque = torqueRPM.Evaluate(engineRPM) * accelAxis - GetDirectionRotation() * engineBrakeFinalTorque; //если машина катится назад с включенной передней передачей, то сопротивление двигателя меняется на противоположное

		//при изменении скорости вращения двигателя, возникает крутящий момент, который воздействует на машину; также необходимо учесть ускорение Кориолиса
//		mainRigidbody.AddRelativeTorque(-engineOrientation * (engineTorque - engineTorqueOld));

		if (engineRPM < engineIdleRPM){ //заводим двигатель
			engineTorque += currStarterTorque;
		}

		CallTorqueOut(); //вызов TorqueOutIsFeedback или TorqueOutNotFeedback

		engineAngularAcceleration = engineTorque * fixedDeltaTime / engineInertia; //угловое ускорение вала двигателя в [рад / с^2]
	}

	IEnumerator ExtendedParameters(){
		while (doCalcParam){
			if (engineRPM >= engineStopRPM){
				engineTemperature += (workTemperature - engineTemperature) * (1.0f + accelAxis) * (engineRPM / engineIdleRPM) * speedTemperatureToWork * delayCalcParam; //нагрев до рабочей температуры
				engineFuel -= accelAxis * engineRPM / engineIdleRPM * consumptionFuel * consumptionFuelRatio / 3600.0f * delayCalcParam; //потребление топлива
				engineOilPressure += (oilMaxPressure - engineOilPressure) * engineRPM / engineIdleRPM * convergenceOil * delayCalcParam; //давление масла растёт до максимального
				engineFuelFill += (1.0f - engineFuelFill) * 0.1f * delayCalcParam;
			}else{
				engineTemperature -= (engineTemperature - environmentTemperature) * speedTemperatureToEnvironment * delayCalcParam; //двигатель остывает, если не заведён
				engineOilPressure -= engineOilPressure * convergenceOil * delayCalcParam; //давление масла падает до нуля
				engineFuelFill -= engineFuelFill * 0.02f * delayCalcParam;
			}
			temperatureKPD = Sqr(engineTemperature / workTemperature - 1.0f) + 1.0f;
//			temperatureKPD = (50.0f * workTemperature) / (Sqr(engineTemperature - workTemperature) + 50.0f * workTemperature); //альтернативный расчёт КПД от температуры

			yield return new WaitForSeconds(delayCalcParam);
		}
	}

	void TorqueOutIsFeedback(){
		engineTorqueOut = engineTorque * decompression;
	}

	void TorqueOutNotFeedback(){
		engineTorqueOut = (torqueRPM.Evaluate(engineRPM) - GetDirectionRotation() * engineBrakeFinalTorque) * decompression;
	}

	void OnEnable(){
		if (!doCalcParam && quality >= 0.5f){
			doCalcParam = true;
			StartCoroutine(ExtendedParameters());
		}
		if (!doGetController){
			doGetController = true;
			StartCoroutine(GetController());
		}
	}

	void OnDisable(){
		doCalcParam = false;
		doGetController = false;
	}

	void OnDestroy(){
		OnDisable();
	}

	public IEngine GetEngine(){
		return this;
	}
	
	public int GetDirectionRotation(){
		return (int)Mathf.Sign(engineAngularVelocity);
	}

	public float throttle{
		get{
			return accelAxis;
		}
		set{
			return;
		}
	}

	public float additionalGas{
		get{
			return additionalAccel;
		}
		set{
			additionalAccel = value;
		}
	}

	public float rpm{
		get{
			return engineRPM;
		}
		set{
			engineAngularVelocity = value * RPMToRadians;
		}
	}

	public float idleRpm{
		get{
			return engineIdleRPM;
		}
		set{
			engineIdleRPM = value;
			SetConstants();
		}
	}

	public float workRpm{
		get{
			return engineWorkRPM;
		}
		set{
			engineWorkRPM = value;
		}
	}

	public float GetMaxRPM(){
		return engineMaxRPM;
	}

	public float torque{
		get{
			return engineTorqueOut;
		}
		set{
			return;
		}
	}

	public bool engineOn{
		get{
			return engineRPM >= engineMinRPM;
		}
		set{
			return;
		}
	}

	public void StartEngine(){
		if (!engineOn){
			//Debug.Log("starter_fast");
			engineAngularVelocity = engineIdleRPM * RPMToRadians;
			engineTemperature = workTemperature;
			engineOilPressure = oilMaxPressure;
		}
	}

	public bool isRotaiting(){
		return Mathf.Abs(engineRPM) > engineStopRPM;
	}

	public float starterTorque{
		get{
			return currStarterTorque;
		}
		set{
			currStarterTorque = value;
		}
	}

	public bool starterOn{
		get{
			return currStarterTorque > 0.0f;
		}
		set{
			return;
		}
	}

	public float angularVelocity{
		get{
			return engineAngularVelocity;
		}
		set{
			return;
		}
	}

	public float inertia{
		get{
			return engineInertia;
		}
		set{
			engineInertia = value;
		}
	}

	public float temperature{
		get{
			return engineTemperature;
		}
		set{
			return;
		}
	}

	public float fuel{
		get{
			return engineFuel;
		}
		set{
			engineFuel = value;
		}
	}

	public float fuelFill{
		get{
			return engineFuelFill;
		}
		set{
			engineFuelFill = value;
		}
	}

	public float oilPressure{
		get{
			return engineOilPressure;
		}
		set{
			engineOilPressure = value;
		}
	}

	public void SetTransmissionON(bool on){
		return;
	}

	public void FeedBackRPM(float fRPM){
		return;
	}

	public float exitRPM{
		get{
			return engineRPM * gearBox.gearRatio;
		}
		set{
			return;
		}
	}

	public float GetQuality(){
		return quality;
	}

	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality == 0.0f){
				enabled = false;
			}else if (newQuality < 0.5f){
				enabled = true;
				temperatureKPD = 1.0f;
				engineFuel = startFuel;
				engineOilPressure = oilMaxPressure;
				doCalcParam = false;
			}else{
				enabled = true;
				if (!doCalcParam){
					doCalcParam = true;
					StartCoroutine(ExtendedParameters());
				}
			}
		}
	}

	public float[] GetParameters(){
		float[] parameters = new float[countParameters];
		parameters[0] = engineIdleRPM;
		parameters[1] = engineMinRPM;
		parameters[2] = engineMaxRPM;
		parameters[3] = engineWorkRPM;
		parameters[4] = engineMaxTorque;
		parameters[5] = engineBrakeTorque;
		parameters[6] = engineBrakeRPMTorque;
		parameters[7] = starterTorque;
		parameters[8] = engineInertia;
		parameters[9] = workTemperature;
		parameters[10] = speedTemperatureToWork;
		parameters[11] = startFuel;
		parameters[12] = consumptionFuel;
		parameters[13] = consumptionFuelRatio;
		parameters[14] = oilMaxPressure;
		parameters[15] = convergenceOil;
		return parameters;
	}

	public bool SetParameters(float[] parameters){
		if (parameters.Length != countParameters){
			return false;
		}

		engineIdleRPM = parameters[0];
		engineMinRPM = parameters[1];
		engineMaxRPM = parameters[2];
		engineWorkRPM = parameters[3];
		engineMaxTorque = parameters[4];
		engineBrakeTorque = parameters[5];
		engineBrakeRPMTorque = parameters[6];
		starterTorque = parameters[7];
		engineInertia = parameters[8];
		workTemperature = parameters[9];
		speedTemperatureToWork = parameters[10];
		startFuel = parameters[11];
		consumptionFuel = parameters[12];
		consumptionFuelRatio = parameters[13];
		oilMaxPressure = parameters[14];
		convergenceOil = parameters[15];

		return true;
	}
}

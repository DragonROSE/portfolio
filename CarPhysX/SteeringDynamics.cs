using UnityEngine;
using System.Collections;

/// <summary>
/// Расчёт углов поворота колёс
/// Написана по книге Steering Dynamics
/// Позволяет выбирать передние и/или задние колёса, как поворотные
/// Есть возможность задать внешние оси вращения для каждого моста
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

/*
 * Чтобы задать внешние оси вращения, необходимо:
 *   1. размерности соответствующих массивов "transform<Front/Rear><Left/Right>Wheels" и "rotate<Front/Rear><Left/Right>" должны совпадать
 *   2. указать Transform, вокруг которого происходит вращение и внутри него должна находится физика колеса (скрипт колеса)
 *   3. при необходимости можно оставлять элементы одного моста = null, в таком случае вращение колёс для данного моста будет происходить вокруг своей собственной оси (не внешней)
 */

public class SteeringDynamics : MonoBehaviour {

	public float maxSteeringAngle = 30.0f; //максимальный угол поворота колёс (внутреннее колесо) [градусы]
	public float rotateAroundPointShift = 0.0f; //сдвиг вдоль продольной оси авто точки, вокруг которой поворачивает авто [м]

	public bool isFrontSteer = true; //поворачиваются передние
	public bool isRearSteer = false; //поворачиваются задние

	public Transform[] transformFrontLeftWheels = null; //объект на котором висят передние левые колёса
	public Transform[] transformFrontRightWheels = null; //объект на котором висят передние правые колёса
	public Transform[] transformRearLeftWheels = null; //объект на котором висят задние левые колёса
	public Transform[] transformRearRightWheels = null; //объект на котором висят задние правые колёса

	public Transform[] rotateFrontLeft = null; //объект вокруг которого вращается переднее левое колесо (не обязательно)
	public Transform[] rotateFrontRight = null; //объект вокруг которого вращается переднее правое колесо (не обязательно)
	public Transform[] rotateRearLeft = null; //объект вокруг которого вращается заднее левое колесо (не обязательно)
	public Transform[] rotateRearRight = null; //объект вокруг которого вращается заднее правое колесо (не обязательно)

	float steeringAngle = 0.0f; //угол поворота колёс

	float turnRadius = 0.0f; //радиус порота
	float steerDirection = 0.0f; //направление поворота (-1 - лево, +1 - право)

	int steeringWheelsCouples = 0; //количество пар поворачивающихся колёс (количество мостов с рулевым управлением)

	///кешированные величины
	float Deg2Rad = Mathf.Deg2Rad;
	float Rad2Deg = Mathf.Rad2Deg;

	IWheel[] frontLeftWheels;
	IWheel[] frontRightWheels;
	IWheel[] rearLeftWheels;
	IWheel[] rearRightWheels;

	public Transform chassisMainTransform = null; //объект на котором висит скрипт шасси
	IChassis chassisMain = null; //интерфейс шасси

	delegate void WheelSteeringDelegate(int numCouple); //выбор на старте, вращение вокруг своей оси или вокруг заданной

	WheelData[] wheelsCouples = null;
	
	//внутренняя структура мостов рулевых колёс
	class WheelData{
		public IWheel wheelLeft = null; //левое колесо данного моста
		public IWheel wheelRight = null; //правое колесо данного моста
		public Transform rotateLeft = null; //внешняя ось вращения левого колеса данного моста (не обязательно)
		public Transform rotateRight = null; //внешняя ось вращения правого колеса данного моста (не обязательно)
		public float frontRear = 0.0f; //если передняя ось, то = 1, иначе = -1
		public float steerLeft = 0.0f; //текуший угол поворота левого колеса данного моста
		public float steerRight = 0.0f; //текуший угол поворота правого колеса данного моста
		public float oldSteerLeft = 0.0f; //предыдущее значение угла поворота левого колеса данного моста
		public float oldSteerRight = 0.0f; //предыдущее значение угла поворота правого колеса данного моста
		public float wheelLengthBase = 0.0f; //отношение расстояния между поворачивающимися колёсами к колёсной базе или половине колёсной базы (если все колёса вращаются)
		public float wheelDistance = 0.0f; //расстояние между колёсами одной оси
		public float wheelBase = 0.0f; //колёсная база для каждой оси
		public WheelSteeringDelegate CallWheelSteering;
	}

	void Start(){
		int shift = 0;

		if (transformFrontLeftWheels.Length != transformFrontRightWheels.Length || transformRearLeftWheels.Length != transformRearRightWheels.Length){
			Debug.LogError("Number of Left and Right wheels is not the same! Can not simulate steering dynamics! Script disabled.");
			enabled = false;
			return;
		}

		if (rotateFrontLeft.Length != rotateFrontRight.Length || rotateRearLeft.Length != rotateRearRight.Length ||
			(rotateFrontLeft.Length != 0 && rotateFrontLeft.Length != transformFrontLeftWheels.Length) ||
			(rotateRearLeft.Length != 0 && rotateRearLeft.Length != transformRearLeftWheels.Length)){
			Debug.LogError("Number of Left and Right wheels axes is not the same! Can not simulate steering dynamics over external axes! Script disabled.");
			enabled = false;
			return;
		}

		if (!isFrontSteer && !isRearSteer){
			isFrontSteer = true;
			Debug.LogWarning("No steering wheels was selected! Front steering selected by default.");
		}

		frontLeftWheels = CarPhysxLib.GetWheelInterface(transformFrontLeftWheels);
		frontRightWheels = CarPhysxLib.GetWheelInterface(transformFrontRightWheels);
		rearLeftWheels = CarPhysxLib.GetWheelInterface(transformRearLeftWheels);
		rearRightWheels = CarPhysxLib.GetWheelInterface(transformRearRightWheels);

		chassisMain = InterfaceLib.GetInterfaceComponent<IChassis>(chassisMainTransform);
		if (chassisMain == null){
			Debug.LogError("Chassis not found! Script disabled.");
			enabled = false;
			return;
		}

		steeringWheelsCouples = 0;
		shift = 0;
		if (isFrontSteer){
			steeringWheelsCouples += frontLeftWheels.Length;
			shift = frontLeftWheels.Length;
		}
		if (isRearSteer){
			steeringWheelsCouples += rearLeftWheels.Length;
		}

		wheelsCouples = new WheelData[steeringWheelsCouples];

		//создание структуры колёс wheelsCouples
		if (isFrontSteer){
			for (int i = 0; i < frontLeftWheels.Length; i++){
				if (rotateFrontLeft.Length <= i || rotateFrontLeft[i] == null){
					wheelsCouples[i] = SetupWheels(frontLeftWheels[i], frontRightWheels[i], null, null, SteerAroundLocal, 1.0f);
				}else{
					wheelsCouples[i] = SetupWheels(frontLeftWheels[i], frontRightWheels[i], rotateFrontLeft[i], rotateFrontRight[i], SteerAroundExternal, 1.0f);
				}
				
			}
		}
		if (isRearSteer){
			for (int i = 0; i < rearLeftWheels.Length; i++){
				if (rotateRearLeft.Length <= i || rotateRearLeft[i] == null){
					wheelsCouples[i + shift] = SetupWheels(rearLeftWheels[i], rearRightWheels[i], null, null, SteerAroundLocal, -1.0f);
				}else{
					wheelsCouples[i + shift] = SetupWheels(rearLeftWheels[i], rearRightWheels[i], rotateRearLeft[i], rotateRearRight[i], SteerAroundExternal, -1.0f);
				}
			}
		}

		//расчёт ширины колеи каждого моста
		if (isFrontSteer){
			for (int i = 0; i < frontLeftWheels.Length; i++){ //передние
				Vector3 localPosLeft = Vector3.zero;
				Vector3 localPosRight = Vector3.zero;
				if (rotateFrontLeft != null && rotateFrontRight != null && rotateFrontLeft.Length > i && rotateFrontRight.Length > i && rotateFrontLeft[i] != null && rotateFrontRight[i] != null){
					localPosLeft = transform.InverseTransformPoint(rotateFrontLeft[i].position);
					localPosRight = transform.InverseTransformPoint(rotateFrontRight[i].position);
				}else{
					localPosLeft = transform.InverseTransformPoint(frontLeftWheels[i].GetTransform().position);
					localPosRight = transform.InverseTransformPoint(frontRightWheels[i].GetTransform().position);
				}
				wheelsCouples[i].wheelDistance = Vector3.Distance(localPosLeft, localPosRight);
			}
		}
		if (isRearSteer){
			for (int i = 0; i < rearLeftWheels.Length; i++){ //задние
				Vector3 localPosLeft = Vector3.zero;
				Vector3 localPosRight = Vector3.zero;
				if (rotateRearLeft != null && rotateRearRight != null && rotateRearLeft.Length > i && rotateRearRight.Length > i && rotateRearLeft[i] != null && rotateRearRight[i] != null){
					localPosLeft = transform.InverseTransformPoint(rotateRearLeft[i].position);
					localPosRight = transform.InverseTransformPoint(rotateRearRight[i].position);
				}else{
					localPosLeft = transform.InverseTransformPoint(rearLeftWheels[i].GetTransform().position);
					localPosRight = transform.InverseTransformPoint(rearRightWheels[i].GetTransform().position);
				}
				wheelsCouples[i + shift].wheelDistance = Vector3.Distance(localPosLeft, localPosRight);
			}
		}

		//расчёт точки вращения (проекция точки, вокруг которой вращается авто при повороте, на продольную ось симметрии авто)
		Vector3 averageFrontWheelsPos = Vector3.zero;
		Vector3 averageRearWheelsPos = Vector3.zero;
		Vector3 centerTurnAround = Vector3.zero;
		for (int i = 0; i < frontLeftWheels.Length; i++){
			averageFrontWheelsPos += transform.InverseTransformPoint(frontLeftWheels[i].GetTransform().position);
		}
		for (int i = 0; i < frontRightWheels.Length; i++){
			averageFrontWheelsPos += transform.InverseTransformPoint(frontRightWheels[i].GetTransform().position);
		}
		for (int i = 0; i < rearLeftWheels.Length; i++){
			averageRearWheelsPos += transform.InverseTransformPoint(rearLeftWheels[i].GetTransform().position);
		}
		for (int i = 0; i < rearRightWheels.Length; i++){
			averageRearWheelsPos += transform.InverseTransformPoint(rearRightWheels[i].GetTransform().position);
		}
		averageFrontWheelsPos /= frontLeftWheels.Length + frontRightWheels.Length;
		averageRearWheelsPos /= rearLeftWheels.Length + rearRightWheels.Length;

		if (isFrontSteer && isRearSteer){
			centerTurnAround = (averageFrontWheelsPos + averageRearWheelsPos) / 2.0f;
		}else if (isFrontSteer){
			centerTurnAround = averageRearWheelsPos;
		}else{
			centerTurnAround = averageFrontWheelsPos;
		}
		centerTurnAround += Vector3.forward * rotateAroundPointShift;

		//расчёт отношений расстояния между осями к колёсной базе
		for (int i = 0; i < steeringWheelsCouples; i++){
			Vector3 localPosLeft = transform.InverseTransformPoint(wheelsCouples[i].wheelLeft.GetTransform().position);
			Vector3 localPosRight = transform.InverseTransformPoint(wheelsCouples[i].wheelRight.GetTransform().position);
			wheelsCouples[i].wheelBase = Vector3.Distance((localPosLeft + localPosRight) / 2.0f, centerTurnAround);
			wheelsCouples[i].wheelLengthBase = wheelsCouples[i].wheelDistance / wheelsCouples[i].wheelBase;
			wheelsCouples[i].wheelDistance /= 2.0f; //для дальнейшей оптимизации формулы tg(a) = WheelBase / (TurnRadius - WheelDistance / 2)
		}
	}

	void FixedUpdate(){
		steeringAngle = chassisMain.steering * maxSteeringAngle;
		steerDirection = Mathf.Sign(steeringAngle);

		for (int i = 0; i < steeringWheelsCouples; i++){
			wheelsCouples[i].oldSteerLeft = wheelsCouples[i].steerLeft;
			wheelsCouples[i].oldSteerRight = wheelsCouples[i].steerRight;
		}

		//расчёт угла поворота колёс первого моста по формуле ctg(a) = ctg(b) + WheelDistance / WheelBase
		if (steeringAngle > 0.0f){
			wheelsCouples[0].steerRight = wheelsCouples[0].frontRear * steeringAngle;
			wheelsCouples[0].steerLeft = wheelsCouples[0].frontRear * 90.0f - Mathf.Atan(1.0f / Mathf.Tan(wheelsCouples[0].steerRight * Deg2Rad) + wheelsCouples[0].frontRear * wheelsCouples[0].wheelLengthBase) * Rad2Deg; //wheelsCouples[0].frontRear * 
		}else if (steeringAngle < 0.0f){
			wheelsCouples[0].steerLeft = wheelsCouples[0].frontRear * steeringAngle;
			wheelsCouples[0].steerRight = wheelsCouples[0].frontRear * -90.0f - Mathf.Atan(1.0f / Mathf.Tan(wheelsCouples[0].steerLeft * Deg2Rad) - wheelsCouples[0].frontRear * wheelsCouples[0].wheelLengthBase) * Rad2Deg;
		}else{
			wheelsCouples[0].steerLeft = 0.0f;
			wheelsCouples[0].steerRight = 0.0f;
		}

		wheelsCouples[0].CallWheelSteering(0);

		//расчёт углов поворота колёс остальных мостов по формуле tg(a) = WheelBase / (TurnRadius - WheelDistance / 2)
		for (int i = 1; i < steeringWheelsCouples; i++){
			turnRadius = wheelsCouples[0].wheelBase / Mathf.Tan(steeringAngle * Deg2Rad) + steerDirection * wheelsCouples[0].wheelDistance;
			wheelsCouples[i].steerLeft = wheelsCouples[i].frontRear * Mathf.Atan(wheelsCouples[i].wheelBase / (turnRadius + wheelsCouples[i].wheelDistance)) * Rad2Deg;
			wheelsCouples[i].steerRight = wheelsCouples[i].frontRear * Mathf.Atan(wheelsCouples[i].wheelBase / (turnRadius - wheelsCouples[i].wheelDistance)) * Rad2Deg;

			wheelsCouples[i].CallWheelSteering(i);
		}
	}

	WheelData SetupWheels(IWheel wheelLeft, IWheel wheelRight, Transform rotateLeft, Transform rotateRight, WheelSteeringDelegate callWheelSteering, float frontRear){
		WheelData result = new WheelData();
		result.wheelLeft = wheelLeft;
		result.wheelRight = wheelRight;
		result.rotateLeft = rotateLeft;
		result.rotateRight = rotateRight;
		result.CallWheelSteering = callWheelSteering;
		result.frontRear = frontRear;
		return result;
	}

	void SteerAroundLocal(int numCouple){
		wheelsCouples[numCouple].wheelLeft.steering = wheelsCouples[numCouple].steerLeft;
		wheelsCouples[numCouple].wheelRight.steering = wheelsCouples[numCouple].steerRight;
	}

	void SteerAroundExternal(int numCouple){
		wheelsCouples[numCouple].rotateLeft.localRotation = Quaternion.Euler(0.0f, wheelsCouples[numCouple].steerLeft, 0.0f);
		wheelsCouples[numCouple].rotateRight.localRotation = Quaternion.Euler(0.0f, wheelsCouples[numCouple].steerRight, 0.0f);
		wheelsCouples[numCouple].wheelLeft.deltaSteering = wheelsCouples[numCouple].steerLeft - wheelsCouples[numCouple].oldSteerLeft;
		wheelsCouples[numCouple].wheelRight.deltaSteering = wheelsCouples[numCouple].steerRight - wheelsCouples[numCouple].oldSteerRight;
	}
}

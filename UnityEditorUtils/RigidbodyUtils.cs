using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Расширение UnityEditor
/// Утилиты для Rigidbody
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class RigidbodyUtils : ScriptableObject{

	const string centerMassName = "CenterMass_FoundedByScript_";

	//Находит центр масс и создаёт на его месте пустой GameObject
	//Корректный расчёт центра масс возможен только в PlayMode
	[MenuItem("Tools/Rigidbody/Find Center Mass Selected Rigidbodys with children")]
	static void FindCenterMassChildren(){
		if (!Application.isPlaying){
			Debug.LogError("Correct calculation of CenterMass is only possible in PlayMode!");
			return;
		}

		List<Rigidbody> allRigidbodys = GetAllRigidbodysInChildren();

		if (allRigidbodys.Count == 0){
			Debug.LogError("No Rigidbodys in selection!");
			return;
		}

		float sumMass = 0.0f;
		Vector3 massMultiplyRigidbody = Vector3.zero;

		foreach (Rigidbody currRigidbody in allRigidbodys){
			sumMass += currRigidbody.mass;
			massMultiplyRigidbody += currRigidbody.mass * (currRigidbody.transform.position + currRigidbody.transform.rotation * currRigidbody.centerOfMass); //домножение на "currRigidbody.transform.rotation" т.к. "currRigidbody.centerOfMass" в локальных координатах, а не глобальных
		}

		if (sumMass != 0.0f){
			massMultiplyRigidbody /= sumMass;
			GameObject centerMass = new GameObject(centerMassName + "SumMass=" + sumMass.ToString());
			centerMass.transform.position = massMultiplyRigidbody;
			Selection.activeObject = centerMass;
			Debug.Log("Mass = " + sumMass.ToString() + ", centerOfMass = " + massMultiplyRigidbody.ToString(), centerMass);
		}else{
			Debug.LogError("Can't find CenterMass! SummMass = 0");
		}
	}

	//Находит центр масс и создаёт на его месте пустой GameObject
	//Корректный расчёт центра масс возможен только в PlayMode
	[MenuItem("Tools/Rigidbody/Find Center Mass Selected Rigidbody")]
	static void FindCenterMassThis(){
		if (!Application.isPlaying){
			Debug.LogError("Correct calculation of CenterMass is only possible in PlayMode!");
			return;
		}

		if (Selection.activeGameObject == null){
			Debug.LogError("GameObject is not selected!");
			return;
		}
		if (Selection.activeGameObject.rigidbody == null){
			Debug.LogError("Rigidbody not found on selected GameObject!");
			return;
		}

		Transform selectedTransform = Selection.activeGameObject.transform;
		Rigidbody selectedRigidbody = Selection.activeGameObject.rigidbody;
		GameObject centerMass = new GameObject(centerMassName + "Mass=" + selectedRigidbody.mass.ToString());
		centerMass.transform.position = selectedTransform.position + selectedTransform.rotation * selectedRigidbody.centerOfMass;
		Selection.activeObject = centerMass;
		Debug.Log("Mass = " + selectedRigidbody.mass.ToString() + ", centerOfMass = " + centerMass.transform.position.ToString(), centerMass);
	}

	//Сумма масс выделенных Rigidbody
	[MenuItem("Tools/Rigidbody/Summ Mass Selected Rigidbodys with children")]
	static void SummMassChildren(){
		List<Rigidbody> allRigidbodys = GetAllRigidbodysInChildren();

		if (allRigidbodys.Count == 0){
			Debug.LogError("No Rigidbodys in selection!");
			return;
		}

		float sumMass = 0.0f;

		foreach (Rigidbody currRigidbody in allRigidbodys){
			sumMass += currRigidbody.mass;
		}

		Debug.Log("Summary mass of selected Rigidbodys with children = " + sumMass.ToString());
	}

	static List<Rigidbody> GetAllRigidbodysInChildren(){
		GameObject[] selectedGameObjects = Selection.gameObjects;

		List<Rigidbody> allRigidbodys = new List<Rigidbody>();

		foreach (GameObject currGameObject in selectedGameObjects){
			Rigidbody[] rigidbodys = currGameObject.GetComponentsInChildren<Rigidbody>();
			allRigidbodys.AddRange(rigidbodys);
		}

		ListLib.RemoveDublicates<Rigidbody>(ref allRigidbodys);

		return allRigidbodys;
	}
}

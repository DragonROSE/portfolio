# Portfolio

Portfolio of programmer Aleksey Usanov

# Projects description

## CarPhysX

Vehicle physics for [Unity3D](https://unity.com/).
Include: wheel physics, engine, gearbox and chassis realization.

## ParserNaturalEarthData

Parser for [NaturalEarthData](https://www.naturalearthdata.com).
Converts DBF & SHP format to CSV.

## ProjectorControl

Simple application for control Panasonic projectors via ethernet.

## UnityEditorUtils

Some utils for [Unity3D](https://unity.com/) editor.

## Ventuz

Modules for [Ventuz](https://www.ventuz.com/).

## Windows & Linux shell

Windows & Linux shell scripts for automatization work processes.

# Images & Screenshots

[Images](https://gitlab.com/DragonROSE/portfolio/-/tree/main/Images)

# Video demonstration

[Aleksey Usanov Showreel](https://youtu.be/Lxe07hLRIBQ)

[Demonstration of an interactive floor](https://youtu.be/Q4VMB2qh0F4)

[Realtime 3D Mapping (OptiTrack + d3)](https://vimeo.com/206033484)

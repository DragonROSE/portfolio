﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Ventuz.Kernel;

/// <summary>
/// MoneyDevice Microcoin SP
/// Protocol: RS232 CCTalk
/// </summary>
/// <para> Company: United 3D Labs </para>
/// <para> Author: Aleksey Usanov </para>

public class Script : ScriptBase, System.IDisposable
{
	// This member is used by the Validate() method to indicate
	// whether the Generate() method should return true or false
	// during its next execution.
	private bool changed;

	private const string MODULE_NAME = "MicroCoinSPController";

	private const string MICROCOIN_IP_ADDRESS = "127.0.0.1";
	private const string MICROCOIN_COMMAND_CONNECT = "CONNECT COM";
	private const string MICROCOIN_COMMAND_START = "START";
	private const string MICROCOIN_COMMAND_STOP = "STOP";
	private const string MICROCOIN_COMMAND_CLOSE = "CLOSE";
	private const string MICROCOIN_ANSWER_OK = "OK";
	private const string MICROCOIN_ANSWER_COIN = "COIN";
	private const int port = 5000;

	private double secondsActivated;
	private int comPort;

	private bool enabled = false; //if disabled, then controls are always enabled (Microcoin disabled)
	private bool coinEnabled = false;

	private DateTime dateTimeStart = DateTime.Now;

	private Socket clientSocket = null;

	private byte[] buffer = new byte[256];

	// This Method is called if the component is loaded/created.
	public Script()
	{
		// Note: Accessing input or output properties from this method
		// will have no effect as they have not been allocated yet.
	}

	// This Method is called if the component is unloaded/disposed
	public virtual void Dispose()
	{
		//		SendData(MICROCOIN_COMMAND_CLOSE);
		//		clientSocket.Close();
	}

	// This Method is called if an input property has changed its value
	public override void Validate()
	{
		// Remember: set changed to true if any of the output 
		// properties has been changed, see Generate()
	}

	// This Method is called every time before a frame is rendered.
	// Return value: if true, Ventuz will notify all nodes bound to this
	//               script node that one of the script's outputs has a
	//               new value and they therefore need to validate. For
	//               performance reasons, only return true if output
	//               values really have been changed.
	public override bool Generate()
	{
		if (enabled)
		{
			TimeSpan deltaTime = DateTime.Now - dateTimeStart;
			if (!coinEnabled && deltaTime.TotalSeconds > secondsActivated)
			{
				Activated = false;
				changed = true;

				SendData(MICROCOIN_COMMAND_START);
				coinEnabled = true;
			}
		}

		if (changed)
		{
			changed = false;
			return true;
		}

		return false;
	}

	// This Method is called if the function/method Init is invoked by the user or a bound event.
	// Return true, if this component has to be revalidated!
	public bool OnInit(int arg)
	{
		string strFileConfig = ReadTextFile(FileConfig);
		MicrocoinConfig config = ParseConfig(strFileConfig);

		enabled = config.enabled;
		secondsActivated = (double)config.secondsEnabled;
		comPort = config.comPort;

		if (enabled)
		{
			Activated = false;

			if (clientSocket == null)
			{
				clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			}

			try
			{
				IPAddress server = IPAddress.Parse(MICROCOIN_IP_ADDRESS);
				clientSocket.BeginConnect(server, port, new AsyncCallback(ConnectCallback), clientSocket);
			}
			catch (SocketException e)
			{
				DisableMicrocoin(e.Message);
			}
		}
		else
		{
			Activated = true;
		}

		changed = true;

		return false;
	}

	public void ConnectCallback(IAsyncResult ar)
	{
		if (clientSocket.Connected)
		{
			SendData(MICROCOIN_COMMAND_CONNECT + comPort.ToString());
			clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
		}
		else
		{
			DisableMicrocoin("Failed connect to Coin Acceptor server");
		}
	}

	public void SendData(String str)
	{
		byte[] data = System.Text.Encoding.UTF8.GetBytes(str);
		SocketAsyncEventArgs socketAsyncData = new SocketAsyncEventArgs();
		socketAsyncData.SetBuffer(data, 0, data.Length);
		clientSocket.SendAsync(socketAsyncData);
	}

	private void ReceiveCallback(IAsyncResult AR)
	{
		int recieved = 0;

		try
		{
			recieved = clientSocket.EndReceive(AR);
		}
		catch (SocketException e)
		{
			DisableMicrocoin(e.Message);
		}

		if (recieved <= 0)
		{
			return;
		}

		byte[] recData = new byte[recieved];
		Buffer.BlockCopy(buffer, 0, recData, 0, recieved);
		clientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), null);
		String stringRecData = System.Text.Encoding.UTF8.GetString(recData);

		if (stringRecData.Contains(MICROCOIN_ANSWER_OK))
		{
			if (!coinEnabled && !Activated)
			{
				SendData(MICROCOIN_COMMAND_START);
				coinEnabled = true;
			}
		}
		else if (stringRecData.Contains(MICROCOIN_ANSWER_COIN))
		{
			OnCoinInserted(0);
			SendData(MICROCOIN_COMMAND_STOP);
			coinEnabled = false;
			Activated = true;
		}
		else
		{
			VLog.Error(MODULE_NAME, "Microcoin Error: " + stringRecData, VPopup.Never);
		}
	}

	private MicrocoinConfig ParseConfig(string config)
	{
		MicrocoinConfig result = new MicrocoinConfig();

		string[] lines = Regex.Split(config, "\r\n|\r|\n");

		foreach (string line in lines)
		{
			if (string.IsNullOrEmpty(line) || line[0] == '#')
			{
				continue;
			}

			string[] param = line.Split('=');

			if (param.Length == 2)
			{
				param[0] = param[0].ToLower();
				param[1] = param[1].ToLower();

				if (param[0] == "enabled")
				{
					if (param[1] == "true")
					{
						result.enabled = true;
					}
					else
					{
						result.enabled = false;
					}
				}
				else if (param[0] == "comport")
				{
					if (!int.TryParse(param[1], out result.comPort))
					{
						VLog.Error(MODULE_NAME, "Parse com port in ini file failed!", VPopup.Never);
					}
				}
				else if (param[0] == "secondsactivated")
				{
					if (!int.TryParse(param[1], out result.secondsEnabled))
					{
						VLog.Error(MODULE_NAME, "Parse SecondsEnabled in ini file failed!", VPopup.Never);
					}
				}
			}
		}

		return result;
	}

	private string ReadTextFile(string fileName)
	{
		string result = string.Empty;
		string workDir = Scene.WorkingDirectory;

		try
		{
			using (StreamReader sr = new StreamReader(workDir + "\\" + fileName))
			{
				result = sr.ReadToEnd();
			}
		}
		catch (Exception e)
		{
			VLog.Error(MODULE_NAME, e.Message, VPopup.Never);
		}

		return result;
	}

	// This Method is called if the function/method DEBUG_CoinInserted is invoked by the user or a bound event.
	// Return true, if this component has to be revalidated!
	public bool OnCoinInserted(int arg)
	{
		dateTimeStart = DateTime.Now;
		Activated = true;

		SendMsgCoin();

		changed = true;

		return false;
	}
	
	// This Method is called if the function/method Close is invoked by the user or a bound event.
	// Return true, if this component has to be revalidated!
	public bool OnClose(int arg)
	{
		SendData(MICROCOIN_COMMAND_CLOSE);
		clientSocket.Close();
		clientSocket = null;

		return false;
	}

	private void DisableMicrocoin(string msg)
	{
		enabled = false;
		Activated = true; //microcoin doesn't work, enable contols (joystick and touch)
		VLog.Error(MODULE_NAME, msg, VPopup.Never);
	}
}

public struct MicrocoinConfig
{
	public bool enabled;
	public int comPort;
	public int secondsEnabled;
}

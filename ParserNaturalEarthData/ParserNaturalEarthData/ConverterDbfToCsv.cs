﻿using System;
using System.IO;
using System.Text;

public static class ConverterDbfToCsv
{
	const int SIZE_FILE_HEADER_DBF = 32;
	const int SIZE_FILE_HEADER_RECORD_DBF = 32;
	const long SIZE_FILE_HEADER_DBF_1 = 4;
	const long SIZE_FILE_HEADER_DBF_2 = 20;
	const long SIZE_FILE_HEADER_RECORD_DBF_1 = 5;
	const long SIZE_FILE_HEADER_RECORD_DBF_2 = 15;

	const string COLUMN_NAME_ID_COUNTRY = "ID_COUNTRY";

	const int LENGTH_END_HEADER_MARKER = 1;
	const int LENGTH_DELETION_MARKER = 1;
	const int LENGTH_CRLF = 2;
	const byte BYTE_SPACE = 0x20;

	public static bool ConvertDbfToCsv(string filepathDbf, char delimiter, char delimiterReplace, ref int outLength, ref byte[] outBytes)
	{
		try
		{
			using (BinaryReader reader = new BinaryReader(File.Open(filepathDbf, FileMode.Open)))
			{
				//skip *.dbf header
				reader.BaseStream.Position += SIZE_FILE_HEADER_DBF_1;

				int countRows = (int)reader.ReadUInt32();
				int sizeHeader = (int)reader.ReadUInt16();
				int lenRecord = (int)reader.ReadUInt16();

				//skip *.dbf header
				reader.BaseStream.Position += SIZE_FILE_HEADER_DBF_2;

				int countColumns = (sizeHeader - LENGTH_END_HEADER_MARKER - SIZE_FILE_HEADER_DBF) / SIZE_FILE_HEADER_RECORD_DBF;

				int[] lenFields = new int[countColumns];

				StringBuilder header = new StringBuilder();

				header.Append(COLUMN_NAME_ID_COUNTRY).Append(delimiter);

				//Field descriptors
				for (int i = 0; i < countColumns; ++i)
				{
					string fieldName = Encoding.ASCII.GetString(reader.ReadBytes(11)).TrimEnd((char)0x00);
					fieldName = fieldName.Replace(delimiter, delimiterReplace);

					header.Append(fieldName);

					//don't insert last delimiter
					if (i < countColumns - 1)
					{
						header.Append(delimiter);
					}

					//skip *.dbf header record
					reader.BaseStream.Position += SIZE_FILE_HEADER_RECORD_DBF_1;

					lenFields[i] = (int)reader.ReadByte();

					//skip *.dbf header record
					reader.BaseStream.Position += SIZE_FILE_HEADER_RECORD_DBF_2;
				}

				reader.BaseStream.Position += LENGTH_END_HEADER_MARKER;

				//allocate memory buffer for output data
				outBytes = new byte[
					COLUMN_NAME_ID_COUNTRY.Length + 1 + //first column + 1 char delimiter
					header.Length + LENGTH_CRLF + //header + new line
					countRows * (lenRecord - LENGTH_DELETION_MARKER + LENGTH_CRLF + countColumns)]; //table with all data, delimiters, new lines

				outLength = 0;

				byte[] byteHeader = Encoding.ASCII.GetBytes(header.ToString());
				byteHeader.CopyTo(outBytes, outLength);

				outLength += byteHeader.Length;

				outBytes[outLength + 0] = 0x0D; //symbol CR
				outBytes[outLength + 1] = 0x0A; //symbol LF

				outLength += 2;

				byte byteDelimiter = (byte)delimiter;
				byte byteDelimiterReplace = (byte)delimiterReplace;

				for (int iRow = 0; iRow < countRows; ++iRow)
				{
					//check "Deletion marker"
					if (reader.ReadByte() == 0x2A)
					{
						continue;
					}

					//add row number
					byte[] bytesNumRow = Encoding.ASCII.GetBytes((iRow + 1).ToString() + delimiter);
					Array.Copy(bytesNumRow, 0, outBytes, outLength, bytesNumRow.Length);
					outLength += bytesNumRow.Length;

					for (int iCol = 0; iCol < countColumns; ++iCol)
					{
						byte[] cell = reader.ReadBytes(lenFields[iCol]);

						int indexStart = 0;
						int indexStop = 0;
						bool foundSpaceStart = false;
						bool foundSpaceStop = false;

						for (int i = 0; i < cell.Length; ++i)
						{
							//repalce Delimiter to DelimiterReplace
							if (cell[i] == byteDelimiter)
							{
								cell[i] = byteDelimiterReplace;
							}

							//trim spaces at begin
							if (!foundSpaceStart && cell[i] != BYTE_SPACE)
							{
								foundSpaceStart = true;
								indexStart = i;
							}
						}

						//trim spaces at end
						for (int i = cell.Length - 1; i >= 0; --i)
						{
							if (!foundSpaceStop && cell[i] != BYTE_SPACE)
							{
								foundSpaceStop = true;
								indexStop = i;
							}
						}

						int lengthString = indexStop - indexStart + 1;

						//cell contains real data
						if (foundSpaceStart && foundSpaceStop)
						{
							Array.Copy(cell, indexStart, outBytes, outLength, lengthString);
							outLength += lengthString;
						}

						//don't insert last delimiter
						if (iCol < countColumns - 1)
						{
							outBytes[outLength] = byteDelimiter;
							++outLength;
						}
					}

					//don't insert last CR LF at the end of file
					if (iRow < countRows - 1)
					{
						outBytes[outLength + 0] = 0x0D; //symbol CR
						outBytes[outLength + 1] = 0x0A; //symbol LF

						outLength += 2;
					}
				}
			}
		}
		catch (Exception e)
		{
#if DEBUG
			Console.WriteLine(e.ToString());
#else
			Console.WriteLine(e.Message);
#endif
			return false;
		}

		return true;
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс шасси вагона/локомотива
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IChassisWagon {

	/// <summary>Возвращает линейную скорость вращения колёс в [км/ч]</summary>
	float GetLinearSpeed();

	/// <summary>Возвращает линейную скорость вращения колёс в [м/с]</summary>
	float GetLinearSpeedms();

	/// <summary>Возвращает скорость вращения колёс в [рад/с]</summary>
	float GetAverageAngularVelocity();

	/// <summary>Возвращает векторную скорость объекта [м/с]</summary>
	Vector3 GetSpeed();

	/// <summary>Позиция вагона/локомотива в глобальных координатах</summary>
	Vector3 GetPosition();

}

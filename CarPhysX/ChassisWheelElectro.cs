using UnityEngine;
using System.Collections;

/// <summary>
/// Реализация колёсной физики шасси, все величины вычисляются в системе "СИ"
/// Общение с колёсами происходит через интерфейс IWheel
/// Реализовано два различных тормоза: тормозные колодки и динамическое торможение
/// Реализована система аварийного торможения двигателем при превышении скорости
/// Реализовано повреждение тормозной системы при перегреве тормозных колодок
/// Для общения с другими классами используется интерфейс I_Chassis
/// Для передачи крутящего момента на колёса используется интерфейс коробки передач I_GearBox
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class ChassisWheelElectro : MonoBehaviour, IChassis, IQuality{

	const float getControllerDelay = 0.5f;

	public Transform[] transformFrontLeftWheels = null; //объект на котором висят передние левые колёса
	public Transform[] transformFrontRightWheels = null; //объект на котором висят передние правые колёса
	public Transform[] transformRearLeftWheels = null; //объект на котором висят задние левые колёса
	public Transform[] transformRearRightWheels = null; //объект на котором висят задние правые колёса

	public float maxSteeringAngle = 30.0f; //максимальный угол поворота колёс [градусы]
	public float steerSpeed = 0.1f; //коэффициент скорости поворота руля, если задействована система дополнительного контроля (additionalControl)
	public float maxSpeed = 50.0f; //максимальный скорость [км/ч]
	public float emergencyBrakeSpeed = 40.0f; //скорость при которой срабатывает аварийное торможение [км/ч]
	public float additionalBrakeTorque = 1000.0f; //дополнительное торможение при торможении двигателем [н*м]
	public float brakeAdditionalGas = 0.5f; //положение педали газа при динамическом торможении

	public float brakeTemperatureMax = 500.0f; //температура перегрева тормозных колодок
	public float speedBrakeTemperatureToMax = 1.0f; //скорость схождения к максимальной температуре
	public float speedBrakeTemperatureToEnvironment = 0.0005f; //скорость схождения к температуре окружающей среды
	public float speedBrakeDamage = 0.001f; //скорость повреждения тормозных колодок
	public float additionalControlRatioSpeed = 0.1f; //скорость падения коэффициента в системе дополнительного контроля, если задействована система дополнительного контроля (additionalControl)
	public float brakeEngineEfficiency = 1.0f; //эффективность торможения двигателем
	public float accelDelay = 1.0f; //"инертность" хода педали газа

	public bool isEmergencyBrake = false; //аварийное торможение, при превышении скорости
	public bool isDamageableBrake = false; //тормоза перегреваются и повреждаются (снижается эффективность торможения)
	public bool isAccelConvergence = false; //наличие "инертности" на педали газа
	public bool isFrontDrive = false; //передние ведущие
	public bool isRearDrive = true; //задние ведущие
	public bool setStartValuesFromController = true; //заполнение осей на старте с контроллера
	public enumCruiseConrol cruiseControl = enumCruiseConrol.None; //тип выбранного круиз контроля

	public enum enumCruiseConrol { None, GasOnly, BrakeOnly, GasAndBrake } //тип круиз контроля

	//кэшированные величины
	float oldSteer = 0.0f;
	float steerTarget = 0.0f;
	float oldSteerTarget = 0.0f;
	float wheelRadius = 1.0f;
	float maxAngularVelocity = 1.0f;
	float averageAngularVelocity = 0.0f;
	float emergencyAngularVelocity = 0.0f;
	float brakeEfficiency = 1.0f;
	float brakeTemperature = 0.0f;
	float oldBrake = 0.0f;
	float oldHandbrake = 1.0f; //по умолчанию ручник включен (затянут)
	float cruiseControlSpeed = 0.0f;
	float accelConvergence = 0.0f;

	//оси с контроллера
	float steerAxis = 0.0f; //угол поворота колёс с органов управления (от -1 до 1)
	float accelAxis = 0.0f; //педаль газа (от 0 до 1)
	float brakeAxis = 0.0f; //степень торможения (от 0 до 1)
	float brakeEngineAxis = 0.0f; //степень торможения двигателем (от 0 до 1)
	float handbrakeAxis = 1.0f; //ручник (от 0 до 1), по умолчанию ручник включен (затянут)

//	public Transform controllerTransform = null; //объект на котором висит контроллер
	private IInputMan controller = null; //данные с органов управления

	public Transform gearBoxTransform = null; //объект на котором висит скрипт КПП
	private IGearBox gearBox = null; //интерфейс КПП
	public Transform engineMainTransform = null; //объект на котором висит скрипт двигателя
	private IEngine engineMain = null; //интерфейс двигателя

	public Transform additionalControlTransform = null;
	private IAdditionalControl additionalControl = null;

//	float rpmToRad = (2.0f * Mathf.PI) / 60.0f; //перевод из оборотов в минуту в радианы в секунду
//	float radToRpm = 60.0f / (2.0f * Mathf.PI); //перевод из радиан в секунду в обороты в минуту
	float msTokmh = 3600.0f / 1000.0f; //перевод из метры в секунду в километры в час
	float radsecTokmh = 1.0f;

	float avgTorque = 0.0f;
	float driveTorque = 0.0f;
	float brakeTorque = 0.0f;

	float quality = 1.0f;

	int driveWheelsCount = 0; //количество ведущих колёс

	bool doGetController = true;

//	bool emergencyBrake = false;

	IWheel[] frontLeftWheels = null;
	IWheel[] frontRightWheels = null;
	IWheel[] rearLeftWheels = null;
	IWheel[] rearRightWheels = null;
	IWheel[] wheelsBrake = null;
	IWheel[] wheelsDrive = null;

	AnimationCurve torqueElectro = null;

	//кривая крутящего момента электродвигателя
	float[] axisCurveX = { 0.0f,  0.1f,  0.2f,  0.3f,  0.4f,  0.5f, 0.6f,  0.7f, 0.8f,  0.9f,  1.0f };
	float[] axisCurveY = { 0.95f, 0.75f, 0.73f, 0.75f, 0.82f, 0.9f, 0.97f, 1.0f, 0.97f, 0.75f, 0.0f };

	delegate void SpeedRestrictionDelegate(); //выбор на старте, срабатывает ли аварийное торможение при превышении скорости
	SpeedRestrictionDelegate CallSpeedRestriction;
	SpeedRestrictionDelegate StartSpeedRestriction;
	delegate void DamageableBrakeDelegate(); //выбор на старте, снижается или нет эффективность торможения
	DamageableBrakeDelegate CallDamageableBrake;
	DamageableBrakeDelegate StartDamageableBrake;
	delegate void AdditionalControlDelegate();
	AdditionalControlDelegate CallAdditionalControl;
	delegate void CruiseControlDelegate();
	CruiseControlDelegate CallCruiseControl;
	CruiseControlDelegate StartCruiseControl;
	delegate void AccelConvergenceDelegate();
	AccelConvergenceDelegate CallAccelConvergence;
	AccelConvergenceDelegate StartAccelConvergence;

//	void OnGUI(){
//		GUI.Label(new Rect(10.0f, 10.0f, 200.0f, 25.0f), "accelAxis = " + accelAxis.ToString("N2"));
//		GUI.Label(new Rect(10.0f, 30.0f, 200.0f, 25.0f), "avgAngVel = " + averageAngularVelocity.ToString("N2"));
//	}

	void Start(){
		GetInterfaces();
		SetConstants();
		SetCurve();
		StartCoroutine(GetControlsStart());
//		StartCoroutine(CheckTag());
	}

	void FixedUpdate(){
		GetControls();
		CallAdditionalControl();
		CalcAverageAngularVelocity();
		CallSpeedRestriction();
		CarMove();
	}

	void GetInterfaces(){
		//controller = InterfaceLib.GetInterfaceComponent<IInputMan>(controllerTransform);
		gearBox = InterfaceLib.GetInterfaceComponent<IGearBox>(gearBoxTransform);
		if (engineMainTransform != null){
			engineMain = InterfaceLib.GetInterfaceComponent<IEngine>(engineMainTransform);
		}
		if (additionalControlTransform != null){
			additionalControl = InterfaceLib.GetInterfaceComponent<IAdditionalControl>(additionalControlTransform);
		}

		if (transformFrontLeftWheels.Length == 0 || transformFrontRightWheels.Length == 0 || transformRearLeftWheels.Length == 0 || transformRearRightWheels.Length == 0){
			Debug.LogError("Wheels not found! Script disabled.");
			enabled = false;
			return;
		}

		frontLeftWheels = CarPhysxLib.GetWheelInterface(transformFrontLeftWheels);
		frontRightWheels = CarPhysxLib.GetWheelInterface(transformFrontRightWheels);
		rearLeftWheels = CarPhysxLib.GetWheelInterface(transformRearLeftWheels);
		rearRightWheels = CarPhysxLib.GetWheelInterface(transformRearRightWheels);
	}

	void SetConstants(){
		if (isFrontDrive){
			wheelRadius = frontLeftWheels[0].wheelRadius;
		}else if (isRearDrive){
			wheelRadius = rearLeftWheels[0].wheelRadius;
		}else{
			wheelRadius = (frontLeftWheels[0].wheelRadius + rearLeftWheels[0].wheelRadius) / 2.0f;
		}

		radsecTokmh = wheelRadius * msTokmh;
		emergencyAngularVelocity = emergencyBrakeSpeed / msTokmh / wheelRadius;

		if (!isFrontDrive && !isRearDrive){
			Debug.LogWarning("Drive wheels wasn't set");
		}

		if (isEmergencyBrake){
			CallSpeedRestriction = SpeedIsRestricted;
		}else{
			CallSpeedRestriction = SpeedNotRestricted;
		}
		StartSpeedRestriction = CallSpeedRestriction;

		if (isDamageableBrake){
			CallDamageableBrake = BrakeIsDamageable;
		}else{
			CallDamageableBrake = BrakeNotDamageable;
		}
		StartDamageableBrake = CallDamageableBrake;

		if (additionalControl == null){
			CallAdditionalControl = ChassisNotAdditionalControl;
		}else{
			CallAdditionalControl = ChassisIsAdditionalControl;
		}

		if (cruiseControl == enumCruiseConrol.GasOnly){
			CallCruiseControl = ChassisGasCruiseControl;
		}else if (cruiseControl == enumCruiseConrol.BrakeOnly){
			CallCruiseControl = ChassisBrakeCruiseControl;
		}else if (cruiseControl == enumCruiseConrol.GasAndBrake){
			CallCruiseControl = ChassisGasBrakeCruiseControl;
		}else{
			CallCruiseControl = ChassisNotCruiseControl;
		}
		StartCruiseControl = CallCruiseControl;

		if (isAccelConvergence){
			CallAccelConvergence = AccelIsConvergence;
		}else{
			CallAccelConvergence = AccelNotConvergence;
		}
		StartAccelConvergence = CallAccelConvergence;

		if (isFrontDrive){
			driveWheelsCount += frontLeftWheels.Length + frontRightWheels.Length;
		}
		if (isRearDrive){
			driveWheelsCount += rearLeftWheels.Length + rearRightWheels.Length;
		}

		wheelsDrive = new IWheel[driveWheelsCount];

		int frontCount = 0;
		int rearCount = 0;

		//заносим в один массив все ведущие колёса, левые и правые идут попеременно
		if (isFrontDrive){
			frontCount = frontLeftWheels.Length + frontRightWheels.Length;
			for (int i = 0; i < frontCount; i++){
				if (i % 2 == 0){
					wheelsDrive[i] = frontLeftWheels[(int)Mathf.Floor(i / 2.0f)];
				}else{
					wheelsDrive[i] = frontRightWheels[(int)Mathf.Floor(i / 2.0f)];
				}
			}
		}
		if (isRearDrive){
			rearCount = rearLeftWheels.Length + rearRightWheels.Length;
			for (int i = 0; i < rearCount; i++){
				if (i % 2 == 0){
					wheelsDrive[i + frontCount] = rearLeftWheels[(int)Mathf.Floor(i / 2.0f)];
				}else{
					wheelsDrive[i + frontCount] = rearRightWheels[(int)Mathf.Floor(i / 2.0f)];
				}
			}
		}

		wheelsBrake = new IWheel[frontLeftWheels.Length + frontRightWheels.Length + rearLeftWheels.Length + rearRightWheels.Length];

		int wheelsCount = 0;

		//заносим в один массив все колёса
		for (int i = 0; i < frontLeftWheels.Length; i++){
			wheelsBrake[i] = frontLeftWheels[i];
		}
		wheelsCount += frontLeftWheels.Length;
		for (int i = 0; i < frontRightWheels.Length; i++){
			wheelsBrake[i + wheelsCount] = frontRightWheels[i];
		}
		wheelsCount += frontRightWheels.Length;
		for (int i = 0; i < rearLeftWheels.Length; i++){
			wheelsBrake[i + wheelsCount] = rearLeftWheels[i];
		}
		wheelsCount += rearLeftWheels.Length;
		for (int i = 0; i < rearRightWheels.Length; i++){
			wheelsBrake[i + wheelsCount] = rearRightWheels[i];
		}
	}

	void SetCurve(){
		maxAngularVelocity = maxSpeed / msTokmh / wheelRadius;
		torqueElectro = new AnimationCurve();

		//задаём кривую крутящего момента электродвигателя
		for (int i = 0; i < axisCurveX.Length; i++){
			torqueElectro.AddKey(axisCurveX[i] * maxAngularVelocity, axisCurveY[i]);
		}
	}

	void GetControls(){
		if(controller != null){
			steerAxis = controller.GetAxis("steering_wheel");
			accelAxis = controller.GetAxis("Gas");
			brakeAxis = controller.GetAxis("Brake");
			brakeEngineAxis = controller.GetAxis("BrakeEngine");
			handbrakeAxis = controller.GetAxis("HandBrake");

			CallCruiseControl();
		}else{
			steerAxis = 0.0f;
			accelAxis = 0.0f;
			brakeAxis = 1.0f;
			brakeEngineAxis = 0.0f;
			handbrakeAxis = 1.0f;
		}
	}

	//мониторинг изменения типа контроллера
	IEnumerator GetController(){
		while (doGetController){
			controller = InputManLib.GetController(this);
			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	//инициализация стартовых величин из контроллера (ожидание инициализации контроллера) и старт мониторинга изменения типа контроллера
	IEnumerator GetControlsStart(){
		bool doStart = true;
		while (doStart){
			controller = InputManLib.GetController(this);

			if (controller != null){
				if (controller.IsAxis("steering_wheel") &&
					controller.IsAxis("Brake") &&
					controller.IsAxis("HandBrake"))
				{
					GetControls();
					if (setStartValuesFromController){
						GetOldAxesValues();
					}

					StartCoroutine(GetController());

					doStart = false;
				}
			}

			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	void GetOldAxesValues(){
		oldSteerTarget = steerTarget;
		oldSteer = steerAxis;
		oldBrake = brakeAxis;
		oldHandbrake = handbrakeAxis;
	}

	void ChassisIsAdditionalControl(){
		if (additionalControl.ratio > 0.0f){
			additionalControl.ratio -= (Mathf.Abs(steerAxis - oldSteer) + Mathf.Clamp01(brakeAxis - oldBrake) + Mathf.Abs(handbrakeAxis - oldHandbrake)) * additionalControlRatioSpeed;
			steerTarget = oldSteerTarget + (steerAxis - oldSteerTarget) * steerSpeed;
			GetOldAxesValues();
		}else{
			steerTarget = oldSteerTarget;
			brakeAxis = 0.0f;
			handbrakeAxis = oldHandbrake;
		}
	}

	void ChassisNotAdditionalControl(){
		steerTarget = steerAxis;
	}

	//подсчитывает среднюю скорость вращения колёс в [рад/с]
	void CalcAverageAngularVelocity(){
		averageAngularVelocity = 0.0f;
		foreach (IWheel currWheel in wheelsDrive){
			averageAngularVelocity += currWheel.angularVelocity;
		}
		averageAngularVelocity /= driveWheelsCount;
	}

	//возвращает среднюю скорость вращения колёс в [рад/с]
	public float GetDifferentialAngularVelocity(){
		return averageAngularVelocity;
	}

	void SpeedIsRestricted(){
		if (Mathf.Abs(averageAngularVelocity) > emergencyAngularVelocity){
			engineMain.additionalGas += brakeAdditionalGas;
			brakeEngineAxis = 1.0f;
		}
	}

	void SpeedNotRestricted(){
		return;
	}

	void ChassisGasCruiseControl(){
		bool cruiseControlAxis = controller.GetAxis("CruiseControl") == 1.0f ? true : false;
		if (cruiseControlAxis){
			float diffAngVelo = Mathf.Abs(cruiseControlSpeed) - Mathf.Abs(GetLinearSpeed());
			accelAxis = Mathf.Clamp01(diffAngVelo);
			engineMain.additionalGas += accelAxis;
		}else{
			cruiseControlSpeed = GetLinearSpeed();
		}
	}

	void ChassisBrakeCruiseControl(){
		bool cruiseControlAxis = controller.GetAxis("CruiseControl") == 1.0f ? true : false;
		if (cruiseControlAxis){
			float diffAngVelo = Mathf.Clamp01(Mathf.Abs(GetLinearSpeed()) - Mathf.Abs(cruiseControlSpeed));
			brakeEngineAxis = Mathf.Clamp01(brakeEngineAxis + diffAngVelo);
		}else{
			cruiseControlSpeed = GetLinearSpeed();
		}
	}

	void ChassisGasBrakeCruiseControl(){
		bool cruiseControlAxis = controller.GetAxis("CruiseControl") == 1.0f ? true : false;
		if (cruiseControlAxis){
			float diffAngVelo = Mathf.Abs(cruiseControlSpeed) - Mathf.Abs(GetLinearSpeed());
			accelAxis = Mathf.Clamp01(diffAngVelo);
			engineMain.additionalGas += accelAxis;
			brakeEngineAxis = Mathf.Clamp01(-diffAngVelo);
		}else{
			cruiseControlSpeed = GetLinearSpeed();
		}
	}

	void ChassisNotCruiseControl(){
		return;
	}

	void AccelIsConvergence(){
		if (accelConvergence < accelAxis){
			accelConvergence += Time.fixedDeltaTime * accelDelay;
		}else{
			accelConvergence = accelAxis;
		}
	}

	void AccelNotConvergence(){
		accelConvergence = accelAxis;
	}

	private void CarMove(){
		avgTorque = gearBox.wheelAccelerationTorque / driveWheelsCount;

		CallAccelConvergence();
		CallDamageableBrake();

		if (brakeEngineAxis > 0.0f){
			engineMain.additionalGas += brakeAdditionalGas;
			avgTorque = Mathf.Abs(avgTorque) * brakeEngineAxis * brakeEngineEfficiency;
			driveTorque = 0.0f;
			brakeTorque = avgTorque;
		}else{
			avgTorque *= accelConvergence;
			driveTorque = avgTorque;
			brakeTorque = 0.0f;
		}

		CalcDriveWheels(wheelsDrive, driveTorque, brakeTorque);
		CalcWheels(wheelsBrake);
	}

	void CalcDriveWheels(IWheel[] currWheels, float torque, float brakeTorque){
		foreach (IWheel currWheel in currWheels){
			currWheel.torque = torque * torqueElectro.Evaluate(Mathf.Abs(currWheel.angularVelocity));
			currWheel.brakeTorque = brakeTorque + additionalBrakeTorque * Mathf.Abs(currWheel.angularVelocity) * brakeEngineAxis;
		}
	}

	void CalcWheels(IWheel[] allWheels){
		foreach (IWheel currWheel in allWheels){
			currWheel.brake = brakeAxis * brakeEfficiency;
			currWheel.handbrake = handbrakeAxis;
		}
	}

	void BrakeIsDamageable(){
		brakeTemperature += brakeAxis * averageAngularVelocity * speedBrakeTemperatureToMax - brakeTemperature * speedBrakeTemperatureToEnvironment;
		if (brakeTemperature > brakeTemperatureMax){
			brakeEfficiency -= speedBrakeDamage * brakeAxis * averageAngularVelocity;
			if (brakeEfficiency < 0.0f){
				brakeEfficiency = 0.0f;
			}
		}
	}

	void BrakeNotDamageable(){
		return;
	}

	void OnEnable(){
		if (!doGetController){
			doGetController = true;
			StartCoroutine(GetController());
		}
	}

	void OnDisable(){
		doGetController = false;
	}

	void OnDestroy(){
		doGetController = false;
	}

	public float gas{
		get{
			return accelAxis;
		}
		set{
			return;
		}
	}

	public float steering{
		get{
			return steerTarget;
		}
		set{
			return;
		}
	}

	public float maxSteering{
		get{
			return maxSteeringAngle;
		}
		set{
			maxSteeringAngle = value;
		}
	}

	public float maxLinearSpeed{
		get{
			return maxSpeed;
		}
		set{
			maxSpeed = value;
		}
	}

//	public float emergencyLinearBrakeSpeed{
//		get{
//			return emergencyBrakeSpeed;
//		}
//		set{
//			emergencyBrakeSpeed = value;
//		}
//	}

	public float inertia{
		get{
			float tempInertia = 0.0f;
			foreach (IWheel currWheel in wheelsDrive){
				tempInertia += currWheel.wheelInertia;
			}
			return tempInertia;
		}
		set{
			return;
		}
	}

	//Возвращает направление вращения дифференциала
	public int GetDirectionRotation(){
		return (int)Mathf.Sign(averageAngularVelocity);
	}

	//Линейная скорость точки на поверхности колеса в [км/ч]
	public float GetLinearSpeed(){
		return averageAngularVelocity * radsecTokmh; //линейная скорость [км/ч]
	}

	//Линейная скорость точки на поверхности колеса в [м/с]
	public float GetLinearSpeedms(){
		return averageAngularVelocity * wheelRadius;
	}

	public Vector3 GetSpeed(){
		return rigidbody.velocity;
	}

//	public void ChangeBrakesMode(int mode){
//		if (mode == 0){ //режим с включенными ограничениями
//			CallSpeedRestriction = SpeedIsRestricted;
//			CallDamageableBrake = BrakeIsDamageable;
//		}else{ //режим с отключенными ограничениями
//			CallSpeedRestriction = SpeedNotRestricted;
//			CallDamageableBrake = BrakeNotDamageable;
//			brakeEfficiency = 1.0f;
//		}
//	}

//	IEnumerator CheckTag(){
//		while (Application.isPlaying){
//			if (tag == "AI"){
//				CallSpeedRestriction = SpeedNotRestricted;
//				CallDamageableBrake = BrakeNotDamageable;
//			}else{
//				CallSpeedRestriction = StartSpeedRestriction;
//				CallDamageableBrake = StartDamageableBrake;
//			}
//
//			yield return new WaitForSeconds(1.0f);
//		}
//	}

	public float GetQuality(){
		return quality;
	}

	/// <summary>Поддерживается два уровня качества: 1 - максимум, 0 - минимум</summary>
	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality < 0.5f){
				CallSpeedRestriction = SpeedNotRestricted;
				CallDamageableBrake = BrakeNotDamageable;
				CallCruiseControl = ChassisNotCruiseControl;
				CallAccelConvergence = AccelNotConvergence;
			}else{
				CallSpeedRestriction = StartSpeedRestriction;
				CallDamageableBrake = StartDamageableBrake;
				CallCruiseControl = StartCruiseControl;
				CallAccelConvergence = StartAccelConvergence;
			}
		}
	}
}

echo off

set script_dir=%~dp0
set project_dir=%script_dir%..
set project_name=Project
set ue_install_dir=C:\Program Files\Epic Games\UE_4.23

"%ue_install_dir%\Engine\Binaries\DotNET\UnrealBuildTool.exe" -projectfiles -project="%project_dir%\%project_name%.uproject" -game -rocket -progress

#!/bin/sh

script_dir=$(dirname "$0")
project_dir="$script_dir"/..
absolute_project_dir=$(readlink -f "$project_dir")
project_name=Project
ue_install_dir="$absolute_project_dir"/../UnrealEngine
absolute_ue_install_dir=$(readlink -f "$ue_install_dir")
absolute_build_dir="$absolute_project_dir"/Build
current_date=$(date +"%Y%m%d")
absolute_package_dir="$absolute_build_dir"/"$project_name"_"$current_date"_Linux64

if [ ! -d "$absolute_build_dir" ]; then
  mkdir "$absolute_build_dir"
fi
if [ ! -d "$absolute_package_dir" ]; then
  mkdir "$absolute_package_dir"
fi

"$absolute_ue_install_dir"/Engine/Build/BatchFiles/RunUAT.sh -ScriptsForProject="$absolute_project_dir"/"$project_name".uproject BuildCookRun -nocompileeditor -nop4 -project="$absolute_project_dir"/"$project_name".uproject -cook -stage -archive -archivedirectory="$absolute_package_dir"/ -package -clientconfig=Shipping -ue4exe="$absolute_ue_install_dir"/Engine/Binaries/Linux/UE4Editor -compressed -distribution -nodebuginfo -targetplatform=Linux -build -utf8output -compile

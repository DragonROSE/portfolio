using UnityEngine;
using System.Collections;

/// <summary>
/// Физика колёсной пары вагона
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.3 </para>

/*
 * Todo:
 * Сделать пробуксовку (скольжение) колёс
 * Оптимизировать скорость исполнения кода
 */

public class Wagon : MonoBehaviour, IWagon {

	const float secSetToRailRoad = 2.0f; //количество секунд на старте, при которых боковое усилие от рельс снижается в "roadForceStartFactor" раз (чтобы поезд встал ровно на рельсы без рывка)
	const float roadForceStartFactor = 0.2f; //множитель силы бокового усилия от рельс (только на старте)
	const float minTangentDistance = 0.01f; //квадрат (x^2) минимального расстояния между двумя ближайшими точками на кривой для расчёта касательной
	const float longitudinalStopFactor = 0.1f; //во сколько раз продольное трение меньше поперечного, когда поезд сошёл с рельс
	const float checkOnRailroadDelay = 0.5f; //ожидание между проверками схода с рельс
	const int maxFlySteps = 3; //максимальное количество шагов "checkOnRailroadDelay", которое вагон может провести в воздухе, после чего считается, что он сошёл с рельс

	public Rigidbody body = null; //Rigidbody к которому прикреплена данная колёсная пара

	public float gauge = 1.0f; //ширина колеи

	public float radius = 0.6f; //радиус колёс [м]
	public float suspensionTravel = 0.2f; //длина хода подвески [м]
	public float springForce = 500000.0f; //сила пружины при максимальном сжатии [н]
	public float springDamping = 100000.0f; //сила амортизатора [н]
	public float railroadForce = 1000000.0f; //сила удержания на рельсах [н]
	public float railroadDamping = 500000.0f; //сила уменьшения поперечных колебаний (поперечный амортизатор) [н]
	public float offRailRoadStopForce = 50000.0f; //сила трения колёс о землю, когда поезд сошёл с рельс [м]

	public float maxSlopeAngle = 10.0f; //максимальный угол уклона тележки (поворот вдоль продольной оси)
	public float maxRotateAngle = 10.0f; //максимальный угол поворота тележки (поворот вокруг вертикальной оси)

	public float frictionTorque = 1000.0f; //трение в колёсной паре [н*м]
	public float brakeTorque = 100000.0f; //максимальный крутящий момент создаваемый тормозами [н*м]
	public float stopVelocity = 0.1f; //линейная скорость, при которой считается, что колёсная пара стоит на месте [м/с]

	//состояние колёсной пары
	float compressionLeft = 0.0f;
	float compressionRight = 0.0f;
	float forwardVelocity = 0.0f;
	float engineTorque = 0.0f;
	float wagonBrake = 0.0f;
	float angularVelo = 0.0f;

	float rotY = 0.0f;
	float rotZ = 0.0f;
	float deltaAngle = 0.0f;
	float direction = 0.0f;
	float directionWay = 0.0f;
	float linePos = 0.0f;
//	float oldLinePos = 0.0f;

	float brakeAxis = 0.0f;

	int pathNumber = 0;
	int oldPathNumber = 0;
	int countFlySteps = 0;

	bool onRailroad = false; //изначально поезд не на рельсах
	bool onGroundLeft = true;
	bool onGroundRight = true;

	RaycastHit raycastHitLeft;
	RaycastHit raycastHitRight;

	Vector3 suspensionForce = Vector3.zero;
	Vector3 railWayForce = Vector3.zero;
	Vector3 stopForce = Vector3.zero;

	Vector3 posLeft = Vector3.zero;
	Vector3 posRight = Vector3.zero;
	Vector3 posVelocityLeft = Vector3.zero;
	Vector3 posVelocityRight = Vector3.zero;
	Vector3 posVelocity = Vector3.zero;
	Vector3 pos = Vector3.zero;
	Vector3 oldPos = Vector3.zero;
	Vector3 oldStopPos = Vector3.zero;
	Vector3 targetPos = Vector3.zero;
	Vector3 oldTargetPos = Vector3.zero;
	Vector3 forward = Vector3.forward;
	Vector3 right = Vector3.right;
	Vector3 down = Vector3.down;

//	Vector3 normal = Vector3.up;
//	Vector3 bodyLocalNormal = Vector3.up;
	Vector3 tangent = Vector3.forward;
	Vector3 bodyLocalTangent = Vector3.forward;
	Vector3 direct = Vector3.zero;
	Vector3 lineFuncPos = Vector3.zero;
	Vector3 deltaPos = Vector3.zero;

	//кэшированные величины
	float shiftX = 0.0f;
	float fullSuspension = 0.0f;
	float zeroSuspension = 0.0f;
	float fixedDeltaTime = 1.0f;
	float Rad2Deg = Mathf.Rad2Deg;

	IRailRoadController railController = null;

	void Start(){
		FoundRigidbody();
		SetConstants();
		SetVariables();
	}

	void FoundRigidbody(){
		//поиск Rigidbody к которому прикреплена колёсная пара, если не указано в инспекторе
		if (body == null){
			body = CarPhysxLib.GetFirstRigidbodyInParent(transform);

			if (body == null){
				Debug.LogError("Attached rigidbody not found! Can't simulate wagon physics! Script disabled.");
				enabled = false;
				return;
			}
		}
	}

	void SetConstants(){
		fixedDeltaTime = Time.fixedDeltaTime;
		shiftX = gauge / 2.0f;
		fullSuspension = radius + suspensionTravel;
		zeroSuspension = 1.0f + radius / suspensionTravel;

		railController = InterfaceLib.FindFirstObjectOfInterface<IRailRoadController>(true);

		if (railController == null){
			Debug.LogError("Railroad not found! Train " + body.gameObject.name + " derailed.");
			onRailroad = false;
		}else{
			StartCoroutine(SearchNearestRailway()); //находим ближайшие рельсы
			StartCoroutine(SetToRailRoad()); //установка колёсной пары на рельсы на старте
		}
	}

	void SetVariables(){
		oldPos = transform.position;
		oldStopPos = transform.position;
	}

	IEnumerator SearchNearestRailway(){
		yield return new WaitForFixedUpdate(); //ожидание, пока отработают все Start()

		pos = transform.position;
		pos.y = 0.0f;

		//поиск позиции, на каких именно рельсах стоит вагон
		if (railController.FoundNearestPoint(ref pathNumber, ref linePos, pos)){
//			Debug.Log("Wagon " + gameObject.name + " was set to railroad: path = " + pathNumber.ToString() + ", linePos = " + linePos.ToString());
			onRailroad = true;

			//для правильного расчёта направления касательной
			if (linePos < 0.5f){
				oldTargetPos = railController.GetPathPoint(pathNumber, linePos + 0.05f);
			}else{
				oldTargetPos = railController.GetPathPoint(pathNumber, linePos - 0.05f);
			}
			targetPos = railController.GetPathPoint(pathNumber, linePos);
			tangent = targetPos - oldTargetPos;

			oldPathNumber = pathNumber;
//			oldLinePos = linePos;

			CalcDirectionWay(); //определение направления на сплайне (по направлению увеличения или уменьшения linePos)
			StartCoroutine(CheckOnRailroad());
		}else{
			onRailroad = false;
			Debug.LogError("Can't find nearest railroad! Train " + body.gameObject.name + " derailed.");
		}
	}

	IEnumerator SetToRailRoad(){
		railroadForce *= roadForceStartFactor;
		railroadDamping *= roadForceStartFactor;
		yield return new WaitForSeconds(secSetToRailRoad);
		railroadForce /= roadForceStartFactor;
		railroadDamping /= roadForceStartFactor;
	}
	
	void FixedUpdate(){
		pos = transform.position;
		forward = transform.forward;
		right = transform.right;
		down = Vector3.Cross(right, forward);
		posLeft = pos - right * shiftX;
		posRight = pos + right * shiftX;

		onGroundLeft = Physics.Raycast(posLeft, down, out raycastHitLeft, fullSuspension);
		onGroundRight = Physics.Raycast(posRight, down, out raycastHitRight, fullSuspension);

		posVelocityLeft = body.GetPointVelocity(posLeft);
		posVelocityRight = body.GetPointVelocity(posRight);
		posVelocity = (posVelocityLeft + posVelocityRight) / 2.0f;

		if (onGroundLeft && onGroundRight){
			forwardVelocity = Vector3.Dot(posVelocity, forward);
			angularVelo = forwardVelocity / radius;
		}else{
			//постепенная остановка вращения колеса, когда колёса в воздухе
			if (Mathf.Abs(angularVelo) >= 0.01f){
				angularVelo -= Mathf.Clamp(angularVelo * 10.0f, -1.0f, 1.0f) * fixedDeltaTime;
			}else{
				angularVelo = 0.0f;
			}
		}

		if (onRailroad){
			//вычисление позиции на линии (кривой, вдоль которой идут рельсы)
			direct.x = pos.x - oldPos.x;
			direct.z = pos.z - oldPos.z;

			deltaPos = transform.InverseTransformPoint(targetPos);
			direction = Mathf.Sign(deltaPos.z); //направление движения по рельсам
//			linePos -= direction * direct.magnitude + directionWay * deltaPos.z;
			linePos -= direction * forwardVelocity * fixedDeltaTime + directionWay * deltaPos.z;

			direction = forwardVelocity * directionWay;
			targetPos = railController.CalcPathPoint(ref pathNumber, ref linePos, direction); //linePos - oldLinePos
			if (pathNumber == -1){
				Debug.Log("End of railway pathPos = " + linePos.ToString() + ". Train " + body.gameObject.name + " derailed.");
				onRailroad = false;
			}else if (pathNumber != oldPathNumber){
				CalcDirectionWay();
				oldTargetPos = targetPos - lineFuncPos; //для пересчёта касательной
				oldPathNumber = pathNumber;
//				Debug.Log("Recalculate directionWay = " + directionWay.ToString());
			}

			deltaPos = targetPos - oldTargetPos;
			if (deltaPos.sqrMagnitude >= minTangentDistance){ //пересчёт касательной, только если пройдено минимальное (элементарное) расстояние
				tangent = deltaPos;
				oldTargetPos = targetPos;
				bodyLocalTangent = body.transform.InverseTransformDirection(tangent);
				bodyLocalTangent.y = 0.0f;
				bodyLocalTangent.Normalize();
			}

			//поворот тележки вокруг вертикальной оси вагона
			deltaAngle = Rad2Deg * Mathf.Acos(Mathf.Abs(bodyLocalTangent.z));
			direction = Mathf.Sign(bodyLocalTangent.x * bodyLocalTangent.z);
			if (deltaAngle > maxRotateAngle){
				Debug.Log("Max rotation angle reached, angle = " + deltaAngle.ToString() + ". Train " + body.gameObject.name + " derailed.");
				deltaAngle = maxRotateAngle;
				onRailroad = false;
			}
			rotY = direction * deltaAngle;

			transform.localRotation = Quaternion.Euler(0.0f, rotY, 0.0f); //поворот колёсной пары вокруг вертикальной оси

			wagonBrake = Mathf.Clamp(forwardVelocity * 10.0f, -1.0f, 1.0f) * (frictionTorque + brakeTorque * brakeAxis);

			railWayForce = RailWayForce(posVelocity); //поперечное усилие в колёсной паре
		}else{
			//сошёл с рельс, расчёт поперечного и продольного трения колёс о землю
			railWayForce = -Vector3.Dot(posVelocity, right) * right * offRailRoadStopForce -
				Vector3.Dot(posVelocity, forward) * forward * offRailRoadStopForce * longitudinalStopFactor;
		}

		stopForce = StopForce();

		if (onGroundLeft){
			compressionLeft = zeroSuspension - raycastHitLeft.distance / suspensionTravel;
			suspensionForce = SuspensionForce(compressionLeft, posVelocityLeft, raycastHitLeft.normal);
			body.AddForceAtPosition(suspensionForce + railWayForce + stopForce, posLeft);
		}else{
			compressionLeft = 0.0f;
		}

		if (onGroundRight){
			compressionRight = zeroSuspension - raycastHitRight.distance / suspensionTravel;
			suspensionForce = SuspensionForce(compressionRight, posVelocityRight, raycastHitRight.normal);
			body.AddForceAtPosition(suspensionForce + railWayForce + stopForce, posRight);
		}else{
			compressionRight = 0.0f;
		}

		oldPos = pos;
	}
	
	//сила реакции подвески
	Vector3 SuspensionForce(float currCompress, Vector3 posVelo, Vector3 currNormal){
		float damperForce = Vector3.Dot(posVelo, currNormal) * springDamping;
		return (currCompress * springForce - damperForce) * currNormal;
	}

	//продольная и поперечная сила реакции колеса
	Vector3 RailWayForce(Vector3 posVelo){
		float damperForce = Vector3.Dot(posVelo, right) * railroadDamping;
		float deltaPos = Vector3.Dot(targetPos - pos, right);
		return (deltaPos * railroadForce - damperForce) * right + (engineTorque / radius - wagonBrake) / radius * forward;
	}

	//удерживает колёса на одном месте, если скорость слишком мала
	Vector3 StopForce(){
		if (Mathf.Abs(forwardVelocity) > stopVelocity){
			oldStopPos = pos;
			return Vector3.zero;
		}else{
			Vector3 stopDirection = (oldStopPos - pos) * springForce * 10.0f;
			return forward * brakeAxis * (stopVelocity - Mathf.Abs(angularVelocity / radius)) * Vector3.Dot(stopDirection, forward);
		}
	}

	IEnumerator CheckOnRailroad(){
		while (onRailroad){
			//если произошло слишком сильное отклонение (поворот вдоль продольной оси), то сошёл с рельс
			rotZ = Mathf.Abs(body.transform.rotation.eulerAngles.z);
			if (rotZ > 180.0f){
				rotZ = 360.0f - rotZ;
			}
			if (rotZ > maxSlopeAngle){
				Debug.Log("Max slope angle reached, angle = " + rotZ.ToString() + ". Train " + body.gameObject.name + " derailed.");
				onRailroad = false;
			}

			if (onGroundLeft && onGroundRight){
				countFlySteps = 0;
			}else{
				countFlySteps++;
			}
			if (countFlySteps > maxFlySteps){
				Debug.Log("Train take off. Train " + body.gameObject.name + " derailed.");
				onRailroad = false; //сошёл с рельс
			}

			yield return new WaitForSeconds(checkOnRailroadDelay);
		}
	}

	void CalcDirectionWay(){
		if (linePos < 0.5f){
			lineFuncPos = railController.GetPathPoint(pathNumber, linePos + 0.1f) - targetPos;
		}else{
			lineFuncPos = targetPos - railController.GetPathPoint(pathNumber, linePos - 0.1f);
		}
		directionWay = Mathf.Sign(Vector3.Dot(lineFuncPos, transform.forward));
	}

	public float torque{
		get{
			return engineTorque;
		}
		set{
			engineTorque = value;
		}
	}

	public float angularVelocity{
		get{
			return angularVelo;
		}
		set{
			return;
		}
	}

	public float brake{
		get{
			return brakeAxis;
		}
		set{
			brakeAxis = value;
		}
	}

//	public int railwayNumber{
//		get{
//			return pathNumber;
//		}
//		set{
//			pathNumber = value;
//		}
//	}

//	public float railwayPos{
//		get{
//			return linePos;
//		}
//		set{
//			linePos = value;
//		}
//	}

	public float steering{
		get{
			return rotY;
		}
		set{
			return;
//			rotY = value;
		}
	}

	public float wheelRadius{
		get{
			return radius;
		}
		set{
			radius = value;
			SetConstants();
		}
	}

	public bool GetOnRailroad(){
		return onRailroad;
	}

	public float GetCompression(){
		return (compressionLeft + compressionRight) / 2.0f;
	}

	public float GetSuspensionDistance(){
		return suspensionTravel;
	}

	public Vector3 GetBodyLocalNormal(){
		Vector3 normal = (raycastHitLeft.normal + raycastHitRight.normal) / 2.0f;
		Vector3 bodyLocalNormal = body.transform.InverseTransformDirection(normal); //нормаль для правильной анимации тележки (колёсной пары)
		return bodyLocalNormal;
	}

	public Vector3 GetBodyLocalTangent(){
		return bodyLocalTangent;
	}

	public Transform GetTransform(){
		return transform;
	}

	public Vector3[] GetDebugInfo(){
		Vector3[] tempArr = new Vector3[10];
		
		Vector3 tempPos = transform.position;
		Vector3 tempRight = transform.right;
		Vector3 tempUp = transform.up;

		tempArr[0] = tempPos;
		tempArr[1] = tempRight * shiftX;
		tempArr[2] = -tempUp * fullSuspension;
		tempArr[3] = raycastHitLeft.normal * compressionLeft;
		tempArr[4] = raycastHitRight.normal * compressionRight;
		tempArr[5] = railWayForce;
		tempArr[6] = posVelocityLeft;
		tempArr[7] = posVelocityRight;
		tempArr[8] = tangent.normalized;
		tempArr[9] = targetPos;

		return tempArr;
	}
}

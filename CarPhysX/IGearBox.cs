using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс коробки передач
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IGearBox{

	/// <summary>Текущее отношение шестерёнок включенной передачи</summary>
	float gearRatio { get; set; }

	/// <summary>Текущий номер включенной передачи</summary>
	int currGear { get; set; }

	/// <summary>Текущий крутящий момент передаваемый на колёса от коробки передач</summary>
	float wheelAccelerationTorque { get; set; }

	/// <summary>Возвращает отношение шестерёнок передачи по индексу</summary>
	float GetGearRatio(int index);

	/// <summary>Устанавливает отношение шестерёнок передачи по индексу</summary>
	void SetGearRatio(int index, float newRatio);

	/// <summary>Количество передач</summary>
	int GetCount();

	/// <summary>Возвращает направление вращения коробки передач в зависимости от включенной скорости</summary>
	/// <returns>1 - если передача вперёд, -1 - если передача назад</returns>
	int GetDirectionRotation();

	/// <summary>[deprecated] Крутящий момент коробки передач</summary>
	float GetTorque(float torq);

	/// <summary>[deprecated] Обратная связь от шасси</summary>
	float FeedBackRPM(float fRPM);

	/// <summary>[deprecated] Смещение в таблице Gears</summary>
	int offsetGears { get; set; }

	/// <summary>[deprecated] Текущее смещение в таблице Gears</summary>
	int currOffsetGears { get; set; }

	/// <summary>[deprecated] Режим работы</summary>
	int mode { get; set; }

	/// <summary>[deprecated] Преобразование номера режима в название режима</summary>
	string GetModeToString(int m);
}

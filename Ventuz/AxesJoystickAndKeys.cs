using System;
using System.Collections.Generic;
using Ventuz.Kernel;

using SlimDX.DirectInput;

/// <summary>
/// ���������� 3D ��������� (6 ��������)
/// ̸����� ���� ��� ���������
/// ������������� ���������� � ����������
/// </summary>
/// <para> Author: ������� ������ </para>
/// <para> Version: 1.0 </para>
public class Script : ScriptBase, System.IDisposable
{
	// This member is used by the Validate() method to indicate
	// whether the Generate() method should return true or false
	// during its next execution.
	private bool changed;

	private const int AXIS_RANGE = 10000;

	private DirectInput directInput = new DirectInput();

	private Guid[] joysticksGuids = null; //Joysticks ID

	private Joystick joystick = null;
	private JoystickState joystickState = new JoystickState();

	private string[] allJoysticks = null;
	private string[] allJoysticksGuids = null;

	private float axisX = 0.0f, axisY = 0.0f, axisZ = 0.0f;
	private float axisRotX = 0.0f, axisRotY = 0.0f, axisRotZ = 0.0f;
	private float oldAxisZ = 0.0f;

	private float axisPan  = 0.0f;
	private float axisTilt = 0.0f;
	private float axisFov  = 0.0f;

	// This Method is called if the component is loaded/created.
	public Script()
	{
		GetAllJoysticks();
		InitializeJoystick();

		// Note: Accessing input or output properties from this method
		// will have no effect as they have not been allocated yet.
	}

	// This Method is called if the component is unloaded/disposed
	public virtual void Dispose()
	{
	}

	// This Method is called if an input property has changed its value
	public override void Validate()
	{
		// Remember: set changed to true if any of the output 
		// properties has been changed, see Generate()
	}

	// This Method is called every time before a frame is rendered.
	// Return value: if true, Ventuz will notify all nodes bound to this
	//               script node that one of the script's outputs has a
	//               new value and they therefore need to validate. For
	//               performance reasons, only return true if output
	//               values really have been changed.
	public override bool Generate()
	{
		if (joystick != null && !joystick.GetCurrentState(ref joystickState).IsFailure)
		{
			axisX    = (float)joystickState.X         / (float)AXIS_RANGE;
			axisY    = (float)joystickState.Y         / (float)AXIS_RANGE;
			axisZ    = (float)joystickState.Z         / (float)AXIS_RANGE;
			axisRotX = (float)joystickState.RotationX / (float)AXIS_RANGE;
			axisRotY = (float)joystickState.RotationY / (float)AXIS_RANGE;
			axisRotZ = (float)joystickState.RotationZ / (float)AXIS_RANGE;
		}
		else
		{
			axisX    = 0;
			axisY    = 0;
			axisZ    = 0;
			axisRotX = 0;
			axisRotY = 0;
			axisRotZ = 0;
		}

		if (AxisReverseX)    { axisX    = -axisX;    }
		if (AxisReverseY)    { axisY    = -axisY;    }
		if (AxisReverseZ)    { axisZ    = -axisZ;    }
		if (AxisReverseRotX) { axisRotX = -axisRotX; }
		if (AxisReverseRotY) { axisRotY = -axisRotY; }
		if (AxisReverseRotZ) { axisRotZ = -axisRotZ; }

		axisX    = AxisDeadZone(axisX,    _DeadZoneMinAxisX,    _DeadZoneMaxAxisX);
		axisY    = AxisDeadZone(axisY,    _DeadZoneMinAxisY,    _DeadZoneMaxAxisY);
		axisZ    = AxisDeadZone(axisZ,    _DeadZoneMinAxisZ,    _DeadZoneMaxAxisZ);
		axisRotX = AxisDeadZone(axisRotX, _DeadZoneMinAxisRotX, _DeadZoneMaxAxisRotX);
		axisRotY = AxisDeadZone(axisRotY, _DeadZoneMinAxisRotY, _DeadZoneMaxAxisRotY);
		axisRotZ = AxisDeadZone(axisRotZ, _DeadZoneMinAxisRotZ, _DeadZoneMaxAxisRotZ);

		//3D joystick axes
		axisPan  = axisRotY + axisX;
		axisTilt = axisRotX;
		axisFov  = axisRotZ;

		//keyboard
		axisPan  += (KeyPanMore  ? 1.0f : 0.0f) + (KeyPanLess  ? -1.0f : 0.0f);
		axisTilt += (KeyTiltMore ? 1.0f : 0.0f) + (KeyTiltLess ? -1.0f : 0.0f);
		axisFov  += (KeyFovMore  ? 1.0f : 0.0f) + (KeyFovLess  ? -1.0f : 0.0f);

		axisPan  = Clamp(axisPan);
		axisTilt = Clamp(axisTilt);
		axisFov  = Clamp(axisFov);

		if (Activated && axisZ >= _DeadZoneMaxAxisZ && oldAxisZ < _DeadZoneMaxAxisZ)
		{
			ChangePanorama();
		}

		oldAxisZ = axisZ;

		//Disable controls if coin not inserted or time is over
		if (Activated)
		{
			AxisPan  = axisPan;
			AxisTilt = axisTilt;
			AxisFov  = axisFov;
		}
		else
		{
			AxisPan  = 0.0f;
			AxisTilt = 0.0f;
			AxisFov  = 0.0f;
		}

		return true;
	}

	private float AxisDeadZone(float axis, float min, float max)
	{
		if ((float)Math.Abs(axis) > min)
		{
			if (axis >= max)
			{
				return 1.0f;
			}
			else if (axis <= -max)
			{
				return -1.0f;
			}
			else
			{
				if (axis > 0.0f)
				{
					return (axis - min) / (max - min);
				}
				else
				{
					return (axis + min) / (max - min);
				}
			}
		}
		else
		{
			return 0.0f;
		}
	}

	private float Clamp(float axis)
	{
		if (axis > 1.0f)
		{
			return 1.0f;
		}
		else if (axis < -1.0f)
		{
			return -1.0f;
		}
		return axis;
	}

	public void GetAllJoysticks()
	{
		IList<DeviceInstance> deviceInstances = directInput.GetDevices(DeviceClass.GameController, DeviceEnumerationFlags.AttachedOnly);

		if (deviceInstances == null)
		{
			return;
		}

		joysticksGuids = new Guid[deviceInstances.Count];
		allJoysticks = new string[deviceInstances.Count];
		allJoysticksGuids = new string[deviceInstances.Count];

		for (int i = 0; i < deviceInstances.Count; ++i)
		{
			allJoysticks[i] = deviceInstances[i].InstanceName;
			allJoysticksGuids[i] = deviceInstances[i].InstanceGuid.ToString();
			joysticksGuids[i] = deviceInstances[i].InstanceGuid;
		}
	}

	private void InitializeJoystick()
	{
		if (joysticksGuids == null || _JoystickNumber >= joysticksGuids.Length)
		{
			return;
		}

		joystick = new Joystick(directInput, joysticksGuids[_JoystickNumber]);
		foreach (DeviceObjectInstance doi in joystick.GetObjects(ObjectDeviceType.Axis))
		{
			joystick.GetObjectPropertiesById((int)doi.ObjectType).SetRange(-AXIS_RANGE, AXIS_RANGE);
		}

		joystick.Properties.AxisMode = DeviceAxisMode.Absolute;
		joystick.Acquire();
	}

// This Method is called if the function/method Debug_DumpJoysticks is invoked by the user or a bound event.
// Return true, if this component has to be revalidated!
	public bool OnDebug_DumpJoysticks(int arg)
	{
		GetAllJoysticks();

		AllJoysticks = allJoysticks;
		AllJoysticksGuids = allJoysticksGuids;

		return false;
	}

	// This Method is called if the function/method Debug_InitJoystick is invoked by the user or a bound event.
	// Return true, if this component has to be revalidated!
	public bool OnDebug_InitJoystick(int arg)
	{
		InitializeJoystick();
		return false;
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс двигателя
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IEngine{

	/// <summary>Возвращает конкретный экземпляр класса</summary>
	IEngine GetEngine();
	
	/// <summary>Возвращает направление вращения двигателя</summary>
	/// <returns>1 - если вращение "вперёд", -1 - если вращение "назад"</returns>
	int GetDirectionRotation();

	/// <summary>Текущее положение педали газа (в том числе и автоматическое)</summary>
	float throttle { get; set; }

	/// <summary>Управление газом через внешние модули</summary>
	float additionalGas { get; set; }

	/// <summary>Текущие обороты двигателя [об/мин]</summary>
	float rpm { get; set; }

	/// <summary>Обороты холостого хода двигателя [об/мин]</summary>
	float idleRpm { get; set; }

	/// <summary>Рабочие обороты двигателя [об/мин]</summary>
	float workRpm { get; set; }

	/// <summary>Возвращает максимально возможные обороты из графика крутящего момента</summary>
	/// <returns>Максимально возможные обороты двигателя [об/мин]</returns>
	float GetMaxRPM();

	/// <summary>Текущий крутящий момент двигателя [н*м]</summary>
	float torque { get; set; }

	/// <summary>Возвращает состояние двигателя: заведён или нет</summary>
	bool engineOn { get; set; }

	/// <summary>Принудительно включить двигатель</summary>
	void StartEngine();

	/// <summary>Возвращает состояние двигателя: вращается или нет (зависит от engineStopRPM)</summary>
	/// <returns>true - обороты двигателя более engineStopRPM, false - менее engineStopRPM</returns>
	bool isRotaiting();

	/// <summary>Текущий крутящий момент стартера</summary>
	float starterTorque { get; set; }

	/// <summary>Состояние стартера: включен или выключен</summary>
	bool starterOn { get; set; }

	/// <summary>Текущие обороты двигателя [рад/с]</summary>
	float angularVelocity { get; set; }

	/// <summary>Инертность двигателя [кг/м^2]</summary>
	float inertia { get; set; }

	/// <summary>[deprecated] Переключение состоянии транстмиссии на двигателе</summary>
	void SetTransmissionON(bool on);

	/// <summary>[deprecated] Обратная связь с трансмиссией</summary>
	void FeedBackRPM(float fRPM);

	/// <summary>[deprecated] Число оборотов вала коробки передач или колёс в минуту</summary>
	float exitRPM { get; set; }
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Реализация физики колеса
/// Основана на CarTutorial Alternative Physics
/// Переработана и доработана по книге "Брайан Бекман - Физика Гонок" ("Brian Beckman - The Physics of Racing")
/// Создана на основе физики Ханс Бастиаан Пасека (Hans Bastiaan Pacejka)
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.4 </para>

/*
 * Рекомендуемые настройки:
 * 1.  springForce - сила пружины при её максимальном сжатии, рекомендуется springForce = суммарная масса машины * 10
 * 2.  damping - рекомендуются значения от 0.1 * springForce до 1 * springForce
 * 3.  mass - масса колеса, рекомендуется умножить реальную массу колеса транспортного средства на 2
 * 4.  frictionTorque - допустимо любое положительное число, влияет на угловое ускорение самозатормаживания колеса (эмитация трения в подшипниках и т.д.)
 * 5.  grip - от 0 до 1, можно менять во время выполнения программы, напрямую влияет на сцепление с поверхностью; значения более 1 допустимы, но не рекомендуются
 * 6.  slipVal - количество дополнительных пересчётов на один шаг, при 100 шагах в секунду (Time.fixedDeltaTime = 0.01) рекомендуется slipVal = 10. Увеличение данного параметра сильно нагружает процессор!
 * 7.  slipConvergence - скорость схождения значения slipVal к 1, требуется для баланса между точностью расчётов и нагрузкой на процессор
 * 8.  fullSlipVelo - угловая скорость в радианах в секунду, ниже которой будет снижаться продольные действующие силы, рекомендуется значение 4
 * 9.  fullAngleVelo - угловая скорость в радианах в секунду, ниже которой будет снижаться поперечные действующие силы, рекомендуется значение от 2 до 7
 * 10. stopVelocity - удерживает машину на месте, если линейная скорость в метрах в секунду меньше указанной, рекомендуется от 0.01 до 0.05
 * 
 * ---
 * Необходимо расширить функционал:
 * много точек касания по радиусу или Physics.SphereCast (круглое колесо)
 * ---
 * 
 */

public class Wheel : MonoBehaviour, IWheel, IQuality{

	public float radius = 0.34f; //радиус колёс [м]
	public float suspensionTravel = 0.2f; //длина хода подвески [м]
	public float tireSuspension = 0.0f; //максимальное сжатие шины [м]
	public float tireWidth = 0.1f; //ширина шины [м]
	public int raycastsWidthCount = 1; //количество лучей по ширине

	public float springForce = 10000.0f; //сила пружины при максимальном сжатии [н]
	public float damping = 5000.0f; //сила амортизатора [м]
	public float mass = 30.0f; //масса колеса [кг]
	public float brakeFrictionTorque = 4000.0f; //максимальная момент силы торможения [н*м]
	public float handbrakeFrictionTorque = 0.0f; //максимальный момент силы торможения ручного тормоза [н*м]
	public float frictionTorque = 10.0f; //внутреннее сопротивление колеса [н*м]

	public float grip = 1.0f; //условный коэффициент сцепления колёс с дорогой, влияет на результирующие силы
	public float slipVal = 10.0f; //количество перещётов сил при приближении скорости колеса к 0
	public float minSlipVal = 5.0f; //количество перещётов сил при quality = 0
	public float slipConvergence = 0.01f; //скорость схождения к 1 величины slipVal
	public float fullSlipVelo = 4.0f; //скорость вращения колеса, начиная с которой не будет применяться снижение _продольной_ приложенной силы (для исключения рывков) [рад/с]
	public float fullAngleVelo = 2.0f; //скорость вращения колеса, начиная с которой не будет применяться снижение _поперечной_ приложенной силы (для исключения рывков) [рад/с]

	public float stopVelocity = 0.01f; //линейная скорость, при которой считается, что колесо стоит на месте [м/с]

	//коэффициенты Ханс Бастиаан Пасека (Hans Bastiaan Pacejka)
	public float[] a = { //15 констант для поперечного сцепления (Lateral Force)
						1.799f,				//коэффициент скорости спада графика после пика					 1.2 .. 18		[б/р]
						0.0f,				//влияние нагрузки на боковой коэффициент трения (*1000)		 -80 .. 80		[1/кН]
						1688.0f,			//боковой коэффициент трения (*1000)							 900 .. 1700	[б/р]
						4140.0f,			//изменение жесткости со скольжением (X - пика графика)			 500 .. 2000	[Н/градус]
						6.026f,				//изменение прогрессивности жесткости / нагрузки				   0 .. 50		[1/кН]
						0.0f,				//влияние развала на жесткость									-0.1 .. 0.1		[%/градус/100] (не доконца ясно, в каких единицах эта константа)
						-0.3589f,			//изменения кривизны под нагрузкой								  -2 .. 2		[1/кН]
						1.0f,				//фактор искривления											 -20 .. +1		[б/р]
						0.0f,				//влияние нагрузки на горизонтальный сдвиг						  -1 .. +1		[градус/кН]
						-6.111f / 1000.0f,	//горизонтальный сдвиг при нулевой нагрузке и развале			  -1 .. +1		[градус]
						-3.244f / 100.0f,	//влияние развала на горизонтальный сдвиг						-0.1 .. +0.1	[градус/градус] (не доконца ясно, в каких единицах эта константа)
						0.0f,				//вертикальный сдвиг											-200 .. 200		[Н]
						0.0f,				//вертикальный сдвиг при нулевой нагрузке						 -10 .. 10		[Н]
						0.0f,				//влияние развала на вертикальный сдвиг от нагрузки				 -10 .. +10		[Н/градус/кН]
						0.0f				//влияние развала на вертикальный сдвиг							 -15 .. +15		[Н/градус]
					   };
	public float[] b = { //11 констант для продольного сцепления (Longitudinal Force)
						1.65f,				//коэффициент скорости спада графика после пика					 1.4 .. 1.8		[б/р]
						0.0f,				//влияние нагрузки на продольный коэффициент трения (*1000)		 -80 .. 80		[1/кН]
						1688.0f,			//продольный коэффициент трения (Y - пика графика) (*1000)		 900 .. 1700	[1/К] (обратная величина к коэффициенту)
						0.0f,				//искривление фактора жесткости / нагрузки						 -20 .. 20		[1/кН]
						229.0f,				//изменение жесткости со скольжением (X - пика графика)			 100 .. 500		[Н/%скольжения]
						0.0f,				//изменение прогрессивности жесткости / нагрузки				  -1 .. 1		[1/кН]
						0.0f,				//изменения кривизны с нагрузкой (^2)							-0.1 .. 0.1		[1/кН^2]
						0.0f,				//изменения кривизны с нагрузкой								  -1 .. 1		[1/кН]
						-10.0f,				//фактор искривления пика графика								 -20 .. 1		[б/р]
						0.0f,				//влияние нагрузки на горизонтальный сдвиг						  -1 .. 1		[1/кН]
						0.0f				//горизонтальный сдвиг											  -5 .. 5		[%]
					   };

	public Rigidbody body = null; //Rigidbody к которому прикреплено данное колесо

	//качество обработки физики
	float quality = 1.0f;
	int startRaycastsWidthCount = 1;
	float startSlipVal = 10.0f;

	//входные величины
	float driveTorque = 0.0f; //ускоряющий крутящий момент передаваемый на колесо от двигателя
	float driveFrictionTorque = 0.0f; //тормозящий крутящий момент передаваемый на колесо от двигателя
	float brakeAxis = 0.0f; //ось тормоза [0 .. 1]
	float handbrakeAxis = 0.0f; //ось стояночного тормоза [0 .. 1]
	float steeringAngle = 0.0f; //угол поворота колеса вокруг своей оси Y [град]
	float driveInertia = 0.0f; //инертность двигателя
//	float suspensionForceInput = 0.0f; //сила возникающая в балке поперечной устойчивости (при необходимости)
	float deltaAngle = 0.0f; //разница в градусах между предыдущим и текущим углом

	//состояние колеса
	float compression = 0.0f; //степень сжатия подвески
	float wheelAngularVelocity = 0.0f; //угловая скорость колеса
	float normalForce = 0.0f;
	float slipAngle = 0.0f;
	float slipRatio = 0.0f;
	bool onGround = false; //касается земли или нет
//	Vector3 worldForce = Vector3.zero;
	Vector3 wheelVelo = Vector3.zero;
	Vector3 localVelo = Vector3.zero;
	Vector3 groundNormal = Vector3.zero;
	Vector3 localGroundNormal = Vector3.zero;
	Vector3 suspensionForce = Vector3.zero;
	Vector3 roadForce = Vector3.zero;
	Vector3 addForcePos = Vector3.zero;
	Quaternion localRotation = Quaternion.identity;
	Quaternion inverseLocalRotation = Quaternion.identity;

	//кешированные величины
	int onGroundCount = 0;
	float inertia = 2.2f; //момент инерции колеса [кг * м^2], рассчитавается как (I = 0.5 * M * R^2), где M - масса колеса, R - радиус колеса
	float maxSlip = 0.0f;
	float maxSlipAngle = 0.0f;
	float oldAngle = 0.0f;
	float totalInertia = 1.0f;
	float longit_DB = 0.0f;
	float zeroSuspension = 0.0f;
	float fullSuspension = 0.0f;
	float fixDeltaTimeDevTotalIn = 0.0f;
	float fixDeltaTimeRadiusDevTotalIn = 0.0f;
	float driveAngularDelta = 0.0f;
	float frictionAngularDelta = 0.0f;
	float magnitudeMotion = 0.0f;
	float fixedDeltaTime; //Time.fixedDeltaTime
	Vector3 pos = Vector3.zero;
	Vector3 up = Vector3.up;
	Vector3 right = Vector3.right;
	Vector3 forward = Vector3.forward;
	Vector3 forceRight = Vector3.zero;
	Vector3 forceForward = Vector3.zero;
	Vector3 oldPos = Vector3.zero;
	Vector3 stopForce = Vector3.zero;

	delegate float LongitudinalForceDelegate(float forceUp, float currSlip); //выбор на старте, упрощенного или полного расчета продольного скольжения
	LongitudinalForceDelegate CallLongitudinalForce;

	WheelRaycasts[] raycasts; //массив всех касаний

	//информация о всех касаниях колесом поверхности
	class WheelRaycasts{
		public RaycastHit hit;
		public bool onGround = false;
		public float shiftX = 0.0f; //сдвиг от центральной точки
	}

	void Start(){
		//поиск Rigidbody к которому прикреплено колесо, если не указано в инспекторе
		if (body == null){
			body = CarPhysxLib.GetFirstRigidbodyInParent(transform);

			if (body == null){
				Debug.LogError("Attached rigidbody not found! Can't simulate wheel physics! Script disabled.");
				enabled = false;
				return;
			}
		}

		//выбор упрощенного или полного расчёта продольной силы в зависимости от заданных коэффициентов продольного скольжения
		if (b[1] == 0.0f && b[3] == 0.0f && b[5] == 0.0f && b[6] == 0.0f && b[7] == 0.0f && b[9] == 0.0f && b[10] == 0.0f){
			CallLongitudinalForce = CalcLongitudinalForceSimple;
		}else{
			CallLongitudinalForce = CalcLongitudinalForceFull;
		}

		SetConstants(); //прерасчёт величин
		InitRaycasts(); //расчёт положения Raycasts
		InitSlipMaxima(); //поиск экстремумов функций (максимум продольного и поперечного сцепления)
	}

	void FixedUpdate(){

		pos = transform.position;
		up = transform.up;
//		forward = transform.forward;
//		right = transform.right;
		right = transform.TransformDirection(localRotation * Vector3.right);

		onGround = false;
		onGroundCount = 0;

		//поиск всех точек касания и подсчёт их количества
		foreach (WheelRaycasts currRaycast in raycasts){
			currRaycast.onGround = Physics.Raycast(pos + right * currRaycast.shiftX, -up, out currRaycast.hit, fullSuspension);

			//если коллайдер является триггером, то перебираем все коллайдеры под колёсами и находим ближайший, не являющийся триггером
			if (currRaycast.onGround && currRaycast.hit.collider.isTrigger){
				float dist = fullSuspension;
				RaycastHit[] hits = Physics.RaycastAll(pos + right * currRaycast.shiftX, -up, fullSuspension);

				currRaycast.onGround = false;

				foreach (RaycastHit currHit in hits){
					if (!currHit.collider.isTrigger && currHit.distance <= dist){
						currRaycast.hit = currHit;
						currRaycast.onGround = true;
						dist = currHit.distance;
					}
				}
			}

			if (currRaycast.onGround) {
				onGround = true;
				onGroundCount++;
			}
		}

/*		//много точек касания (круглое колесо)
		// --- требуется доработка ---
		float degRot = 360.0f / raycasts;
		float[] cosins = new float[raycasts];

		for (int i = 0; i < 5; i++){
			onGrounds[i * 2] = Physics.Raycast(pos - right * tireWidth, Quaternion.AngleAxis(-90.0f + i * degRot, transform.right) * -up, out hits[i * 2], radius);
			onGrounds[i * 2 + 1] = Physics.Raycast(pos + right * tireWidth, Quaternion.AngleAxis(-90.0f + i * degRot, transform.right) * -up, out hits[i * 2 + 1], radius);
			cosins[i] = Mathf.Cos((-90.0f + i * degRot) * Mathf.Deg2Rad);
		}
		for (int i = 5; i < 10; i++){
			onGrounds[i * 2] = Physics.Raycast(pos - right * tireWidth, Quaternion.AngleAxis(-90.0f + degRot + i * degRot, transform.right) * -up, out hits[i * 2], radius);
			onGrounds[i * 2 + 1] = Physics.Raycast(pos + right * tireWidth, Quaternion.AngleAxis(-90.0f + degRot + i * degRot, transform.right) * -up, out hits[i * 2 + 1], radius);
			cosins[i] = Mathf.Cos((-90.0f + degRot + i * degRot) * Mathf.Deg2Rad);
		}

		for (int i = 0; i < raycasts; i++){ //много точек касания
			if (onGrounds[i]){
				onGroundCount++;
			}
		}
*/

		if (onGround){ //колесо касается поверхности
			float currCompression = 0.0f;
			Vector3 currNormal = Vector3.zero;

			compression = 0.0f;
			groundNormal = Vector3.zero;
			addForcePos = Vector3.zero;
			foreach (WheelRaycasts currRaycast in raycasts){
				if (currRaycast.onGround){
					currNormal = currRaycast.hit.normal;
					groundNormal += currNormal;
					addForcePos += currRaycast.hit.point;
					currCompression = (zeroSuspension - currRaycast.hit.distance / suspensionTravel) * Vector3.Dot(currNormal, up);
					if (compression < currCompression){
						compression = currCompression;
					}
				}
			}

			compression -= 0.5f;
			compression = Mathf.Clamp01(compression);

/*			//для круглого колеса
			for (int i = 0; i < 20; i++){
				if (onGrounds[i]){
					groundNormal += hits[i].normal;
					compressions[i] = cosins[i] - ((hits[i].distance - radius + tireSuspension) / suspensionTravel); //делить не на suspensionTravel, а на tireSuspension (?)
				}
			}
*/

			groundNormal /= onGroundCount;
			addForcePos /= onGroundCount;
			localGroundNormal = transform.InverseTransformDirection(inverseLocalRotation * groundNormal);

			wheelVelo = body.GetPointVelocity(pos);
//			wheelVelo = (pos - oldPos) / fixedDeltaTime; //работает нестабильно, но нужно как-то подключить, т.к. нужна реакция на поворот (особенно когда стоит на месте)
			localVelo = transform.InverseTransformDirection(inverseLocalRotation * wheelVelo); //направление силы трения

			suspensionForce = SuspensionForce();
			roadForce = RoadForce();
			stopForce = StopForce();

			body.AddForceAtPosition(suspensionForce + roadForce + stopForce, addForcePos);

		}else{ //колесо в "воздухе" (ничего не касается)
			compression = 0.0f;
			slipRatio = 0.0f;
			suspensionForce = Vector3.zero;
			roadForce = Vector3.zero;

			oldPos = pos;

			DriveFrictionDelta(1.0f);
			AngularVelocityDelta();
		}
	}

	void SetConstants(){
		longit_DB = b[4] / (b[0] * b[2]);
		inertia = mass * radius * radius / 2.0f;
		totalInertia = inertia + driveInertia;
		zeroSuspension = 1.0f + (radius - tireSuspension) / suspensionTravel;
		fullSuspension = suspensionTravel / 2.0f + radius - tireSuspension; //полная длина хода подвески (ось колеса совпадает с положением данного transform, ход подвески от этой точки ввех и вниз = suspensionTravel / 2.0f)
		fixedDeltaTime = Time.fixedDeltaTime;
		fixDeltaTimeDevTotalIn = fixedDeltaTime / totalInertia;
		fixDeltaTimeRadiusDevTotalIn = radius * fixedDeltaTime / totalInertia;
		startRaycastsWidthCount = raycastsWidthCount;
		startSlipVal = slipVal;
		oldPos = transform.position;
	}

	void InitRaycasts(){
		if (raycastsWidthCount < 1){
			raycastsWidthCount = 1;
			Debug.LogWarning("The number of raycasts can't be less than 1, raycastsWidthCount set to 1");
		}

		raycasts = new WheelRaycasts[raycastsWidthCount];

		if (raycastsWidthCount == 1){ //если одна точка касания, то ставим её по центру (смещение = 0)
			raycasts[0] = new WheelRaycasts();
			raycasts[0].shiftX = 0.0f;
		}else{ //если точек касания > 1, то рассчитываем координаты смещения относительно центра для каждой точки касания
			for (int i = 0; i < raycasts.Length; i++){
				raycasts[i] = new WheelRaycasts();
				raycasts[i].shiftX = (tireWidth * (float)i) / ((float)raycastsWidthCount - 1.0f) - tireWidth / 2.0f;
			}
		}
	}

	//находим экстремумы продольного и поперечного сечения (с помощью численного метода решения уравнений)
	void InitSlipMaxima(){
		const float stepSize = 0.001f;
		const float testNormalForce = 4000.0f; //в данном случае сила может быть любой отличной от 0, нам нужен только экстремум

		float force = 0.0f;

		for (float tempSlip = stepSize; ; tempSlip += stepSize){
			float newForce = CallLongitudinalForce(testNormalForce, tempSlip);
			if (force < newForce){
				force = newForce;
			}else{
				maxSlip = tempSlip - stepSize;
				break;
			}
		}

		force = 0;

		for (float tempSlipAngle = stepSize; ; tempSlipAngle += stepSize){
			float newForce = CalcLateralForce(testNormalForce, tempSlipAngle);
			if (force < newForce){
				force = newForce;
			}else{
				maxSlipAngle = tempSlipAngle - stepSize;
				break;
			}
		}
	}

	//сила реакции подвески
	Vector3 SuspensionForce(){
		normalForce = compression * springForce;

		float damperForce = Vector3.Dot(localVelo, localGroundNormal) * damping;

//		return (normalForce - damperForce + suspensionForceInput) * groundNormal;
		return (normalForce - damperForce) * groundNormal;
	}

	//сила реакции резины
	Vector3 RoadForce(){
		int slipRes = (int)(slipVal - Mathf.Abs(wheelAngularVelocity) * slipVal * slipConvergence);
		if (slipRes < 1){
			slipRes = 1;
		}
		float invSlipRes = (1.0f / (float)slipRes);

		DriveFrictionDelta(invSlipRes);

		Vector3 totalForce = Vector3.zero;
		float newAngle = steeringAngle;

		slipAngle = SlipAngle();

		for (int i = 0; i < slipRes; i++){
			localRotation = Quaternion.Euler(0.0f, oldAngle + (((newAngle - oldAngle) * (float)i) - deltaAngle * ((float)slipRes - (float)i - 1.0f)) * invSlipRes, 0.0f);
			inverseLocalRotation = Quaternion.Inverse(localRotation);
			forward = transform.TransformDirection(localRotation * Vector3.forward);

			slipRatio = SlipRatio();

			Vector3 mforce = invSlipRes * grip * CombinedForce(normalForce, slipRatio, slipAngle);
			
			wheelAngularVelocity -= mforce.z * fixDeltaTimeRadiusDevTotalIn;
			AngularVelocityDelta();

			totalForce += transform.TransformDirection(localRotation * mforce); //итоговая сила в мировых координатах
		}

//		right = transform.TransformDirection(localRotation * Vector3.right);

		oldAngle = newAngle;
		return totalForce;
	}

	//удерживает колёса на одном месте, если скорость слишком мала
	Vector3 StopForce(){
		if (magnitudeMotion > stopVelocity){
			oldPos = pos;
			return Vector3.zero;
		}else{
			Vector3 stopDirection = (oldPos - pos) * springForce * 10.0f;
			return right * Vector3.Dot(stopDirection, right) + forward * (brakeAxis + handbrakeAxis) * (stopVelocity - Mathf.Abs(angularVelocity / radius)) * Vector3.Dot(stopDirection, forward);
		}
	}

	//степень продольного скольжения (ось Z)
	float SlipRatio(){
		float wheelRoadVelo = Vector3.Dot(wheelVelo, forward); //проекция вектора скорости колеса на ось Z колеса

		if (wheelRoadVelo == 0.0f){ //когда машина стоит, а колесо начинает крутится, то скольжение = 0
			return 0.0f;
		}

		float absRoadVelo = Mathf.Abs(wheelRoadVelo);
		float damping = Mathf.Clamp01(absRoadVelo / fullSlipVelo); //уменьшаем степень скольжения при приближении скорости вращения к 0 (для избежания рывков)

		float wheelTireVelo = wheelAngularVelocity * radius; //линейная скорость точки на поверхности колеса
		return (wheelTireVelo - wheelRoadVelo) / absRoadVelo * damping;
	}

	//угол бокового скольжения (ось X) (угол увода)
	float SlipAngle(){
		Vector3 wheelMotionDirection = localVelo;
		wheelMotionDirection.y = 0.0f;

		float sqrMagnitudeMotion = wheelMotionDirection.sqrMagnitude;

		if (sqrMagnitudeMotion < Mathf.Epsilon){ //если боковая скорость слишком мала, то считаем её равной 0
			return 0.0f;
		}

		magnitudeMotion = Mathf.Sqrt(sqrMagnitudeMotion);
		float sinSlipAngle = wheelMotionDirection.x / magnitudeMotion; //нормализуем только по оси X
		sinSlipAngle = Mathf.Clamp(sinSlipAngle, -1.0f, 1.0f); //чтобы избежать ошибки точности
		float damping = Mathf.Clamp01(magnitudeMotion / fullAngleVelo);

		//необходимо провести эксперимент на разных машинах, и установить, что работает стабильнее линейная или квадратичная зависимость damping
//		return -Mathf.Asin(sinSlipAngle) * damping * damping;
		return -Mathf.Asin(sinSlipAngle) * damping;
	}

	//продольная сила возникающая в точке касания (полная версия)
	float CalcLongitudinalForceFull(float forceUp, float currSlip){
		forceUp *= 0.001f; //конвертируем в килоньютоны [кН]
		currSlip *= 100.0f; //конвертируем в %
		float sqrForceUp = forceUp * forceUp;
		float uP = b[1] * forceUp + b[2]; //оценка пика продольного коэффициента трения, как линейная зависимость от веса на колесе
		float D = uP * forceUp;
		float B = ((b[3] * sqrForceUp + b[4] * forceUp) * Mathf.Exp(-b[5] * forceUp)) / (b[0] * D);
		float S = currSlip + b[9] * forceUp + b[10];
		float E = b[6] * sqrForceUp + b[7] * forceUp + b[8];
		float SB = S * B;
		return D * Mathf.Sin(b[0] * Mathf.Atan(SB + E * (Mathf.Atan(SB) - SB)));
	}

	//продольная сила возникающая в точке касания (облегчённая версия)
	float CalcLongitudinalForceSimple(float forceUp, float currSlip){
		forceUp *= 0.001f; //конвертируем в килоньютоны [кН]
		currSlip *= 100.0f; //конвертируем в %
		float D = b[2] * forceUp;
		float SB = currSlip * longit_DB;
		return D * Mathf.Sin(b[0] * Mathf.Atan(SB + b[8] * (Mathf.Atan(SB) - SB)));
	}

	//поперечная сила возникающая в точке касания (в закомментированных строчках - расчёт для сход-развала)
	//вызывается один раз на старте, для определения маскимально возможного угла увода (срыв в боковое скольжение)
	float CalcLateralForce(float forceUp, float currSlipAngle){
		forceUp *= 0.001f; //конвертируем в килоньютоны [кН]
		currSlipAngle *= Mathf.Rad2Deg; //конвертируем угол в градусы
		float uP = a[1] * forceUp + a[2]; //пик бокового коэффициента трения
		float D = uP * forceUp;
		float B = (a[3] * Mathf.Sin(2.0f * Mathf.Atan(forceUp / a[4]))) / (a[0] * D);
//		float B = (a[3] * Mathf.Sin(2.0f * Mathf.Atan(forceUp / a[4])) * (1 - a[5] * Mathf.Abs(yb))) / (a[0] * D); //yb - угол развала (сход-развал)
		float S = currSlipAngle + a[9] * forceUp + a[10];
//		float S = slipAngle + a[8] * yb + a[9] * forceUp + a[10];
		float E = a[6] * forceUp + a[7];
		float Sv = a[12] * forceUp + a[13];
//		float Sv = ((a[11,1] * forceUp + a[11,2]) * yb + a[12]) * forceUp + a[13];
		float SB = S * B;
		return D * Mathf.Sin(a[0] * Mathf.Atan(SB + E * (Mathf.Atan(SB) - SB))) + Sv;
	}

	//расчёт влияния продольного скольжения на поперечное и наоборот (книга "Брайан Бекман - Физика Гонок", глава 25)
	Vector3 CombinedForce(float forceUp, float currSlip, float currSlipAngle){
		float unitSlip = currSlip / maxSlip;
		float unitAngle = currSlipAngle / maxSlipAngle;
		float p = Mathf.Sqrt(unitSlip * unitSlip + unitAngle * unitAngle);

		if (p > Mathf.Epsilon){
			forceRight.x = localGroundNormal.y; //поворот вектора localGroundNormal на 90 градусов вокруг оси Z
			forceRight.y = -localGroundNormal.x;
			forceForward.y = -localGroundNormal.z; //поворот вектора localGroundNormal на 90 градусов вокруг оси X
			forceForward.z = localGroundNormal.y;
			return forceRight * unitAngle / p * CallLongitudinalForce(forceUp, p * maxSlipAngle) + forceForward * unitSlip / p * CallLongitudinalForce(forceUp, p * maxSlip);
		}else{
			return Vector3.zero;
		}
	}

	//суммарный момент торможения
	void DriveFrictionDelta(float invSlipRes){
		driveAngularDelta = driveTorque * fixDeltaTimeDevTotalIn * invSlipRes;
		float totalFrictionTorque = brakeFrictionTorque * brakeAxis + handbrakeFrictionTorque * handbrakeAxis + frictionTorque + driveFrictionTorque;
		frictionAngularDelta = totalFrictionTorque * fixDeltaTimeDevTotalIn * invSlipRes;
	}

	//изменение угловой скорости вращения колеса
	void AngularVelocityDelta(){
		wheelAngularVelocity += driveAngularDelta;
		if (Mathf.Abs(wheelAngularVelocity) > frictionAngularDelta){
			wheelAngularVelocity -= frictionAngularDelta * Mathf.Sign(wheelAngularVelocity);
		}else{
			wheelAngularVelocity = 0.0f;
		}
	}

	public float torque{
		get{
			return driveTorque;
		}
		set{
			driveTorque = value;
		}
	}

	public float angularVelocity{
		get{
			return wheelAngularVelocity;
		}
		set{
			wheelAngularVelocity = value;
		}
	}

	public float brake{
		get{
			return brakeAxis;
		}
		set{
			brakeAxis = value;
		}
	}

	public float brakeTorque{
		get{
			return driveFrictionTorque;
		}
		set{
			driveFrictionTorque = value;
		}
	}
	
	public float handbrake{
		get{
			return handbrakeAxis;
		}
		set{
			handbrakeAxis = value;
		}
	}

	public float steering{
		get{
			return steeringAngle;
		}
		set{
			steeringAngle = value;
		}
	}

	public float deltaSteering{
		get{
			return deltaAngle;
		}
		set{
			deltaAngle = value;
		}
	}

	public float wheelRadius{
		get{
			return radius;
		}
		set{
			radius = value;
			SetConstants();
		}
	}

	public float wheelInertia{
		get{
			return inertia;
		}
		set{
			inertia = value;
			mass = 2.0f * inertia / (radius * radius);
			SetConstants();
		}
	}

	public float engineInertia{
		get{
			return driveInertia;
		}
		set{
			driveInertia = value;
			SetConstants();
		}
	}

	public bool GetOnGround(){
		return onGround;
	}

	public float GetCompression(){
		return compression;
	}

	public float GetSuspensionDistance(){
		return suspensionTravel;
	}

	public float GetFullSuspensionDistance(){
		return suspensionTravel / 2.0f + radius - tireSuspension;
	}

	public Transform GetTransform(){
		return transform;
	}

	public Vector3[] GetDebugInfo(){
		Vector3[] tempArr = new Vector3[10 + raycasts.Length];
		
		Vector3 tempPos = transform.position;
		Vector3 tempForward;
		Vector3 tempRight;
		Vector3 tempUp;

		if (onGround){
			tempForward = forward;
			tempRight = right;
			tempUp = up;
		}else{
			tempForward = transform.forward;
			tempRight = transform.right;
			tempUp = transform.up;
		}

		tempArr[0] = tempPos;
		tempArr[1] = tempForward;
		tempArr[2] = tempRight;
		tempArr[3] = tempUp;
		tempArr[4] = -tempUp * fullSuspension;
//		tempArr[5] = tempPos - tempUp * radius;
		tempArr[5] = addForcePos;
		tempArr[6] = wheelVelo;
		tempArr[7] = suspensionForce;
		tempArr[8] = roadForce;
		tempArr[9] = stopForce;

		for (int i = 0; i < raycasts.Length; i++){
			tempArr[10 + i] = tempPos + tempRight * raycasts[i].shiftX;
		}

		return tempArr;
	}

	public float GetQuality(){
		return quality;
	}

	/// <summary>Поддерживается три уровня качества: 1 - максимум, менее 0.5 - минимум, 0 - выключено</summary>
	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;
			raycasts = null;

			if (newQuality == 0.0f){
				enabled = false;
			}else if (newQuality < 0.5f){
				enabled = true;
				oldPos = transform.position;
				raycastsWidthCount = 1;
				slipVal = minSlipVal;
			}else{
				enabled = true;
				oldPos = transform.position;
				raycastsWidthCount = startRaycastsWidthCount;
				slipVal = startSlipVal;
			}

			InitRaycasts();
		}
	}
}

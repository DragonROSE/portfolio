using UnityEngine;
using System.Collections;

/// <summary>
/// Анимация вращения рулевого колеса (баранка)
/// Вращает руль вокруг локальной оси Х, ось должна быть направлена от водителя
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class SteeringWheelRotate : MonoBehaviour {

	public Transform steeringWheel = null; //3D-объект руля

	public float maxSteeringWheelAngle = 540.0f; //максимальный угол поворота руля [град]

	public Transform chassisTransform = null; //объект на котором висит скрипт шасси
	private IChassis chassis = null; //интерфейс шасси

	void Start(){
		chassis = InterfaceLib.GetInterfaceComponent<IChassis>(chassisTransform);
		if (chassis == null){
			Debug.LogError("Chassis interface not found, can't animate! Script disabled.");
			enabled = false;
			return;
		}

		if (steeringWheel == null){
			steeringWheel = transform;
		}
	}

	void LateUpdate(){
		steeringWheel.localRotation = Quaternion.Euler(-chassis.steering * maxSteeringWheelAngle, 0.0f, 0.0f);
	}
}

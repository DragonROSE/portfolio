using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс колеса
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IWheel{

	/// <summary>Крутящий момент передаваемый на колёса [н*м]</summary>
	float torque { get; set; }

	/// <summary>Угловая скорость колёса [рад/с]</summary>
	float angularVelocity { get; set; }

	/// <summary>Тормоз (ось)</summary>
	float brake { get; set; }

	/// <summary>Торможение двигателем [н*м]</summary>
	float brakeTorque { get; set; }

	/// <summary>Ручной тормоз (ось)</summary>
	float handbrake { get; set; }

	/// <summary>Угол поворота колеса вокруг своей оси [град]</summary>
	float steering { get; set; }

	/// <summary>Разница между предыдущим и следующим углом (указывается только если не используется steering) [град]</summary>
	float deltaSteering { get; set; }

	/// <summary>Радиус колеса [м]</summary>
	float wheelRadius { get; set; }

	/// <summary>Инертность колеса [кг * м^2]</summary>
	float wheelInertia { get; set; }

	/// <summary>Инертность двигателя [кг * м^2]</summary>
	float engineInertia { get; set; }

	/// <summary>Касается земли или нет</summary>
	bool GetOnGround();

	/// <summary>Коэффициент сжатия подвески</summary>
	float GetCompression();

	/// <summary>Ход подвески</summary>
	float GetSuspensionDistance();

	/// <summary>Полный ход подвески</summary>
	float GetFullSuspensionDistance();

	/// <summary>Возвращает Transform, на котором находится колесо</summary>
	Transform GetTransform();

	/// <summary>Информация для отрисовки векторов</summary>
	Vector3[] GetDebugInfo();

}

using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс шасси
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IChassis{

	/// <summary>Ось педали газа</summary>
	float gas { get; set; }

	/// <summary>Ось руля</summary>
	float steering { get; set; }

	/// <summary>Максимальный угол поворота колёс в градусах</summary>
	float maxSteering { get; set; }

	/// <summary>Максимальная скорость в километрах в час</summary>
	float maxLinearSpeed { get; set; }

	/// <summary>Инертность колёс [кг/м^2]</summary>
	float inertia { get; set; }

	/// <summary>Возвращает направление вращения дифференциала</summary>
	/// <returns>1 - если вращение "вперёд", -1 - если вращение "назад"</returns>
	int GetDirectionRotation();

	/// <summary>Возвращает скорость вращения дифференциала в [об/мин]</summary>
	float GetDifferentialAngularVelocity();

	/// <summary>Возвращает линейную скорость вращения колёс в [км/ч]</summary>
	float GetLinearSpeed();

	/// <summary>Возвращает линейную скорость вращения колёс в [м/с]</summary>
	float GetLinearSpeedms();

	/// <summary>Возвращает векторную скорость объекта [м/с]</summary>
	Vector3 GetSpeed();

}

echo off

set script_dir=%~dp0
set project_dir=%script_dir%..
set project_name=Project
set ue_install_dir=C:\Program Files\Epic Games\UE_4.23
set build_dir=%project_dir%\Build
for /f %%a in ('powershell -Command "Get-Date -format yyyyMMdd"') do set current_date=%%a
set package_dir=%build_dir%\%project_name%_%current_date%_Win64

if not exist "%build_dir%" mkdir "%build_dir%"
if not exist "%package_dir%" mkdir "%package_dir%"

echo.
echo Building project ...
echo.

call "%ue_install_dir%\Engine\Build\BatchFiles\RunUAT.bat" -ScriptsForProject="%project_dir%\%project_name%.uproject" BuildCookRun -installed -nop4 -project="%project_dir%\%project_name%.uproject" -cook -stage -archive -archivedirectory="%package_dir%" -package -clientconfig=Shipping -ue4exe="%ue_install_dir%\Engine\Binaries\Win64\UE4Editor-Cmd.exe" -clean -compressed -distribution -nodebuginfo -targetplatform=Win64 -build -utf8output

echo.
echo Moving builded files ...
echo.

move /y "%package_dir%\WindowsNoEditor\*" "%package_dir%\"
for /d %%a in ("%package_dir%\WindowsNoEditor\*") do move /y "%%a" "%package_dir%\"
rmdir /s /q "%package_dir%\WindowsNoEditor"

explorer "%package_dir%"

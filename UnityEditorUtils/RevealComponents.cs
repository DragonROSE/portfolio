using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Расширение UnityEditor
/// Выводит в консоль запрошенные компоненты на выделенных GameObjects
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class RevealComponents : ScriptableObject {

	[MenuItem("Tools/Reveal/Reveal Animation")]
	static void RevealAnimation(){
		RevealComponent<Animation>();
	}

	[MenuItem("Tools/Reveal/Reveal AudioListener")]
	static void RevealAudioListener(){
		RevealComponent<AudioListener>();
	}

	[MenuItem("Tools/Reveal/Reveal BoxCollider")]
	static void RevealBoxCollider(){
		RevealComponent<BoxCollider>();
	}

	[MenuItem("Tools/Reveal/Reveal Collider")]
	static void RevealCollider(){
		RevealComponent<Collider>();
	}

	[MenuItem("Tools/Reveal/Reveal Light")]
	static void RevealLight(){
		RevealComponent<Light>();
	}

	[MenuItem("Tools/Reveal/Reveal MeshCollider")]
	static void RevealMeshCollider(){
		RevealComponent<MeshCollider>();
	}

	[MenuItem("Tools/Reveal/Reveal MeshFilter")]
	static void RevealMeshFilter(){
		RevealComponent<MeshFilter>();
	}

	[MenuItem("Tools/Reveal/Reveal MeshRenderer")]
	static void RevealMeshRenderer(){
		RevealComponent<MeshRenderer>();
	}

	[MenuItem("Tools/Reveal/Reveal MonoBehaviour")]
	static void RevealMonoBehaviour(){
		RevealComponent<MonoBehaviour>();
	}

	[MenuItem("Tools/Reveal/Reveal Rigidbody")]
	static void RevealRigidbody(){
		RevealComponent<Rigidbody>();
	}

	[MenuItem("Tools/Reveal/Reveal Terrain")]
	static void RevealTerrain(){
		RevealComponent<Terrain>();
	}

	static void RevealComponent<revealComponent>() where revealComponent : Component{
		GameObject[] selectedGameObjects = Selection.gameObjects;
		List<revealComponent> listFoundComponents = new List<revealComponent>();
		
		foreach (GameObject currGameObject in selectedGameObjects){
			revealComponent[] foundComponents = currGameObject.GetComponentsInChildren<revealComponent>(true); //включая неактивные GameObject и неактивные Component
			if (foundComponents != null){
				listFoundComponents.AddRange(foundComponents);
			}
		}

		ListLib.RemoveDublicates<revealComponent>(ref listFoundComponents); //удаление дупликатов из листа

		foreach (revealComponent currComponent in listFoundComponents){
			GameObject componentGameObject = currComponent.gameObject;
			Debug.Log(typeof(revealComponent).ToString() + " found @ " + componentGameObject.name, componentGameObject);
		}
	}
}

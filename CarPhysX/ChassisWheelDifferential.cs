using UnityEngine;
using System.Collections;

/// <summary>
/// Реализация колёсной физики шасси, все величины вычисляются в системе "СИ"
/// Реализован дифференциал задней оси и его блокировка
/// Привод на задние и/или передние колёса
/// Для общения с другими классами используется интерфейс I_Chassis и I_Chassis_Extended
/// Для передачи крутящего момента на колёса используется интерфейс коробки передач I_GearBox
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.4 </para>

/*
 * Todo:
 * 1. разные дифференциалы для ведущих осей (управление каждым)
 * 2. межосевой дифференциал (доделать: блокировка, распределение и т.д.)
 * 3. независимая блокировка каждого из дифференциалов, в том числе межосевого
 * 4. тормозной клапан (распределение силы торможения, между передними/задними осями)
 * 5. тормоз раздельно левыми и правыми колёсами, на данный момент возможен только для задних осей
 */

public class ChassisWheelDifferential : MonoBehaviour, IChassis, IChassisExtended, IQuality{

	const float getControllerDelay = 0.5f;

	public Transform[] transformFrontLeftWheels = null; //объект на котором висят передние левые колёса
	public Transform[] transformFrontRightWheels = null; //объект на котором висят передние правые колёса
	public Transform[] transformRearLeftWheels = null; //объект на котором висят задние левые колёса
	public Transform[] transformRearRightWheels = null; //объект на котором висят задние правые колёса

	public float maxSteeringAngle = 30.0f;
	public float wheelInertia = 1.0f; //инертность колёс

	public bool isDifferentialLockable = false; //наличие блокировки дифференциала
	public bool differentialInteraxalLocked = false; //блокировка межосевого дифференциала
	public bool isLeftRightBrake = false; //два раздельных тормоза для левых и правых колёс
	public bool isFrontDrive = false; //передние ведущие
	public bool isRearDrive = true; //задние ведущие
	public bool isFrontBrake = true;
	public bool isRearBrake = true;
	public bool isABS = false;

	float sumCompression = 0.0f; //сумма сжатия ведущих колёс
	float accelTorque = 0.0f;
	float brakeTorque = 0.0f;
	float averageAngularVelocity = 0.0f;
	float diffAngularVelocity = 0.0f;
	float quality = 1.0f;

	int driveWheelsCount = 0; //количество ведущих колёс
	int driveWheelsCouples = 0; //количество пар ведущих колёс (количество ведущих мостов)

	bool differentialLocked = false;

//	public Transform controllerTransform = null; //объект на котором висит контроллер
	private IInputMan controller = null; //данные с органов управления
	
	public Transform gearBoxTransform = null; //объект на котором висит скрипт КПП
	IGearBox gearBox = null; //интерфейс КПП
	IGearBoxExtended gearBoxExtended = null; //расширенный интерфейс КПП

	IWheel[] frontLeftWheels;
	IWheel[] frontRightWheels;
	IWheel[] rearLeftWheels;
	IWheel[] rearRightWheels;

	float steerAxis = 0.0f; //угол поворота колёс с органов управления (от -1 до 1)
	float brakeAxis = 0.0f; //степень торможения (от 0 до 1)
	float brakeAxisLeft = 0.0f; //степень торможения левой стороной (от 0 до 1)
	float brakeAxisRight = 0.0f; //степень торможения правой стороной (от 0 до 1)
	float handbrakeAxis = 0.0f; //ручник (от 0 до 1)

	float radsecTokmh = 1.0f;
	float wheelRadius = 1.0f;

	bool doGetController = true;

	delegate void WheelBrakeDelegate(); //выбор на старте, тип тормозов
	WheelBrakeDelegate CallWheelBrake;
	delegate void WheelBrakeAxisDelegate(); //выбор на старте, тип тормозмозных педалей
	WheelBrakeAxisDelegate CallBrakeAxis;
	delegate void DifferentialLockDelegate(); //выбор на старте, наличие блокировки дифференциала
	DifferentialLockDelegate CallDifferentialLock;

	WheelDataBrake[] wheelsBrake = null; //тормозящие колёса
	WheelDataDrive[] wheelsDrive = null; //ведущие колёса

	//структура колёс
	class WheelDataBrake{
		public IWheel wheel = null; //колесо
//		public float factorForce = 0.0f; //коэффициент распределения нагрузки (необходим для распределителя тормозной системы)
		public Transform transform = null;
	}

	//структура ведущего колеса
	class WheelDataDrive{
		public IWheel wheel = null; //колесо
		public float factorForce = 0.0f; //коэффициент распределения нагрузки на ведущих колёсах (от 0 до 1), необходим для распределения крутящего момента в дифференциале
	}

	void Start(){
		GetInterfaces();
		SetConstants();
		CreateWheels();
		StartCoroutine(GetController());
	}

	void FixedUpdate(){
		GetControls();
		CallDifferentialLock();
		averageAngularVelocity = CalcAverageAngularVelocity();
		Differential();
		CarMove();
		CallWheelBrake();
	}

	void GetInterfaces(){
//		controller = InterfaceLib.GetInterfaceComponent<IInputMan>(controllerTransform);
		gearBox = InterfaceLib.GetInterfaceComponent<IGearBox>(gearBoxTransform);
		gearBoxExtended = InterfaceLib.GetInterfaceComponent<IGearBoxExtended>(gearBoxTransform);

		frontLeftWheels = CarPhysxLib.GetWheelInterface(transformFrontLeftWheels);
		frontRightWheels = CarPhysxLib.GetWheelInterface(transformFrontRightWheels);
		rearLeftWheels = CarPhysxLib.GetWheelInterface(transformRearLeftWheels);
		rearRightWheels = CarPhysxLib.GetWheelInterface(transformRearRightWheels);
	}

	void SetConstants(){
		wheelRadius = rearLeftWheels[0].wheelRadius;
		radsecTokmh = wheelRadius * 3600.0f / 1000.0f;

		if (isLeftRightBrake){
			CallWheelBrake = WheelBrakeLeftRight;
		}else{
			if (isABS){
				CallWheelBrake = WheelBrakeABS;
			}else{
				CallWheelBrake = WheelBrake;
			}
		}

		if (isLeftRightBrake){
			CallBrakeAxis = BrakeLeftRight;
		}else{
			CallBrakeAxis = BrakeAll;
		}

		if (isDifferentialLockable){
			CallDifferentialLock = DifferentialIsLockable;
		}else{
			CallDifferentialLock = DifferentialNotLockable;
		}
	}

	void CreateWheels(){
		if (isFrontDrive){
			driveWheelsCount += frontLeftWheels.Length + frontRightWheels.Length;
		}
		if (isRearDrive){
			driveWheelsCount += rearLeftWheels.Length + rearRightWheels.Length;
		}
		driveWheelsCouples = (int)(driveWheelsCount / 2);

		wheelsDrive = new WheelDataDrive[driveWheelsCount];

		int frontCount = 0;
		int rearCount = 0;

		//создание структуры ведущих колёс, левые и правые идут попеременно
		if (isFrontDrive){
			frontCount = frontLeftWheels.Length + frontRightWheels.Length;
			for (int i = 0; i < frontCount; i++){
				if (i % 2 == 0){
					wheelsDrive[i] = SetupWheelsDrive(frontLeftWheels[(int)Mathf.Floor(i / 2.0f)]);
				}else{
					wheelsDrive[i] = SetupWheelsDrive(frontRightWheels[(int)Mathf.Floor(i / 2.0f)]);
				}
			}
		}
		if (isRearDrive){
			rearCount = rearLeftWheels.Length + rearRightWheels.Length;
			for (int i = 0; i < rearCount; i++){
				if (i % 2 == 0){
					wheelsDrive[i + frontCount] = SetupWheelsDrive(rearLeftWheels[(int)Mathf.Floor(i / 2.0f)]);
				}else{
					wheelsDrive[i + frontCount] = SetupWheelsDrive(rearRightWheels[(int)Mathf.Floor(i / 2.0f)]);
				}
			}
		}

		int wheelsCount = 0;
		int brakeWheelsCount = 0;
		if (isFrontBrake){
			brakeWheelsCount += frontLeftWheels.Length + frontRightWheels.Length;
		}
		if (isRearBrake){
			brakeWheelsCount += rearLeftWheels.Length + rearRightWheels.Length;
		}

		wheelsBrake = new WheelDataBrake[brakeWheelsCount];

		if (isFrontBrake){
			for (int i = 0; i < frontLeftWheels.Length; i++){
				wheelsBrake[i] = SetupWheelsBrake(frontLeftWheels[i]);
			}
			wheelsCount += frontLeftWheels.Length;
			for (int i = 0; i < frontRightWheels.Length; i++){
				wheelsBrake[i + wheelsCount] = SetupWheelsBrake(frontRightWheels[i]);
			}
			wheelsCount += frontRightWheels.Length;
		}
		if (isRearBrake){
			for (int i = 0; i < rearLeftWheels.Length; i++){
				wheelsBrake[i + wheelsCount] = SetupWheelsBrake(rearLeftWheels[i]);
			}
			wheelsCount += rearLeftWheels.Length;
			for (int i = 0; i < rearRightWheels.Length; i++){
				wheelsBrake[i + wheelsCount] = SetupWheelsBrake(rearRightWheels[i]);
			}
		}
	}

	WheelDataBrake SetupWheelsBrake(IWheel currWheel){
		WheelDataBrake result = new WheelDataBrake();
		result.wheel = currWheel;
		result.transform = currWheel.GetTransform();
		return result;
	}

	WheelDataDrive SetupWheelsDrive(IWheel currWheel){
		WheelDataDrive result = new WheelDataDrive();
		result.wheel = currWheel;
		return result;
	}

	void GetControls(){
		steerAxis = controller.GetAxis("steering_wheel");
		CallBrakeAxis();
		handbrakeAxis = controller.GetAxis("HandBrake");
	}

	void BrakeAll(){
		brakeAxis = controller.GetAxis("Brake");
	}

	void BrakeLeftRight(){
		brakeAxisLeft = controller.GetAxis("BrakeLeft");
		brakeAxisRight = controller.GetAxis("BrakeRight");
	}

	void DifferentialIsLockable(){
		differentialLocked = controller.GetAxis("DifferentialLock") == 1.0f ? true : false;
	}

	void DifferentialNotLockable(){
		return;
	}

	void Differential(){
		//блокировка межосевого дифференциала
		if (differentialInteraxalLocked){
			for (int i = 0; i < driveWheelsCount; i += 2){
				diffAngularVelocity = averageAngularVelocity - (wheelsDrive[i].wheel.angularVelocity + wheelsDrive[i + 1].wheel.angularVelocity) / 2.0f;
				wheelsDrive[i].wheel.angularVelocity += diffAngularVelocity;
				wheelsDrive[i + 1].wheel.angularVelocity += diffAngularVelocity;
			}
		}
		//расчёт разницы нагрузки на колёса одного моста
		for (int i = 0; i < driveWheelsCount; i += 2){
			sumCompression = wheelsDrive[i].wheel.GetCompression() + wheelsDrive[i + 1].wheel.GetCompression();
			if (differentialLocked){ //дифференциал заблокирован
				wheelsDrive[i].factorForce = 0.5f + Mathf.Clamp(averageAngularVelocity - wheelsDrive[i].wheel.angularVelocity, -0.5f, 0.5f);
				wheelsDrive[i + 1].factorForce = 0.5f + Mathf.Clamp(averageAngularVelocity - wheelsDrive[i + 1].wheel.angularVelocity, -0.5f, 0.5f);
//				wheelsDrive[i].factorForce = 0.5f;
//				wheelsDrive[i + 1].factorForce = 0.5f;
				wheelsDrive[i].wheel.angularVelocity = averageAngularVelocity;
				wheelsDrive[i + 1].wheel.angularVelocity = averageAngularVelocity;
			}else if (sumCompression == 0.0f){ //оба колеса висят в воздухе
				wheelsDrive[i].factorForce = 0.5f;
				wheelsDrive[i + 1].factorForce = 0.5f;
			}else{
				wheelsDrive[i].factorForce = 1.0f - wheelsDrive[i].wheel.GetCompression() / sumCompression;
				wheelsDrive[i + 1].factorForce = 1.0f - wheelsDrive[i + 1].wheel.GetCompression() / sumCompression;
			}
		}
	}

	//подсчитывает среднюю скорость вращения ведущих колёс в [рад/с]
	public float CalcAverageAngularVelocity(){
		float tempAvgAngVelo = 0.0f;
		foreach (WheelDataDrive currWheel in wheelsDrive){
			tempAvgAngVelo += currWheel.wheel.angularVelocity;
		}
		return tempAvgAngVelo /= driveWheelsCount;
	}

	//возвращает среднюю скорость вращения колёс в [рад/с]
	public float GetDifferentialAngularVelocity(){
		return averageAngularVelocity;
	}

	void CarMove(){
		accelTorque = gearBox.wheelAccelerationTorque / driveWheelsCouples;
		brakeTorque = gearBoxExtended.wheelBrakeTorque / driveWheelsCouples;

		//крутящий момент для ведущих колёс
		foreach (WheelDataDrive currWheel in wheelsDrive){
			currWheel.wheel.torque = accelTorque * currWheel.factorForce;
			currWheel.wheel.brakeTorque = brakeTorque * currWheel.factorForce;
		}
	}

	void WheelBrakeABS(){
		foreach (WheelDataBrake currWheel in wheelsBrake){
			if (rigidbody.GetPointVelocity(currWheel.transform.position).magnitude - Mathf.Abs(currWheel.wheel.angularVelocity) * currWheel.wheel.wheelRadius < 1.0f){
				currWheel.wheel.brake = brakeAxis;
			}else{
				currWheel.wheel.brake = 0.0f;
			}
			currWheel.wheel.handbrake = handbrakeAxis;
		}
	}

	//(недоделано) тормозной клапан (распределение силы торможения, между передними/задними)
	void WheelBrake(){
//		float diff = frontLeftWheels[0].GetCompression() - rearLeftWheels[0].GetCompression();
//		for (int i = 0; i < frontLeftWheels.Length + frontRightWheels.Length; i++){
//			wheelsBrake[i].factorForce = 1.0f + diff;
//			wheelsBrake[i].factorForce = Mathf.Clamp01(wheelsBrake[i].factorForce);
//		}
//		for (int i = frontLeftWheels.Length + frontRightWheels.Length; i < wheelsBrake.Length; i++){
//			wheelsBrake[i].factorForce = 1.0f - diff;
//			wheelsBrake[i].factorForce = Mathf.Clamp01(wheelsBrake[i].factorForce);
//		}
		foreach (WheelDataBrake currWheel in wheelsBrake){
//			currWheel.wheel.brake = brakeAxis * currWheel.factorForce;
			currWheel.wheel.brake = brakeAxis;
			currWheel.wheel.handbrake = handbrakeAxis;
		}
	}

	void WheelBrakeLeftRight(){
		foreach (Wheel currWheel in rearLeftWheels){
			currWheel.brake = brakeAxisLeft;
			currWheel.handbrake = handbrakeAxis;
		}
		foreach (Wheel currWheel in rearRightWheels){
			currWheel.brake = brakeAxisRight;
			currWheel.handbrake = handbrakeAxis;
		}
	}

	//мониторинг изменения типа контроллера
	IEnumerator GetController(){
		while (doGetController){
			controller = InputManLib.GetController(this);
			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	void OnEnable(){
		if (!doGetController){
			doGetController = true;
			StartCoroutine(GetController());
		}
	}

	void OnDisable(){
		doGetController = false;
	}

	void OnDestroy(){
		OnDisable();
	}

	public float gas{
		get{
			return controller.GetAxis("Gas");
		}
		set{
			return;
		}
	}

	public float steering{
		get{
			return steerAxis;
		}
		set{
			return;
		}
	}

	public float maxSteering{
		get{
			return maxSteeringAngle;
		}
		set{
			maxSteeringAngle = value;
		}
	}

	public float maxLinearSpeed{
		get{
			return 0.0f;
		}
		set{
			return;
		}
	}

	public float inertia{
		get{
			return wheelInertia;
		}
		set{
			return;
		}
	}

	//Возвращает направление вращения дифференциала
	public int GetDirectionRotation(){
		return (int)Mathf.Sign(averageAngularVelocity);
	}

	//Линейная скорость точки на поверхности колеса в [км/ч]
	public float GetLinearSpeed(){
		return averageAngularVelocity * radsecTokmh;
	}

	//Линейная скорость точки на поверхности колеса в [м/с]
	public float GetLinearSpeedms(){
		return averageAngularVelocity * wheelRadius;
	}

	public Vector3 GetSpeed(){
		return rigidbody.velocity;
	}

	public float GetQuality(){
		return quality;
	}

	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality == 0.0f){
				enabled = false;
			}else{
				enabled = true;
			}
		}
	}
}

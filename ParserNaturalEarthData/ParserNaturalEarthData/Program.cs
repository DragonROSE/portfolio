﻿using System;
using System.IO;
using System.Text;

class Program
{
	const string TYPE_CONVERTER_SHP = "shp";
	const string TYPE_CONVERTER_DBF = "dbf";
	const char DELIMITER_CSV = ';';
	const char DELIMITER_REPLACE_CSV = ',';

	static void Main(string[] args)
	{
		if (args.Length != 3)
		{
			Console.WriteLine("Please use 3 parameters: <shp|dbf> <in_filepath> <out_filepath>");
		}
		else
		{
			string typeConverter = args[0].ToLower();

			if (typeConverter == TYPE_CONVERTER_SHP)
			{
				StringBuilder sb = new StringBuilder();
				if (ConverterShpToCsv.ConvertShpToCsv(args[1], DELIMITER_CSV, sb))
				{
					SaveOutputFile(args[2], sb);
				}
				else
				{
					Console.WriteLine("Error while parse SHP file!");
				}
			}
			else if (typeConverter == TYPE_CONVERTER_DBF)
			{
				byte[] bytes = null;
				int length = 0;
				if (ConverterDbfToCsv.ConvertDbfToCsv(args[1], DELIMITER_CSV, DELIMITER_REPLACE_CSV, ref length, ref bytes))
				{
					SaveOutputFile(args[2], length, ref bytes);
				}
				else
				{
					Console.WriteLine("Error while parse DBF file!");
				}
			}
		}

#if DEBUG
		Console.WriteLine("Program end. Press enter to terminate application.");
		Console.ReadLine();
#endif
	}

	static void SaveOutputFile(string filepath, StringBuilder sb)
	{
		try
		{
			File.WriteAllText(filepath, sb.ToString());
		}
		catch (Exception e)
		{
#if DEBUG
			Console.WriteLine(e.ToString());
#else
			Console.WriteLine(e.Message);
#endif
		}
	}

	static void SaveOutputFile(string filepath, int length, ref byte[] bytes)
	{
		try
		{
//			File.WriteAllText(filepath, sb.ToString(), Encoding.ASCII);
			using (BinaryWriter writer = new BinaryWriter(File.Open(filepath, FileMode.Create)))
			{
				writer.Write(bytes, 0, length);
			}
		}
		catch (Exception e)
		{
#if DEBUG
			Console.WriteLine(e.ToString());
#else
			Console.WriteLine(e.Message);
#endif
		}
	}
}

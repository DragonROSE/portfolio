echo off

set project_name=project
set git_branch=release
set projects_folder=C:\Projects
set build_folder=Build
set output_folder=\\mainserver.local\public\builds
set unity_folder=C:\Program Files\Unity\2018.2.6f1
for /f %%a in ('powershell -Command "Get-Date -format yyyyMMdd-HHmm"') do set date_time=%%a
set build_name=%project_name%-%date_time%-%git_branch%
set sources_folder=%projects_folder%\%project_name%

cd /d "%sources_folder%"

git fetch --all
git reset --hard origin/%git_branch%

cd /d "%unity_folder%\Editor"

echo.
echo Start build %build_name%

Unity.exe -batchmode -nographics -quit -projectPath "%sources_folder%" -buildWindows64Player "%sources_folder%\%build_folder%\%project_name%.exe"

robocopy "%sources_folder%\%build_folder%" "%output_folder%\%project_name%\%build_name%" /e /move

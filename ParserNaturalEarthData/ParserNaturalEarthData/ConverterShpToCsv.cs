﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

public static class ConverterShpToCsv
{
	const string HEADER_CSV = "Record;Shape;X;Y";
	const long SIZE_FILE_HEADER_SHP = 100;
	const long COUNT_DOUBLES_MBR = 4;
	const int TYPE_SHAPE_POLYLINE = 3;
	const int TYPE_SHAPE_POLYGON = 5;

	public static bool ConvertShpToCsv(string filepathShp, char delimiter, StringBuilder outStringBuilder)
	{
		outStringBuilder.AppendLine(HEADER_CSV);

		try
		{
			using (BinaryReader reader = new BinaryReader(File.Open(filepathShp, FileMode.Open)))
			{
				//skip ".shp" header
				reader.BaseStream.Position = SIZE_FILE_HEADER_SHP;

				//while not EOF
				while (reader.BaseStream.Position < reader.BaseStream.Length)
				{
					int recordNum = Utils.FromBigEndianToInt32(reader.ReadBytes(sizeof(int)));

					//skip "Record length" (int32)
					reader.BaseStream.Position += (long)sizeof(int);

					int typeShape = reader.ReadInt32();

					if (typeShape == TYPE_SHAPE_POLYLINE || typeShape == TYPE_SHAPE_POLYGON)
					{
						//skip MBR (4 doubles)
						reader.BaseStream.Position += (long)sizeof(double) * COUNT_DOUBLES_MBR;

						int countParts = reader.ReadInt32();
						int countPoints = reader.ReadInt32();

						//TODO: Memory allocations on every while loop
						int[] parts = new int[countParts];
						for (int i = 0; i < countParts; ++i)
						{
							parts[i] = reader.ReadInt32();
						}

						int partNum = 1;
						for (int i = 0; i < countPoints; ++i)
						{
							if (parts.Length > partNum && parts[partNum] == i)
							{
								++partNum;
							}

							outStringBuilder.
								Append(recordNum.ToString()).Append(delimiter).
								Append(partNum.ToString()).Append(delimiter).
								Append(reader.ReadDouble().ToString(CultureInfo.InvariantCulture)).Append(delimiter).
								Append(reader.ReadDouble().ToString(CultureInfo.InvariantCulture));

							if (reader.BaseStream.Position < reader.BaseStream.Length)
							{
								outStringBuilder.AppendLine();
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
#if DEBUG
			Console.WriteLine(e.ToString());
#else
			Console.WriteLine(e.Message);
#endif
			return false;
		}

		return true;
	}
}

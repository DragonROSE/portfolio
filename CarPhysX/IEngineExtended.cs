using UnityEngine;
using System.Collections;

/// <summary>
/// Интерфейс с расширенными параметрами двигателя
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IEngineExtended{

	/// <summary>Температура двигателя в градусах цельсия</summary>
	float temperature { get; set; }

	/// <summary>Текущий уровень топлива в литрах</summary>
	float fuel { get; set; }

	/// <summary>Текущий уровень заполненности топливом двигателя</summary>
	float fuelFill { get; set; }

	/// <summary>Текущее давление масла в двигателе</summary>
	float oilPressure { get; set; }

}

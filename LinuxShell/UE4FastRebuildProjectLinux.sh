#!/bin/sh

script_dir=$(dirname "$0")
project_dir="$script_dir"/..
absolute_project_dir=$(readlink -f "$project_dir")
project_name=Project
ue_install_dir="$absolute_project_dir"/../UnrealEngine
absolute_ue_install_dir=$(readlink -f "$ue_install_dir")

"$absolute_ue_install_dir"/Engine/Build/BatchFiles/Linux/Build.sh "$project_name"Editor Linux Development "$absolute_project_dir"/"$project_name".uproject -waitmutex

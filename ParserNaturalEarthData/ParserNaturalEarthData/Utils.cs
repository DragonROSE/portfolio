﻿public static class Utils
{
	public static int FromBigEndianToInt32(byte[] bytes)
	{
		if (bytes.Length != 4)
		{
			return 0;
		}

		byte[] tempBytes = new byte[4] { bytes[3], bytes[2], bytes[1], bytes[0] };

		return System.BitConverter.ToInt32(tempBytes, 0);
	}

	public static uint FromBigEndianToUInt32(byte[] bytes)
	{
		if (bytes.Length != 4)
		{
			return 0;
		}

		byte[] tempBytes = new byte[4] { bytes[3], bytes[2], bytes[1], bytes[0] };

		return System.BitConverter.ToUInt32(tempBytes, 0);
	}
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Анимация колёсной пары вагона
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class WagonAnimation : MonoBehaviour {

	public Transform wagonTransform = null; //трансформ, на котором висит скрипт физики колёсной пары
	public Transform wagonAnimate = null; //колёсная пара (вагонетка с колёсами)

	public Transform[] wagonAxis = null; //все оси колесной пары

	public Vector3 shift = Vector3.zero; //сдвиг относительно начальной позиции

	IWagon interfaceWagon = null;

	//состояние анимации колёсной пары
	float rotation = 0.0f;

	Vector3 startPos = Vector3.zero;
	Vector3 normal = Vector3.up;
	Vector3 tangent = Vector3.forward;
	Vector3 right = Vector3.right;

	//кэшированные величины
	float suspensionTravel = 0.0f;

	void Start(){
		if (wagonTransform == null){
			interfaceWagon = InterfaceLib.GetFirstInterfaceInParentChildren<IWagon>(transform);
		}else{
			interfaceWagon = InterfaceLib.GetInterfaceComponent<IWagon>(wagonTransform);
		}

		if (interfaceWagon == null){
			Debug.LogError("Wagon not found, can't animate wagon! Script disabled.");
			enabled = false;
			return;
		}

		if (wagonAnimate == null){
			wagonAnimate = transform;
		}

		suspensionTravel = interfaceWagon.GetSuspensionDistance();

		startPos = wagonAnimate.localPosition + shift;
	}

	void LateUpdate(){
		if (interfaceWagon.GetOnRailroad()){
			tangent = interfaceWagon.GetBodyLocalTangent();
			normal = interfaceWagon.GetBodyLocalNormal();

			tangent *= Mathf.Sign(tangent.z);
			right = Vector3.Cross(normal, tangent);
			tangent = Vector3.Cross(right, normal);

			wagonAnimate.localPosition = startPos + Vector3.up * interfaceWagon.GetCompression() * suspensionTravel;
			wagonAnimate.localRotation = Quaternion.LookRotation(tangent, normal);
		}

		rotation = Mathf.Repeat(rotation + Time.deltaTime * interfaceWagon.angularVelocity * Mathf.Rad2Deg, 360.0f);
		foreach (Transform currAxis in wagonAxis){
			currAxis.localRotation = Quaternion.Euler(rotation, 0.0f, 0.0f);
		}
	}
}

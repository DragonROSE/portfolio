/*
 * United 3D Labs
 * Aleksey Usanov
 * 
 * Linux console program for send commands
 * to Panasonic projectors over ethernet
 *
 * To compile program use:
 * g++ -O2 main.cpp md5.cpp -o ProjectorControl
 *
 */

#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "md5.h"

#define ANSWER_CODE_SHIFT 12
#define ANSWER_CODE_LENGTH 8

#define BUFFER_LENGTH 256

int createSocket()
{
	int result = socket(AF_INET, SOCK_STREAM, 0);
	if (result < 0)
	{
		printf("Error opening socket!\n");
		exit(1);
	}
	return result;
}

void connectToProjector(int sock, const char *str_addr, const char *str_port)
{
	int port = atoi(str_port);

	struct sockaddr_in server;
	server.sin_addr.s_addr = inet_addr(str_addr);
	server.sin_family = AF_INET;
	server.sin_port = htons(port);

	int err_code = connect(sock, (struct sockaddr *)&server, sizeof(server));
	if (err_code < 0)
	{
		printf("Connection to %s:%s failed!\n", str_addr, str_port);
		exit(1);
	}
}

void readFromSocket(int sock, char *buffer)
{
	memset(buffer, 0, BUFFER_LENGTH); //clean input buffer

	int err_code = read(sock, buffer, BUFFER_LENGTH);
	if (err_code < 0)
	{
		printf("Read from socket error!\n");
		exit(1);
	}
}

void writeToSocket(int sock, const char *buffer)
{
	int err_code = write(sock, buffer, strlen(buffer));
	if (err_code < 0)
	{
		printf("Write to socket error!\n");
		exit(1);
	}
}

void processAnswer(const char *buffer)
{
	//if first and second byte in answer is zero then command executed, else - error
	if (buffer[0] == '0' && buffer[1] == '0')
	{
		if (buffer[4] == '0')
		{
			printf("standby\n");
		}
		else if (buffer[4] == '1')
		{
			printf("working\n");
		}
		else
		{
			printf("Error: %c\n", buffer[4]);
		}
	}
	else if (buffer[0] == 'E')
	{
		if (buffer[3] == '1')
		{
			printf("Error: unknown command\n");
		}
		else if (buffer[3] == '2')
		{
			printf("Error: parameter out of range\n");
		}
		else if (buffer[3] == '3' || buffer[3] == '4')
		{
			printf("Error: projector is busy\n");
		}
		else if (buffer[3] == '5')
		{
			printf("Error: incorrect length of the command\n");
		}
		else if (buffer[3] == 'A')
		{
			printf("Error: incorrect login or password\n");
		}
		else
		{
			printf("Unknown error\n");
		}
	}
	else
	{
		printf("Unknown error\n");
	}
}

int main(int argc, char *argv[])
{
	if (argc != 6)
	{
		printf("Usage: %s <ip_address> <port> <login> <password> <command>\n", argv[0]);
		printf("Available commands: status, wakeup, poweroff\n");
		return 0;
	}

	char buffer_in[BUFFER_LENGTH], buffer_out[BUFFER_LENGTH];
	char md5_format[BUFFER_LENGTH], answer_code[BUFFER_LENGTH];

	char *input_addr = argv[1];
	char *input_port = argv[2];
	char *input_login = argv[3];
	char *input_pass = argv[4];
	char *input_command = argv[5];

	int sock = createSocket();
	connectToProjector(sock, input_addr, input_port);

	readFromSocket(sock, buffer_in);

	//copy special code
	memset(answer_code, 0, BUFFER_LENGTH);
	strncpy(answer_code, buffer_in + ANSWER_CODE_SHIFT, ANSWER_CODE_LENGTH);

	//format string "login:pass:code"
	strcpy(md5_format, input_login);
	strcat(md5_format, ":");
	strcat(md5_format, input_pass);
	strcat(md5_format, ":");
	strcat(md5_format, answer_code);

	//create md5 from special string
	MD5 md5_code(md5_format);
	std::string md5_str = md5_code.hexdigest();

	strcpy(buffer_out, md5_str.c_str());
	strcat(buffer_out, "00"); //special symbols (delimiter)

	//choose command to send
	bool requery = false;
	if (strcmp(input_command, "wakeup") == 0)
	{
		strcat(buffer_out, "PON");
	}
	else if (strcmp(input_command, "poweroff") == 0)
	{
		strcat(buffer_out, "POF");
	}
	else //query status
	{
		strcat(buffer_out, "QPW");
		requery = true;
	}

	strcat(buffer_out, "\r"); //"command end" symbol

	//send command
	writeToSocket(sock, buffer_out);

	//read answer
	if (requery)
	{
		readFromSocket(sock, buffer_in);
		processAnswer(buffer_in);
	}

	close(sock);

	return 0;
}

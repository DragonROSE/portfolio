using UnityEngine;
using System.Collections;

/// <summary>
/// Расширенный интерфейс шасси
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>
/// </summary>

public interface IChassisExtended{

	/// <summary>Принудительно пересчитать скорость вращения дифференциала и вернуть её</summary>
	float CalcAverageAngularVelocity();
	
	/// <summary>Максимальная скорость при которой срабатывает аварийная система торможения в километрах в час</summary>
//	float emergencyLinearBrakeSpeed { get; set; }

}

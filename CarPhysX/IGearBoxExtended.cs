using UnityEngine;
using System.Collections;

/// <summary>
/// Расширенный интерфейс коробки передач
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public interface IGearBoxExtended{

	/// <summary>Текущее положение сцепления</summary>
	float clutch { get; set; }

	/// <summary>Обратная связь - угловое ускорение передаваемое на двигатель от шасси</summary>
	float feedbackAngularAcceleration { get; set; }

	/// <summary>Текущий крутящий момент (торможение) передаваемый на колёса от коробки передач</summary>
	float wheelBrakeTorque { get; set; }

}

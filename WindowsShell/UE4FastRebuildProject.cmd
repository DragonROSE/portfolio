echo off

set script_dir=%~dp0
set project_dir=%script_dir%..
set project_name=Project
set ue_install_dir=C:\Program Files\Epic Games\UE_4.23\

echo.
echo Deleting binaries...

del /f /s /q "%project_dir%\Binaries\*"
for /d %%a in ("%project_dir%\Plugins\*") do del /f /s /q "%%a\Binaries\*"

echo.
echo Building project...

call "%ue_install_dir%\Engine\Build\BatchFiles\Build.bat" %project_name%Editor Win64 Development -Project="%project_dir%\%project_name%.uproject" -WaitMutex -FromMsBuild

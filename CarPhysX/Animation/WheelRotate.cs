using UnityEngine;
using System.Collections;

/// <summary>
/// Анимация вращения и движения колёс
/// Может работать с WheelCollider и интерфейсом IWheel
/// Если какой-либо элемент не задан в инспекторе, то пытается его найти
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class WheelRotate : MonoBehaviour, IQuality {

	public bool animateSuspension = true; //анимировать подвеску или нет (по Y)
	public bool animateRotation = true; //анимировать вращение колеса вокруг своей оси или нет (по X)

	public WheelCollider wheelCollider = null; //WheelCollider

	public Transform transformInterfaceWheel = null; //transform на котором висит интерфейс колеса
	IWheel interfaceWheel = null; //интерфейс колеса

	public Transform wheelModel = null; //модель колеса для анимации (если не указан, то используется данный transform)

	float radius = 1.0f;
	float offset = 0.0f;
	float rotation = 0.0f;
	float rad2Deg = Mathf.Rad2Deg;

	float quality = 1.0f;

	const float rpmToDeg = 360.0f / 60.0f;

	Vector3 locPosWheel = Vector3.zero; //локальная позиция 3D объекта колеса
	Vector3 startPos = Vector3.zero;

	delegate void AnimationDelegate();
	AnimationDelegate CallAnimation;

	void Start(){
		if (wheelCollider == null){
			wheelCollider = transform.GetComponentInChildren<WheelCollider>();
		}

		if (transformInterfaceWheel == null){
			interfaceWheel = InterfaceLib.GetFirstInterfaceInParentChildren<IWheel>(transform);
		}else{
			interfaceWheel = InterfaceLib.GetInterfaceComponent<IWheel>(transformInterfaceWheel);
		}

		if (wheelCollider == null && interfaceWheel == null){
			Debug.LogError("Wheel not found, can't animate wheel! Script disabled.");
			enabled = false;
			return;
		}

		if (wheelCollider != null){
			CallAnimation = AnimationWheelCollider;
			radius = wheelCollider.radius;
			offset = wheelCollider.suspensionDistance;
		}else{
			CallAnimation = AnimationWheelInterface;
			offset = interfaceWheel.GetSuspensionDistance();
		}

		if (wheelModel == null){
			wheelModel = transform;
		}

		if (animateSuspension){
			startPos = wheelModel.localPosition + transformInterfaceWheel.position - wheelModel.position;
		}
	}

	void LateUpdate(){
		CallAnimation();
	}

	void AnimationWheelCollider(){
		if (animateSuspension){
			WheelHit hit;
			locPosWheel = transform.localPosition;
			if (wheelCollider.GetGroundHit(out hit)){
				locPosWheel.y -= Vector3.Dot(transform.position - hit.point, transform.up) - radius; //расчёт положения колеса от сжатия подвески
			}else{
				locPosWheel.y = startPos.y - offset;
			}

			transform.localPosition = locPosWheel;
		}

		if (animateRotation){
			rotation = Mathf.Repeat(rotation + Time.deltaTime * wheelCollider.rpm * rpmToDeg, 360.0f); //поворот вокруг основной оси колеса
			transform.localRotation = Quaternion.Euler(rotation, wheelCollider.steerAngle, 0.0f);
		}
	}

	void AnimationWheelInterface(){
		if (animateSuspension){
			wheelModel.localPosition = startPos + Vector3.up * (interfaceWheel.GetCompression() - 0.5f) * offset;
		}
		if (animateRotation){
			rotation = Mathf.Repeat(rotation + Time.deltaTime * interfaceWheel.angularVelocity * rad2Deg, 360.0f);
			wheelModel.localRotation = Quaternion.Euler(rotation, interfaceWheel.steering, 0.0f);
		}
	}

	public float GetQuality(){
		return quality;
	}

	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality == 0.0f){
				enabled = false;
			}else{
				enabled = true;
			}
		}
	}
}

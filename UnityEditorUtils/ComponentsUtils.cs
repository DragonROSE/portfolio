using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Убивает все коллайдеры
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class ComponentsUtils : ScriptableObject {

	//Удаляет все коллайдеры на выделенных объектах
	[MenuItem("Tools/Component/Remove All Colliders")]
	static void RemoveAllCollidersFromSelected(){
		GameObject[] selection = Selection.gameObjects;

		foreach (GameObject currGameObject in selection){
			Collider[] allColliders = currGameObject.GetComponentsInChildren<Collider>(true);

			foreach (Collider currCollider in allColliders){
				if (currCollider != null){
					if (Application.isPlaying){
						Destroy(currCollider);
					}else{
						DestroyImmediate(currCollider);
					}
				}
			}
		}
	}

}

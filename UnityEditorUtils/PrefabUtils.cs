using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Расширение UnityEditor
/// Работа с префабами (Prefab)
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class PrefabUtils : ScriptableObject {

	//Для выделенных GameObjects выполняет операцию Revert, если они наследуются от префаба
	[MenuItem("Tools/Prefab/Revert Selected GameObjects To its Prefabs")]
	static void RevertToPrefabSelection(){
		int count = 0;
		GameObject[] gameObjects = Selection.gameObjects;

		foreach (GameObject currGameObject in gameObjects){
			if (PrefabUtility.GetPrefabObject(currGameObject) != null){
				PrefabUtility.RevertPrefabInstance(currGameObject);
				count++;
			}else{
				Debug.LogError("GameObject " + currGameObject.name + " can't revert to prefab", currGameObject);
			}
		}

		Debug.Log("Reverted GameObjects to prefab count = " + count.ToString());
	}
}

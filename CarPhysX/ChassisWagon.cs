using UnityEngine;
using System.Collections;

/// <summary>
/// Физика шасси вагона/локомотива
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class ChassisWagon : MonoBehaviour, IChassisWagon{

	const float getControllerDelay = 0.5f;

	public bool isController = false;

	public Transform wagonFrontTransform = null;
	public Transform wagonRearTransform = null;
	public Transform gearBoxTransform = null;

	IWagon wagonFront = null;
	IWagon wagonRear = null;
	IGearBox mainGearBox = null;

	float accel = 0.0f;
	float brake = 0.0f;
	float torque = 0.0f;
	float averageAngularVelocity = 0.0f;

	float radsecTokmh = 1.0f;
	float wheelRadius = 1.0f;

	bool doGetController = true;

	IInputMan controller = null; //данные с органов управления

	void Start(){
		wagonFront = InterfaceLib.GetInterfaceComponent<IWagon>(wagonFrontTransform);
		wagonRear = InterfaceLib.GetInterfaceComponent<IWagon>(wagonRearTransform);

		wheelRadius = wagonFront.wheelRadius;
		radsecTokmh = wheelRadius * 3600.0f / 1000.0f;

		if (gearBoxTransform != null){
			mainGearBox = InterfaceLib.GetInterfaceComponent<IGearBox>(gearBoxTransform);
		}
		if (mainGearBox == null){
			enabled = false;
		}

		if (isController){
			StartCoroutine(GetController());
		}
	}
	
	void FixedUpdate(){
		if (mainGearBox != null){
			accel = controller.GetAxis("Gas");
			brake = controller.GetAxis("Brake");
			torque = mainGearBox.wheelAccelerationTorque * accel / 4.0f; //передача крутящего момента на 4 точки касания (на каждую 1/4 результирующей силы)
			wagonFront.torque = torque;
			wagonRear.torque = torque;
			wagonFront.brake = brake;
			wagonRear.brake = brake;
			averageAngularVelocity = (wagonFront.angularVelocity + wagonRear.angularVelocity) * 0.5f;
		}
	}

	//мониторинг изменения типа контроллера
	IEnumerator GetController(){
		while (doGetController){
			controller = InputManLib.GetController(this);
			yield return new WaitForSeconds(getControllerDelay);
		}
	}

	void OnEnable(){
		if (!doGetController && isController){
			doGetController = true;
			StartCoroutine(GetController());
		}
	}

	void OnDisable(){
		doGetController = false;
	}

	void OnDestroy(){
		doGetController = false;
	}

	//Линейная скорость точки на поверхности колеса в [км/ч]
	public float GetLinearSpeed(){
		return wagonFront.angularVelocity * radsecTokmh;
	}

	//Линейная скорость точки на поверхности колеса в [м/с]
	public float GetLinearSpeedms(){
		return averageAngularVelocity * wheelRadius;
	}

	public float GetAverageAngularVelocity(){
		return averageAngularVelocity;
	}

	public Vector3 GetSpeed(){
		return rigidbody.velocity;
	}

	public Vector3 GetPosition(){
		return transform.position;
	}
}

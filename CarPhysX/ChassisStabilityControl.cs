using UnityEngine;
using System.Collections;

/// <summary>
/// Реализует поперечную устойчивость выбранного моста
/// Позволяет использовать в качестве колёс WheelCollider или колёса реализуемые через интерфейс IWheel
/// Скрипт рекомендуется вешать на основной (головной) Rigidbody
/// Мост указывается с помощью левого и правого колеса одной оси
/// Если основной Rigidbody или одно из колёс не найдено при старте, то скрипт отключается
/// </summary>
/// <para> Author: Алексей Усанов </para>
/// <para> Version: 1.0 </para>

public class ChassisStabilityControl : MonoBehaviour, IQuality {

	public Rigidbody mainRigidbody = null; //основной Rigidbody к которому прикладывается сила (если не указан, то используется текущий)
	public WheelCollider wheelColliderLeft = null; //левое колесо (WheelCollider)
	public WheelCollider wheelColliderRight = null; //правое колесо (WheelCollider)

	public Transform wheelInterfaceTransformLeft = null; //Transform интерфейса левого колеса
	public Transform wheelInterfaceTransformRight = null; //Transform интерфейса правого колеса

	public float antiRoll = 0.0f; //сила поперечной устойчивости (рекомендуется половина величины жесткости подвески) [н]

	public string antiRollName = "Front"; //имя балки поперечной устойчивости (необходимо для Loader в файле XML Settings)

	float compressionLeft = 0.0f;
	float compressionRight = 0.0f;
	float antiRollForce = 0.0f;
	float quality = 1.0f;

	float wheelRadiusLeft, wheelRadiusRight;
	float wheelOffsetLeft, wheelOffsetRight;

	bool groundedLeft = false;
	bool groundedRight = false;

	IWheel wheelInterfaceLeft = null; //левое колесо (IWheel)
	IWheel wheelInterfaceRight = null; //правое колесо (IWheel)

	Transform wheelTransformLeft = null;
	Transform wheelTransformRight = null;

	delegate void WheelAntiRollDelegate(); //выбор на старте, работа поперечной устойчивости для WheelCollider или через IWheel
	WheelAntiRollDelegate CallWheelAntiRoll;

	void Start(){
		if (mainRigidbody == null){ //попытка найти основной Rigidbody
			if (rigidbody != null){
				mainRigidbody = rigidbody;
			}else{
				Debug.LogError("Rigidbody not found! Script disabled.");
				enabled = false;
				return;
			}
		}

		if (wheelInterfaceTransformLeft != null && wheelInterfaceTransformRight != null){
			wheelInterfaceLeft = InterfaceLib.GetInterfaceComponent<IWheel>(wheelInterfaceTransformLeft);
			wheelInterfaceRight = InterfaceLib.GetInterfaceComponent<IWheel>(wheelInterfaceTransformRight);
		}

		if ((wheelColliderLeft == null || wheelColliderRight == null) && (wheelInterfaceLeft == null || wheelInterfaceRight == null)){
			Debug.LogError("Wheels not found! Script disabled.");
			enabled = false;
			return;
		}

		if (wheelColliderLeft != null && wheelColliderRight != null){
			wheelRadiusLeft = wheelColliderLeft.radius;
			wheelRadiusRight = wheelColliderRight.radius;
			wheelOffsetLeft = wheelColliderLeft.suspensionDistance;
			wheelOffsetRight = wheelColliderRight.suspensionDistance;
			wheelTransformLeft = wheelColliderLeft.transform;
			wheelTransformRight = wheelColliderRight.transform;
			CallWheelAntiRoll = WheelColliderAntiRoll;
		}else{
			wheelTransformLeft = wheelInterfaceTransformLeft;
			wheelTransformRight = wheelInterfaceTransformRight;
			CallWheelAntiRoll = WheelInterfaceAntiRoll;
		}
	}

	// ------- DEBUG Start ------- //
//	void LateUpdate(){
//		Debug.DrawRay(wheelTransformLeft.position, wheelTransformLeft.up * antiRollForce / mainRigidbody.mass);
//		Debug.DrawRay(wheelTransformRight.position, wheelTransformRight.up * -antiRollForce / mainRigidbody.mass);
//	}
	// -------- DEBUG End -------- //

	void FixedUpdate(){
		CallWheelAntiRoll();

		antiRollForce = (compressionLeft - compressionRight) * antiRoll;

		if (groundedLeft){
			mainRigidbody.AddForceAtPosition(wheelTransformLeft.up * antiRollForce, wheelTransformLeft.position);
		}
		if (groundedRight){
			mainRigidbody.AddForceAtPosition(wheelTransformRight.up * -antiRollForce, wheelTransformRight.position);
		}
	}

	void WheelColliderAntiRoll(){
		compressionLeft = 0.0f;
		compressionRight = 0.0f;

		WheelHit hit;

		groundedLeft = wheelColliderLeft.GetGroundHit(out hit);
		if (groundedLeft){
			compressionLeft = 1.0f - (-wheelColliderLeft.transform.InverseTransformPoint(hit.point).y - wheelRadiusLeft) / wheelOffsetLeft;
		}

		groundedRight = wheelColliderRight.GetGroundHit(out hit);
		if (groundedRight){
			compressionRight = 1.0f - (-wheelColliderRight.transform.InverseTransformPoint(hit.point).y - wheelRadiusRight) / wheelOffsetRight;
		}
	}

	void WheelInterfaceAntiRoll(){
		groundedLeft = wheelInterfaceLeft.GetOnGround();
		groundedRight = wheelInterfaceRight.GetOnGround();
		compressionLeft = wheelInterfaceLeft.GetCompression();
		compressionRight = wheelInterfaceRight.GetCompression();
	}

	public float GetQuality(){
		return quality;
	}

	public void SetQuality(float newQuality){
		if (quality != newQuality){
			quality = newQuality;

			if (newQuality == 0.0f){
				enabled = false;
			}else{
				enabled = true;
			}
		}
	}
}

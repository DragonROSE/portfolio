using System;
using Ventuz.Kernel;

/// <summary>
/// ���������� ������
/// ������� �������� ������
/// ������� ��������� ��� ����������� � �������
/// </summary>
/// <para> Author: ������� ������ </para>
/// <para> Version: 1.0 </para>
public class Script : ScriptBase, System.IDisposable
{
    // This member is used by the Validate() method to indicate
    // whether the Generate() method should return true or false
    // during its next execution.
    private bool changed;

	private float _axisPan = 0.0f;
	private float _axisTilt = 0.0f;
	private float _axisFov = 0.0f;

    // This Method is called if the component is loaded/created.
    public Script()
    {
        // Note: Accessing input or output properties from this method
        // will have no effect as they have not been allocated yet.
    }

    // This Method is called if the component is unloaded/disposed
    public virtual void Dispose()
    {
    }

    // This Method is called if an input property has changed its value
    public override void Validate()
    {
        // Remember: set changed to true if any of the output 
        // properties has been changed, see Generate()

		InputEvent();
    }

    // This Method is called every time before a frame is rendered.
    // Return value: if true, Ventuz will notify all nodes bound to this
    //               script node that one of the script's outputs has a
    //               new value and they therefore need to validate. For
    //               performance reasons, only return true if output
    //               values really have been changed.
    public override bool Generate()
    {
		_axisPan = AxisUpdate(_axisPan, AxisPan, _SensPan);
		_axisTilt = AxisUpdate(_axisTilt, AxisTilt, _SensTilt);
		_axisFov = AxisUpdate(_axisFov, AxisFov, _SensFov);

		_axisTilt = SmoothStopLimits(_axisTilt, Tilt, _MinTilt, _MaxTilt);
		_axisFov = SmoothStopLimits(_axisFov, Fov, _MinFov, _MaxFov);

		float fovFactor = Fov / _MaxFov;

		Pan += _axisPan * _SpeedPan * fovFactor;
		Tilt += _axisTilt * _SpeedTilt * fovFactor;
		Fov += _axisFov * _SpeedFov;

		Tilt = Clamp(Tilt, _MinTilt, _MaxTilt);
		Fov = Clamp(Fov, _MinFov, _MaxFov);

        return true;
	}

	private float AxisUpdate(float axis, float axisInput, float sens)
	{
		if ((float)Math.Abs(axisInput - axis) > sens)
		{
			if (axisInput > axis)
			{
				axis += sens;
			}
			else if (axisInput < axis)
			{
				axis -= sens;
			}
		}
		else
		{
			axis = axisInput;
		}

		return axis;
	}

	//������� �������� ��������, ��� ����������� � �������
	private float SmoothStopLimits(float axis, float camParam, float min, float max)
	{
		float perc = (float)Math.Abs(max - min) * _PercentToSmoothStop / 100.0f;

		if (camParam + perc > max)
		{
			float axisSmooth = (max - camParam) / perc;
			if (axis > axisSmooth)
			{
				axis = axisSmooth;
			}
		}
		else if (camParam - perc < min)
		{
			float axisSmooth = (min - camParam) / perc;
			if (axis < axisSmooth)
			{
				axis = axisSmooth;
			}
		}

		return axis;
	}

	private float Clamp(float val, float min, float max)
	{
		if (val > max)
		{
			val = max;
		}
		else if (val < min)
		{
			val = min;
		}

		return val;
	}
	
	// This Method is called if the function/method Reset is invoked by the user or a bound event.
	// Return true, if this component has to be revalidated!
	public bool OnReset(int arg)
	{
		Pan = 0.0f;
		Tilt = 0.0f;
		Fov = _StartFov;

		return false;
	}

}
